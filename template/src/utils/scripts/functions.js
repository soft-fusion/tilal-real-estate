import $ from 'jquery';
import { t } from '../i18n/i18n';

export const getShortenValue = value => {
  const rtl = $('body').hasClass('rtl');
  if (value >= 1000000) {
    let number = Math.round((value / 1000000 + Number.EPSILON) * 100) / 100;
    number = number.toString().split('.');

    const translatedNumber = number.map(n => t(`numbers.${n}`)).join('.');

    return `${rtl ? `${t('shorten.million', 'M')}` : ''}${translatedNumber}${
      !rtl ? `${t('shorten.million', 'M')}` : ''
    }`;
  } else if (value >= 1000) {
    let number = Math.round(value / 1000 + Number.EPSILON);
    number = number.toString().split('.');

    const translatedNumber = number.map(n => t(`numbers.${n}`)).join('.');

    return `${rtl ? `${t('shorten.thousand', 'k')}` : ''}${translatedNumber}${
      !rtl ? `${t('shorten.thousand', 'k')}` : ''
    }`;
  } else return t(`numbers.${value}`, value);
};

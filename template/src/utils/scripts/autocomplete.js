import $ from 'jquery';

const autocomplete = (input, values, options = {}) => {
  const parent = $(input).parent();
  const wrapper = $('<div>', { class: 'autocompleteSF' });
  parent.append(wrapper);
  wrapper.append(input);

  $(input).on('input', () => {
    $('.autocompleteSF__list').remove();
    if ($(input).val()) {
      const filtered = values.filter(item =>
        item.name.toLowerCase().startsWith(
          $(input)
            .val()
            .toLowerCase()
        )
      );
      const autocompleteWrapper = $('<div>', { class: 'autocompleteSF__list' });
      filtered.forEach(item => {
        const listItem = options.optionAsLink
          ? $('<a>', { href: item.url })
          : $('<div>');
        listItem.addClass('autocompleteSF__item');
        listItem.html(item.name);
        autocompleteWrapper.append(listItem);
        listItem.on('click', () => {
          $(input)
            .val(item.name)
            .trigger('autocomplete:change');
          autocompleteWrapper.remove();
        });
      });
      wrapper.append(autocompleteWrapper);
      document.addEventListener('click', e => {
        const target = e.target;
        if (
          $(target).closest(input).length === 0 &&
          $(target).closest(autocompleteWrapper).length === 0 &&
          !$(target).is(input)
        ) {
          autocompleteWrapper.remove();
        }
      });
    } else {
      if (!options.optionAsLink) {
        $(input).trigger('autocomplete:change');
      }
    }
  });
};

export default autocomplete;

import store from 'store2';
import $ from 'jquery';

export const fetchProjects = async () => {
  const lang = $('body').hasClass('rtl') ? '' : '?lang=en';
  const res = await fetch(
    `${location.origin}/wp-json/allInvestment/v1/response${lang}`
  );
  const data = await res.json();

  const projects = data
    .map(project => {
      const types = [...new Set(project.apartment.map(a => a.type))];
      const prices = project.apartment.map(a => parseInt(a.price, 10));
      const bedrooms = project.apartment.map(a => parseInt(a.bedrooms, 10));
      const bathrooms = project.apartment.map(a => parseInt(a.bathrooms, 10));
      if (
        prices.some(b => Number.isNaN(b)) ||
        bedrooms.some(b => Number.isNaN(b)) ||
        bathrooms.some(b => Number.isNaN(b)) ||
        !project.position.latitude ||
        !project.position.longitude ||
        !project.city
      ) {
        return null;
      }
      return {
        title: project.name_investment,
        url: project.link_investment,
        location: project.city,
        image: project.miniature,
        position: [project.position.latitude, project.position.longitude],
        types: types,
        price: [Math.min(...prices), Math.max(...prices)],
        bedrooms: [Math.min(...bedrooms), Math.max(...bedrooms)],
        bathrooms: [Math.min(...bathrooms), Math.max(...bathrooms)]
      };
    })
    .filter(project => project !== null);

  store.session('projects', projects);
};

export const fetchCities = async () => {
  const lang = $('body').hasClass('rtl') ? '' : '?lang=en';
  const res = await fetch(
    `${location.origin}/wp-json/allCity/v1/response${lang}`
  );
  const data = await res.json();

  const cities = data.map(c => ({ name: c.name_city, url: c.link_city }));

  store.session('cities', cities);
};

export const fetchOneProject = async investmentName => {
  const lang = $('body').hasClass('rtl') ? '' : '&lang=en';
  const res = await fetch(
    `${location.origin}/wp-json/searchPosition/v1/response/?investment=${investmentName}${lang}`
  );
  const data = await res.json();

  return data[0];
};

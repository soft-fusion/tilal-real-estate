import $ from 'jquery';
import ar from './lang/ar.json';
import en from './lang/en.json';

export const t = (key, fallback) => {
  const isArabic = $('body').hasClass('rtl');
  const dictionary = isArabic ? ar : en;
  if (!fallback) {
    fallback = key;
  }

  try {
    let result = '';
    if (key.includes('numbers')) {
      const number = key.replace('numbers.', '').split('');
      number.forEach(n => {
        result += dictionary.numbers[n];
      });
    } else {
      const splitPath = key.split('.');
      result = splitPath.reduce((acc, cur) => acc[cur], dictionary);
    }

    if (!result) throw new Error();

    return result;
  } catch (e) {
    console.warn(
      `i18n warning:\n^^^^^^^^^^^^\nKey '${key}' couldn't be resolved, so fallback '${fallback}' was used instead.\n`
    );

    return fallback;
  }
};

import $ from 'jquery';
import { fetchOneProject } from '../../utils/scripts/fetchHelper';
import mapsStyles from '../mapsCommon/mapsStyles';

const investmentMap = {
  googleMapsLink: null,
  position: null,
  settings: {
    target: '.investmentMap',
    map: '#investment-map'
  },
  async init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      const name = $(this.$target.root).attr('data-investment-name');
      const project = await fetchOneProject(name);
      this.position = project.position;
      this.googleMapsLink = project.google_maps_link;
      this.initMap();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      map: target.find(settings.map)
    };
  },
  initMap() {
    window.googleMapsLoader.load().then(google => {
      $('.investmentMap__placeholder').addClass('-hidden');
      const map = new google.maps.Map(
        document.getElementById('investment-map'),
        {
          zoom: 12,
          fullscreenControl: false,
          fullscreenControlOptions: false,
          zoomControl: true,
          zoomControlOptions: false,
          clickableIcons: true,
          disableDefaultUI: true,
          disableDoubleClickZoom: true,
          draggable: true,
          draggableCursor: 'default',
          center: new google.maps.LatLng(
            this.position.latitude,
            this.position.longitude
          ),
          styles: mapsStyles
        }
      );
      const marker = new google.maps.Marker({
        position: new google.maps.LatLng(
          this.position.latitude,
          this.position.longitude
        ),
        map: map,
        icon: `${location.origin}/wp-content/themes/tilal/assets/images/markerHP-fit.svg`
      });
      marker.addListener('click', async () => {
        const position = marker.getPosition();
        map.setCenter(position);
        animateZoom(map, 18);
        const zoomDiff = Math.abs(18 - map.getZoom());
        if (this.googleMapsLink) {
          setTimeout(() => {
            window.open(this.googleMapsLink, '_blank');
          }, zoomDiff * 250);
        }
      });
      const animateZoom = (map, targetZoom, commandedZoom) => {
        const currentZoom = commandedZoom || map.getZoom();
        if (currentZoom !== targetZoom) {
          google.maps.event.addListenerOnce(
            map,
            'zoom_changed',
            async function() {
              animateZoom(
                map,
                targetZoom,
                currentZoom + (targetZoom > currentZoom ? 1 : -1)
              );
            }
          );
          setTimeout(function() {
            map.setZoom(currentZoom + (targetZoom > currentZoom ? 1 : -1));
          }, 150);
        }
      };
    });
  }
};
export default investmentMap;

import $ from 'jquery';
const formChanger = {
  settings: {
    target: 'body',
    callBoxForm: '.callBox #crmWebToEntityForm form',
    callBoxFormInputs: '.callBox #crmWebToEntityForm form input',
    contactOffcanvasForm: '.contactOffcanvas #crmWebToEntityForm form',
    contactOffcanvasFormInputs:
      '.contactOffcanvas #crmWebToEntityForm form input',
    joinForm: '.joinOurVipList #crmWebToEntityForm form',
    joinFormInputs: '.joinOurVipList #crmWebToEntityForm form input',
    newsletterForm: "div[data-type='signupform_0']",
    newsletterFormInput: "div[data-type='signupform_0'] form input",
    newsletterForm2Input: "div[data-type='signupform'] form input",
    newsletterForm2: "div[data-type='signupform']"
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.catchFormsOriginalData();
      this.bindEvents();
      this.newsletterFormFixes();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      callBoxForm: target.find(settings.callBoxForm),
      callBoxFormInputs: target.find(settings.callBoxFormInputs),
      contactOffcanvasForm: target.find(settings.contactOffcanvasForm),
      contactOffcanvasFormInputs: target.find(
        settings.contactOffcanvasFormInputs
      ),
      joinForm: target.find(settings.joinForm),
      joinFormInputs: target.find(settings.joinFormInputs),
      newsletterForm: target.find(settings.newsletterForm),
      newsletterForm2: target.find(settings.newsletterForm2),
      newsletterFormInput: target.find(settings.newsletterFormInput),
      newsletterForm2Input: target.find(settings.newsletterForm2Input)
    };
  },
  catchFormsOriginalData() {
    const rows = $(this.$target.callBoxForm).find('.zcwf_row');
    let originalData = {
      name: $(this.$target.callBoxForm).attr('name')
    };
    let inputs = [];
    $.each(rows, (index, row) => {
      inputs.push(
        $(row)
          .find('input')
          .attr('id')
      );
    });
    originalData.inputs = inputs;
    this.originalData = originalData;
  },
  newsletterFormFixes() {
    if (this.$target.newsletterForm.length > 0) {
      $("div[data-type='signupform'] form input[type='button']").on(
        'click',
        this.clickOnSecondNewsletter.bind(this)
      );
      $("div[data-type='signupform'] form input[type='text']").on(
        'input',
        this.inputOnSecondNewsletter.bind(this)
      );
    }
  },
  clickOnSecondNewsletter() {
    $("div[data-type='signupform_0'] form input[type='button']").trigger(
      'click'
    );
    setTimeout(() => {
      let style = $("div[data-type='signupform_0'] form #errorMsgDiv")[0].style
        .display;
      $("div[data-type='signupform'] form #errorMsgDiv").css('display', style);
    }, 100);
    setTimeout(() => {
      $("div[data-type='signupform'] form input[type='text']")[0].value = $(
        "div[data-type='signupform_0'] form input[type='text']"
      )[0].value;
    }, 1100);
  },
  inputOnSecondNewsletter(e) {
    $("div[data-type='signupform_0'] form input[type='text']")[0].value =
      e.currentTarget.value;
  },
  bindEvents() {
    $(this.$target.callBoxFormInputs).on('input', this.changeForm.bind(this));
    $(this.$target.joinFormInputs).on('input', this.changeForm.bind(this));
  },
  changeForm(e) {
    const target = $(e.currentTarget);
    let thisForm;
    if ($(target).parents('.callBox').length > 0) {
      thisForm = this.$target.callBoxForm;
    } else if ($(target).parents('.contactOffcanvas').length > 0) {
      thisForm = this.$target.contactOffcanvasForm;
    } else {
      thisForm = this.$target.joinForm;
    }
    let otherForm;
    if (
      ($('.callBox').length && $(target).parents('.callBox').length > 0) ||
      ($('.contactOffcanvas').length &&
        $(target).parents('.contactOffcanvas').length > 0)
    ) {
      otherForm = this.$target.joinForm;
    } else if ($('.callBox').length) {
      otherForm = this.$target.callBoxForm;
    } else if ($('.contactOffcanvas').length) {
      otherForm = this.$target.contactOffcanvasForm;
    }
    const thisFormRows = $(thisForm).find('.zcwf_row');
    const otherFormRows = $(otherForm).find('.zcwf_row');
    $(thisForm).attr('name', this.originalData.name);
    $.each($(thisFormRows), (index, row) => {
      $(row)
        .find('input')
        .attr('id', this.originalData.inputs[index]);
    });
    $(otherForm).attr('name', `${this.originalData.name}_other`);
    $.each($(otherFormRows), (index, row) => {
      $(row)
        .find('input')
        .attr('id', `${this.originalData.inputs[index]}_other`);
    });
  }
};
export default formChanger;

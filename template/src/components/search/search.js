import $ from 'jquery';

const search = {
  settings: {
    target: '.search',
    offCanvas: '.searchOffCanvas',
    buttonOffCanvas: '.search__button',
    closeOffCanvas: '.searchOffCanvas__close',
    backdropOffCanvas: '.searchOffCanvas__backdrop',
    map: '.searchMap',
    buttonMap: '.searchOffCanvas__button',
    closeMap: '.searchMap__backButton',
    modal: '.searchFiltersModal',
    buttonModal: '.searchMap__filtersButton',
    closeModal: '.searchFiltersModal__close',
    backdropModal: '.searchFiltersModal__backdrop'
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      offCanvas: $(this.settings.offCanvas),
      buttonOffCanvas: target.find(this.settings.buttonOffCanvas),
      closeOffCanvas: $(this.settings.closeOffCanvas),
      backdropOffCanvas: $(this.settings.backdropOffCanvas),
      map: $(this.settings.map),
      buttonMap: target.find(this.settings.buttonMap),
      closeMap: $(this.settings.closeMap),
      modal: $(this.settings.modal),
      buttonModal: target.find(this.settings.buttonModal),
      closeModal: $(this.settings.closeModal),
      backdropModal: $(this.settings.backdropModal)
    };
  },
  bindEvents() {
    $(
      `${this.settings.buttonOffCanvas}, ${this.settings.closeOffCanvas}, ${this.settings.backdropOffCanvas}`
    ).on('click', () => {
      $(this.$target.offCanvas).toggleClass('-open');
    });
    $(`${this.settings.buttonMap}, ${this.settings.closeMap}`).on(
      'click',
      () => {
        $(this.$target.map).toggleClass('-open');
        if ($(this.$target.map).hasClass('-open')) {
          $('body').css('overflow', 'hidden');
        } else {
          $('body').removeAttr('style');
        }
      }
    );
    $(
      `${this.settings.buttonModal}, ${this.settings.closeModal}, ${this.settings.backdropModal}`
    ).on('click', () => {
      $(this.$target.modal).toggleClass('-open');
    });
  }
};
export default search;

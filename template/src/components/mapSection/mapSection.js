import $ from 'jquery';
import store from 'store2';
import { t } from '../../utils/i18n/i18n';
import mapsInfoWindow from '../mapsCommon/mapsInfoWindow';
import mapsStyles from '../mapsCommon/mapsStyles';

const mapSection = {
  markers: [],
  mapMarkers: [],
  filteredMarkers: [],
  settings: {
    target: '.mapSection',
    boxButton: '.mapSection__backButton',
    filtersCount: '.mapSection__filtersCount',
    modal: '.mapSectionModal',
    buttonModal: '.mapSection__filtersButton',
    closeModal: '.mapSectionModal__close',
    backdropModal: '.mapSectionModal__backdrop',
    form: '.mapSectionModal__form',
    submitFilters: '.mapSectionModal__showButton',
    resultCount: '.mapSectionModal__resultCount',
    locationFilter: 'input[name="location"]',
    homeTypeFilter: 'input[name="home-type"]',
    priceFromFilter: 'input[name="price-from"]',
    priceToFilter: 'input[name="price-to"]',
    bedroomsFilter: 'input[name="bedrooms"]',
    bathroomsFilter: 'input[name="bathrooms"]'
  },
  async init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.markers = store.session('projects');
      this.initMap();
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      boxButton: target.find(this.settings.boxButton),
      filtersCount: target.find(this.settings.filtersCount),
      modal: $(this.settings.modal),
      buttonModal: target.find(this.settings.buttonModal),
      closeModal: $(this.settings.closeModal),
      backdropModal: $(this.settings.backdropModal),
      form: $(this.settings.form),
      submitFilters: $(this.settings.submitFilters),
      resultCount: $(this.settings.resultCount)
    };
  },
  bindEvents() {
    $(this.$target.boxButton).on('click', function() {
      $(this)
        .parents('.mapSection__box')
        .toggleClass('-open');
    });
    $(
      `${this.settings.buttonModal}, ${this.settings.closeModal}, ${this.settings.backdropModal}`
    ).on('click', () => {
      $(this.$target.modal).toggleClass('-open');
    });
    $(this.$target.submitFilters).on('click', this.submitFilters.bind(this));
    $(this.$target.form)
      .find(this.settings.locationFilter)
      .on('autocomplete:change', this.setFiltersAndMarkers.bind(this));
    $(this.$target.form)
      .find(this.settings.homeTypeFilter)
      .on('change', this.setFiltersAndMarkers.bind(this));
    $(this.$target.form)
      .find(this.settings.priceFromFilter)
      .on('blur', this.setFiltersAndMarkers.bind(this));
    $(this.$target.form)
      .find(this.settings.priceToFilter)
      .on('blur', this.setFiltersAndMarkers.bind(this));
    $(this.$target.form)
      .find(this.settings.bedroomsFilter)
      .on('change', this.setFiltersAndMarkers.bind(this));
    $(this.$target.form)
      .find(this.settings.bathroomsFilter)
      .on('change', this.setFiltersAndMarkers.bind(this));
  },
  submitFilters() {
    $(this.$target.filtersCount).html(
      t(
        `numbers.${Object.keys(this.filters).length}`,
        Object.keys(this.filters).length
      )
    );
    this.hideMarkers();
    $(this.$target.modal).toggleClass('-open');
  },
  hideMarkers() {
    this.mapMarkers.forEach((marker, index) => {
      marker.setVisible(this.markers[index].isVisible);
    });
  },
  clearForm() {
    $('.mapSectionModal__input')
      .val('')
      .trigger('change');
    $('.mapSectionModal__homeTypeInput')
      .prop('checked', false)
      .trigger('change');
    $('.mapSectionModal__barInput.-lower')
      .val('0')
      .trigger('change');
    $('.mapSectionModal__barInput.-upper')
      .val($(this.$target.bar).attr('data-end'))
      .trigger('change');
    $('.mapSectionModal__numberInput').attr('value', 'Any');
  },
  initMap() {
    let infoWindow;
    window.googleMapsLoader.load().then(google => {
      $('.mapSection__placeholder').addClass('-hidden');
      const map = new google.maps.Map(document.getElementById('hp-map'), {
        zoom: 6.3,
        fullscreenControl: false,
        fullscreenControlOptions: false,
        zoomControl: true,
        zoomControlOptions: false,
        clickableIcons: true,
        disableDefaultUI: true,
        disableDoubleClickZoom: true,
        draggable: true,
        draggableCursor: 'default',
        center: new google.maps.LatLng(24.72, 46.82),
        styles: mapsStyles
      });
      map.addListener('click', () => {
        if (infoWindow) {
          infoWindow.close();
        }
      });
      this.markers.forEach(item => {
        let marker = new google.maps.Marker({
          position: new google.maps.LatLng(...item.position),
          map: map,
          icon: `${location.origin}/wp-content/themes/tilal/assets/images/markerHP-fit.svg`
        });
        this.mapMarkers.push(marker);
        marker.addListener('click', () => {
          if (infoWindow) {
            infoWindow.close();
          }
          infoWindow = new google.maps.InfoWindow({
            content: mapsInfoWindow(item)
          });
          infoWindow.open({
            anchor: marker,
            map,
            shouldFocus: false
          });
        });
      });
    });
  },
  prepareFilters() {
    const filters = {};
    const location = $(this.$target.form)
      .find(this.settings.locationFilter)
      .val();
    const homeType = $(this.$target.form)
      .find(this.settings.homeTypeFilter)
      .filter(':checked')
      .toArray()
      .map(type => $(type).val());
    const priceFrom = $(this.$target.form)
      .find(this.settings.priceFromFilter)
      .val();
    const priceTo = $(this.$target.form)
      .find(this.settings.priceToFilter)
      .val();
    const bedrooms = $(this.$target.form)
      .find(this.settings.bedroomsFilter)
      .val();
    const bathrooms = $(this.$target.form)
      .find(this.settings.bathroomsFilter)
      .val();

    filters.location = location;
    filters['home-type'] = homeType;
    filters['price-from'] =
      priceFrom === $('.mapSectionModal__bar').attr('data-start')
        ? undefined
        : parseInt(priceFrom, 10);
    filters['price-to'] =
      (parseInt(priceTo, 10) === priceTo) ===
      $('.searchFiltersModal__bar').attr('data-end')
        ? undefined
        : parseInt(priceTo, 10);
    filters.bedrooms =
      bedrooms.toLowerCase() === 'any' ? undefined : parseInt(bedrooms, 10);
    filters.bathrooms =
      bathrooms.toLowerCase() === 'any' ? undefined : parseInt(bathrooms, 10);

    Object.keys(filters).forEach(key => {
      if (
        filters[key] === undefined ||
        filters[key] === '' ||
        filters[key].length === 0
      ) {
        delete filters[key];
      }
    });
    return filters;
  },
  setFiltersAndMarkers() {
    this.filters = this.prepareFilters();
    this.filteredMarkers = this.markers.filter(marker => {
      if (
        this.filters.location &&
        this.filters.location.toLowerCase() !== marker.location.toLowerCase()
      ) {
        return false;
      }
      if (
        this.filters['home-type'] &&
        !this.filters['home-type'].some(t => marker.types.includes(t))
      ) {
        let counter = 0;
        marker.types.forEach(marker => {
          marker.forEach(element => {
            this.filters['home-type'].forEach(sValue => {
              if (sValue === element) {
                counter = counter + 1;
              }
            });
          });
        });
        if (counter === 0) {
          return false;
        }
      }
      if (
        (this.filters['price-from'] || this.filters['price-from'] === 0) &&
        this.filters['price-from'] > marker.price[1]
      ) {
        return false;
      }
      if (
        (this.filters['price-to'] || this.filters['price-to'] === 0) &&
        this.filters['price-to'] < marker.price[0]
      ) {
        return false;
      }
      if (this.filters.bedrooms && this.filters.bedrooms > marker.bedrooms[1]) {
        return false;
      }
      if (
        this.filters.bathrooms &&
        this.filters.bathrooms > marker.bathrooms[1]
      ) {
        return false;
      }
      return true;
    });
    this.markers.forEach(marker => {
      marker.isVisible = this.filteredMarkers.includes(marker);
    });
    $(this.$target.resultCount).html(
      t(`numbers.${this.filteredMarkers.length}`, this.filteredMarkers.length)
    );
  }
};
export default mapSection;

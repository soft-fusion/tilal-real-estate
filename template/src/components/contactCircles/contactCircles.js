import $ from 'jquery';
const contactCircles = {
  settings: {
    target: '.contactCircles',
    openForm: '.-openForm',
    form: '.callBox',
    modal: '.customModal',
    close: '.contactCircles__close',
    formPlace: '.callBox__formPlace',
    callBox: '.callBox'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
      $(this.$target.callBox).css('max-width', `${window.innerWidth - 48}px`);
      $('.callBox__box').css(
        'max-height',
        `${this.$target.openForm.offset().top - 140}px`
      );
      $(window).on('resize', e => {
        $(this.$target.callBox).css('max-width', `${window.innerWidth - 48}px`);
        $('.callBox__box').css(
          'max-height',
          `${this.$target.openForm.offset().top - 140}px`
        );
      });
      if ($(window).width() <= 969) {
        //this.stickyCircleOnScroll();
        this.newStickyCircle();
      }
      $(window).on('resize scroll ', e => {
        if ($(window).width() <= 969) {
          //this.stickyCircleOnScroll();
          this.newStickyCircle();
        }
      });
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      openForm: target.find(settings.openForm),
      form: target.find(settings.form),
      close: target.find(settings.close),
      formPlace: target.find(settings.formPlace),
      callBox: target.find(settings.callBox),
      modal: $(settings.modal)
    };
    this.createCircle = true;
  },
  bindEvents() {
    $(this.$target.openForm).on('click', this.openModal.bind(this));
    $(this.$target.close).on('click', this.closeModal.bind(this));
    $(this.$target.modal).on('click', this.closeModal.bind(this));
  },
  stickyCircleOnScroll() {
    if ($(this.$target.root)[0].getBoundingClientRect().top < -80) {
      $(
        $(this.$target.openForm).parents('.contactCircles__circleBox')
      ).addClass('-sticky');
      $(this.$target.openForm).css(
        'box-shadow',
        '0 0 18px 8px rgba(0, 0, 0, 0.1)'
      );
      $($(this.$target.root).find('.-grey')).css('margin-left', '0px');
    } else {
      if (
        $(
          $(this.$target.openForm).parents('.contactCircles__circleBox')
        ).hasClass('animation')
      ) {
        if (
          $(window).height() -
            $(this.$target.root)[0].getBoundingClientRect().top <
          160
        ) {
          $($(this.$target.openForm).parents('.contactCircles__circleBox')).css(
            'top',
            `calc(100% - ${$(window).height() -
              $(this.$target.root)[0].getBoundingClientRect().top -
              80}px`
          );
        } else {
          $($(this.$target.openForm).parents('.contactCircles__circleBox')).css(
            'top',
            `0`
          );
        }
      }

      if (
        window.scrollY == 0 &&
        $(
          $(this.$target.openForm).parents('.contactCircles__circleBox')
        ).hasClass('-sticky')
      ) {
        $(
          $(this.$target.openForm).parents('.contactCircles__circleBox')
        ).removeClass('-sticky');
        $($(this.$target.openForm).parents('.contactCircles__circleBox')).css(
          'position',
          'fixed'
        );
        $(
          $(this.$target.openForm).parents('.contactCircles__circleBox')
        ).addClass('animation');
        $($(this.$target.openForm).parents('.contactCircles__circleBox')).css(
          'right',
          'calc(100% - 140px'
        );
        setTimeout(() => {
          $(
            $(this.$target.openForm).parents('.contactCircles__circleBox')
          ).removeAttr('style');
          $(
            $(this.$target.openForm).parents('.contactCircles__circleBox')
          ).removeClass('animation');
        }, 200);
        $($(this.$target.root).find('.-grey')).removeAttr('style');
      }
    }
  },
  openModal(event) {
    let button = event.currentTarget;
    if (!$(button).hasClass('-open')) {
      $(this.$target.form).slideDown('fast');
      $(this.$target.modal).fadeIn();
      $(button).addClass('-open');
    } else {
      this.closeModal(event);
    }
  },
  closeModal(event) {
    $(this.$target.form).slideUp('fast');
    $(this.$target.modal).fadeOut();
    $(this.$target.openForm).removeClass('-open');
  },
  newStickyCircle() {
    if (
      $(this.$target.root)[0].getBoundingClientRect().top < 0 &&
      this.createCircle
    ) {
      let circle = document.createElement('div');
      circle.className = 'contactCircles__circleBox -sticky';
      circle.style.opacity = 0;
      circle.innerHTML = `<div class="contactCircles__circle -openForm"> ${this.$target.openForm[0].innerHTML} </div>`;
      document.body.appendChild(circle);
      setTimeout(() => {
        circle.style.opacity = 1;
      }, 200);
      $($(circle).children('.contactCircles__circle')).on('click', () => {
        this.$target.openForm[0].click();
        $(circle).addClass('-open');
        $($(circle).children('.contactCircles__circle')).addClass('-open');
        $(this.$target.openForm).removeClass('-open');
        this.$target.callBox.addClass('-fixed');
      });
      $(this.$target.modal).on('click', () => {
        $(circle).removeClass('-open');
        $($(circle).children('.contactCircles__circle')).removeClass('-open');
        this.$target.callBox.removeClass('-fixed');
      });
      this.createCircle = false;
      this.checkPositionsOnScroll(circle, this.$target.openForm[0]);
    }
  },
  checkPositionsOnScroll(item, item2) {
    $(window).on('scroll', () => {
      if (item2.getBoundingClientRect().top < 0) {
        $(item).css('opacity', '1');
        $(item).css('visibility', 'visible');
      } else {
        $(item).css('opacity', '0');
        $(item).css('visibility', 'hidden');
      }
    });
  }
};
export default contactCircles;

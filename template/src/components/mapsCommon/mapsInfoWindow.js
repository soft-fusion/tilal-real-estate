import $ from 'jquery';
import { t } from '../../utils/i18n/i18n';
import { getShortenValue } from '../../utils/scripts/functions';

const mapsInfoWindow = marker => {
  const { title, price, types, image, bedrooms, bathrooms, url } = marker;
  let table = [];
  types.forEach(marker => {
    marker.forEach(element => {
      if (!table.includes(element)) {
        table.push(element);
      }
    });
  });
  const typesText = $('body').hasClass('rtl')
    ? `${table.map(type => t(`home-type.${type}`)).join(' ,')}`
    : `${table.map(type => t(`home-type.${type}`)).join(', ')}`;
  const priceText = $('body').hasClass('rtl')
    ? `${getShortenValue(price[1])} - ${getShortenValue(price[0])}`
    : `${getShortenValue(price[0])} - ${getShortenValue(price[1])}`;

  const emptyText = $('body').hasClass('rtl') ? `قريبا` : `Coming Soon`;
  const soon = $('body').hasClass('rtl') ? `قريبا` : `SOON`;
  let priceIf = t('currencies.sar', 'SAR');
  if (bedrooms[0] === 0 && bathrooms[0] === 0 && price[0] === 0) {
    return `<a href="#popup" class="maps__infoWindow maps__linkHover -open-modal">
    <div class="maps__soon">${soon}</div>
    <img class="maps__image" src="${image}" alt="${title}" />
    <div class="maps__main">
      <div class="maps__title">${title}</div>
      <div class="maps__grid">
        <div class="maps__element">
          <div class="maps__label">From</div>
          <div class="maps__value">
          ${emptyText}
          </div>
        </div>
        <div class="maps__element">
          <div class="maps__label">Type</div>
          <div class="maps__value">
            ${emptyText}
          </div>
        </div>
        <div class="maps__element">
          <div class="maps__label">Bedrooms</div>
          <div class="maps__value">
          ${emptyText}
            </div>
        </div>
        <div class="maps__element">
          <div class="maps__label">Bathrooms</div>
          <div class="maps__value">
          ${emptyText}
          </div>
        </div>
      </div>
    </div>
  </a>`;
  } else {
    return `<a href="${url}" class="maps__infoWindow">
    <img class="maps__image" src="${image}" alt="${title}" />
    <div class="maps__main">
      <div class="maps__title">${title}</div>
      <div class="maps__grid">
        <div class="maps__element">
          <div class="maps__label">From</div>
          <div class="maps__value">

           ${priceIf} ${priceText}
          </div>
        </div>
        <div class="maps__element">
          <div class="maps__label">Type</div>
          <div class="maps__value">${typesText}</div>
        </div>
        <div class="maps__element">
          <div class="maps__label">Bedrooms</div>
          <div class="maps__value">${getShortenValue(
            bedrooms[0]
          )} - ${getShortenValue(bedrooms[1])}</div>
        </div>
        <div class="maps__element">
          <div class="maps__label">Bathrooms</div>
          <div class="maps__value">${getShortenValue(
            bathrooms[0]
          )} - ${getShortenValue(bathrooms[1])}</div>
        </div>
      </div>
    </div>
  </a>`;
  }
};

export default mapsInfoWindow;

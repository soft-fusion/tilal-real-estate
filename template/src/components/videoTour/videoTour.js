import $ from 'jquery';

const videoTour = {
  settings: {
    target: '.videoTour',
    playButton: '#play_button',
    video: '#video'
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      playButton: target.find(settings.playButton),
      video: target.find(settings.video)
    };
  },
  bindEvents() {
    const playButton = $(this.$target.playButton)[0];
    const video = $(this.$target.video)[0];
    $(this.$target.playButton).on('click', function() {
      video.play();
    });
    $(this.$target.video).on('click', function() {
      if (video.playing) {
        video.pause();
      } else {
        video.play();
      }
    });
    $(this.$target.video).on('play', function() {
      $(playButton).css('display', 'none');
      $(video).attr({ controls: true });
    });
    $(this.$target.video).on('pause', function() {
      $(playButton).removeAttr('style');
      $(video).attr({ controls: false });
    });
  }
};
export default videoTour;

import $ from 'jquery';

const buttonSection = {
  settings: {
    target: '.buttonSection',
    button: '.buttonSection__button.-open-modal',
    modal: '.buttonSection__popup',
    formInputs: '.buttonSection__popup form input[type="text"]',
    formTextarea: '.buttonSection__popup form textarea',
    buttonClose: '.buttonSection__close',
    buttonSection: '.buttonSection__buttonHide.-open-modal'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      button: target.find(settings.button),
      modal: target.find(settings.modal),
      formInputs: target.find(settings.formInputs),
      formTextarea: target.find(settings.formTextarea),
      buttonClose: target.find(settings.buttonClose),
      buttonSection: target.find(settings.buttonSection)
    };
  },
  bindEvents() {
    $(this.$target.button).on('click', this.clickButtonForm.bind(this));
    $(this.$target.buttonSection).on('click', this.openModal.bind(this));
    $(this.$target.modal).on('click', this.closeModal.bind(this));
    $(this.$target.buttonClose).on('click', this.closeModalButton.bind(this));
    $(this.$target.formInputs).on('input', this.hideLabels.bind(this));
    $(this.$target.formTextarea).on('input', this.hideLabels.bind(this));
  },
  hideLabels(e) {
    const target = $(e.currentTarget);
    const label = $(target)
      .parents('li')
      .find('.zf-labelName');
    if ($(target).val().length > 0) {
      $(label).css('display', 'none');
    } else {
      $(label).css('display', '');
    }
  },
  clickButtonForm(e) {
    e.preventDefault();
    $('.buttonSection__buttonHide.-open-modal')[0].click();
  },
  openModal(e) {
    e.preventDefault();
    let modal = $(e.currentTarget).siblings('.buttonSection__popup');
    if (modal.length > 0) {
      $(modal).removeClass('-close');
      document.body.style.overflow = 'hidden';
      document.body.style.height = '100%';
      if (
        !$(modal)
          .find('iframe')
          .attr('src')
      ) {
        $(modal)
          .find('iframe')
          .attr(
            'src',
            $(modal)
              .find('iframe')
              .attr('data-src')
          );
        if ($('.buttonSection__placeholder').length === 0) {
          $(modal)
            .find('.buttonSection__popupBox')
            .append('<div class="buttonSection__placeholder"></div>"');
        }
        $(modal)
          .find('iframe')
          .on('load', function() {
            setTimeout(() => {
              $(this)
                .removeClass('-hidden')
                .css('height', '100%');
              $('.buttonSection__placeholder').addClass('-hidden');
            }, 250);
          });
      }
    }
  },
  closeModal(e) {
    if ($(e.target).hasClass('buttonSection__popup')) {
      $(e.target).addClass('-close');
      document.body.style.overflow = 'auto';
      document.body.style.height = 'auto';
    }
  },
  closeModalButton(e) {
    $(e.currentTarget)
      .parents('.buttonSection__popup')
      .addClass('-close');
    document.body.style.overflow = 'auto';
    document.body.style.height = 'auto';
  }
};
export default buttonSection;

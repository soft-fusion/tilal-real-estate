import $ from 'jquery';
const header = {
  settings: {
    target: '.header',
    toggle: '.-menuOpen',
    right: '.header__right'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      toggle: target.find(settings.toggle),
      right: target.find(settings.right)
    };
  },
  bindEvents() {
    $(this.$target.toggle).on('click', this.toggleMenu.bind(this));
    let lastPositionY = 0;
    window.onscroll = () => {
      if (window.scrollY < lastPositionY) {
        $(this.$target.root).addClass('-whiteBg');
        if (window.pageYOffset > 0) {
          $(this.$target.root).addClass('-whiteBg');
        } else {
          $(this.$target.root).removeClass('-whiteBg');
        }
      } else {
        if (!$(this.$target.root).hasClass('-alwaysWhite')) {
          $(this.$target.root).removeClass('-whiteBg');
        }
      }
      lastPositionY = window.scrollY;
    };
  },
  toggleMenu(event) {
    let button = event.currentTarget;
    if ($(button).hasClass('-open')) {
      $(button).removeClass('-open');
      $(this.$target.right).removeClass('-visible');
      $(this.$target.root).removeClass('-openMobileMenu');
      if (
        !$(this.$target.root).hasClass('-alwaysWhite') &&
        window.pageYOffset == 0
      ) {
        $(this.$target.root).removeClass('-whiteBg');
      }
      document.body.style.overflow = 'auto';
      document.body.style.height = 'auto';
    } else {
      $(button).addClass('-open');
      $(this.$target.right).addClass('-visible');
      $(this.$target.root).addClass('-whiteBg');
      $(this.$target.root).addClass('-openMobileMenu');
      document.body.style.overflow = 'hidden';
      document.body.style.height = '100%';
    }
  }
};
export default header;

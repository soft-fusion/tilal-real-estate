import $ from 'jquery';

const animations = {
  settings: {
    target: 'body',
    contactMap: '.contact__map',
    contactLeft: '.contact__left',
    contactText: '.contact__left > div',
    contactRight: '.contact__right',

    headerContainer: '.header__container',
    newsletter: '.newsletter',
    hr: '.customHr',
    newsletterContent: '.newsletter__container > div',

    bannerImg: '.banner__photoInBg',
    bannerShape: '.banner__shape',
    bannerTitle: '.banner__title',
    bannerLogo: '.banner__logo',
    bannerScrollBox: '.banner__scrollBox',
    bannerCircles: '.contactCircles',
    bannerButtons: '.contactButtons',

    careerText: '.careerInfo__text',
    careerBox: '.careerInfo__box',
    careerSmallText: '.careerInfo__smallText',
    careerScroll: '.careerInfo__scroll',
    careerImg: '.careerInfo__img',

    jobsOffersTitle: '.jobsOffers__title',

    boardMembersName: '.board__name',
    boardMembersInfo: '.board__info',
    boardMembersTitle: '.board__title',
    boardMembersImg: '.board__person img',

    imagesSectionImg: '.imagesSection__img',
    imagesSectionText: '.imagesSection__text',
    imagesSectionCaption: '.imagesSection__caption',
    imageTextText: '.imageText__right',
    imageTextTitle: '.imageText__title',
    imageTextImg: '.imageText__image',

    aboutUsItem: '.aboutUsItem',
    aboutUsTitle: '.aboutUs__title',
    aboutUsImg: '.aboutUs__image',

    promiseSectionItem: '.promiseSectionItem',
    promiseSectionTitle: '.promiseSection__title',
    promiseSectionImg: '.promiseSection__image',
    promiseSectionSlogan: '.promiseSection__slogan',

    buttonSectionButton: '.buttonSection__button',

    textText: '.textSection__text',
    textBox: '.textSection__box',
    textSmallText: '.textSection__smallText',
    textScroll: '.textSection__scroll',
    textImg: '.textSection__img',

    aboutContentText: '.aboutContent__text',
    aboutContentBox: '.aboutContent__box',

    iconTextGridIcon: '.iconTextGrid__icon',
    iconTextGridTitle: '.iconTextGrid__title',
    iconTextGridText: '.iconTextGrid__text',

    joinOurVipListLeft: '.joinOurVipList__left',
    joinOurVipListRight: '.joinOurVipList__right',
    joinOurVipListTitle: '.joinOurVipList__title',

    mapSectionTitle: '.mapSection__title',
    mapSectionMap: '.mapSection__map',

    tlrBox: '.tlr__box',
    tlrLink: '.tlr__link',

    logosTitle: '.logos__title',
    logosItem: '.logos__item',

    tabsItem: '.tabs__item',

    detailsGridItem: '.detailsGrid__item',

    textListTag: '.textList__tag',
    textListTitle: '.textList__title',
    textListText: '.textList__text',
    textListTag2: '.textList__tag2',
    textListLi: '.textList__list li',
    textListButtonWrapper: '.textList__buttonWrapper',

    scatterGalleryImage: '.scatterGallery__image img',
    scatterGalleryCaption: '.scatterGallery__caption',

    videoTourPlayer: '.videoTour__player',
    videoTourButton: '.videoTour__button',
    videoTourText: '.videoTour__text',

    interactiveMapTitle: '.interactiveMap__title',
    interactiveMapMap: '.interactiveMap__map',
    interactiveMapPlaceholder: '.interactiveMap__placeholder',

    homesGridTitle: '.homesGrid__title',
    homesGridItem: '.homesGrid__item',

    investmentGalleryTitle: '.investmentGallery__title',
    investmentGalleryImage: '.investmentGallery__image',
    investmentGalleryText: '.investmentGallery__text',

    floorPlanTitle: '.floorPlan__title',
    floorPlanButtonHolder: '.floorPlan__buttonHolder',
    floorPlanImageHolder: '.floorPlan__imageHolder',
    floorPlanText: '.floorPlan__text',
    floorPlanTag: '.floorPlan__tag',
    floorPlanListItem: '.floorPlan__list li',

    investmentMap: '.investmentMap__map',

    projectsGallerySlide: '.projectsGallery__slide',
    projectsGalleryNav: '.projectsGallery__nav',

    textListNarrowLeft: '.textListNarrow__left',
    textListNarrowRight: '.textListNarrow__right',
    textListNarrowTitle: '.textListNarrow__title'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.scrollOnlyDown();
      this.animation();
      this.bindEvents();
    }
  },
  bindEvents() {
    $(window).on('scroll', this.animation.bind(this));
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {};
    for (const key in settings) {
      Object.assign(this.$target, {
        [key]: target.find(settings[key])
      });
    }
  },
  scrollOnlyDown() {
    setTimeout(() => {
      $.each(this.$target, (index, item) => {
        $.each(item, (index2, item2) => {
          if (item2.getBoundingClientRect().top < window.scrollY) {
            $(item2).addClass('active');
          }
        });
      });
    }, 200);
  },
  animation() {
    $.each(this.$target, (index, item) => {
      $.each(item, (index2, item2) => {
        if ($(item2).isInViewport() && !$(item2).hasClass('active')) {
          $(item2).addClass('active');
        }
      });
    });
    if ($('.jobsOffers__offers').length > 0) {
      if (
        $('.jobsOffers__offers').isInViewport() &&
        !$('.jobsOffers__offers').hasClass('active')
      ) {
        let jobs = $('.jobsOffers__offers').find('.rec-job-info');
        if (jobs.length > 0) {
          $.each(jobs, (index, item) => {
            if ($(item).isInViewport() && !$(item).hasClass('active')) {
              $(item).addClass('active');
            }
          });
          let counter = 0;
          $.each(jobs, (index, item) => {
            if ($(item).hasClass('active')) {
              counter++;
            }
          });
          if (counter === jobs.length) {
            $('.jobsOffers__offers').addClass('active');
          }
        }
      }
      if (
        $('.jobsOffers__offers')[0].getBoundingClientRect().top < 0 &&
        !$('.jobsOffers__offers').hasClass('active')
      ) {
        let jobs = $('.jobsOffers__offers').find('.rec-job-info');
        if (jobs.length > 0) {
          $.each(jobs, (index, item) => {
            $(item).addClass('active');
          });
        }
      }
    }
  }
};
export default animations;
$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};

import $ from 'jquery';
const contactButtons = {
  settings: {
    target: '.contactButtons',
    salesButton: '.contactButtons__button.-sales',
    vipButton: '.contactButtons__button.-vip',
    buttonClose: '.buttonSection__close'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      salesButton: target.find(settings.salesButton),
      vipButton: target.find(settings.vipButton),
      buttonClose: $(settings.buttonClose)
    };
  },
  bindEvents() {
    $(this.$target.salesButton).on('click', () => {
      this.openOffcanvas('sales');
    });
    $(this.$target.vipButton).on('click', this.openModal.bind(this));
    $(this.$target.buttonClose).on('click', this.closeModalButton.bind(this));
    $(window).on('scroll resize', this.makeStickyInTabs.bind(this));
  },
  openOffcanvas(type) {
    const offcanvas = $(`.contactOffcanvas.-${type}`);
    offcanvas.addClass('-open');
    document.body.style.overflow = 'hidden';
  },
  makeStickyInTabs() {
    if ($('.tabs.-sticky').length) {
      $(this.$target.root).addClass('-sticky');
    } else {
      $(this.$target.root).removeClass('-sticky');
    }
  },
  openModal(e) {
    e.preventDefault();
    let modal = $(e.currentTarget)
      .parents(this.settings.target)
      .siblings('.buttonSection__popup');
    if (modal.length > 0) {
      $(modal).removeClass('-close');
      document.body.style.overflow = 'hidden';
      document.body.style.height = '100%';
      if (
        !$(modal)
          .find('iframe')
          .attr('src')
      ) {
        $(modal)
          .find('iframe')
          .attr(
            'src',
            $(modal)
              .find('iframe')
              .attr('data-src')
          );
        if ($('.buttonSection__placeholder').length === 0) {
          $(modal)
            .find('.buttonSection__popupBox')
            .append('<div class="buttonSection__placeholder"></div>"');
        }
        $(modal)
          .find('iframe')
          .on('load', function() {
            setTimeout(() => {
              $(this)
                .removeClass('-hidden')
                .css('height', '100%');
              $('.buttonSection__placeholder').addClass('-hidden');
            }, 250);
          });
      }
    }
  },
  closeModalButton(e) {
    $(e.currentTarget)
      .parents('.buttonSection__popup')
      .addClass('-close');
    document.body.style.overflow = 'auto';
    document.body.style.height = 'auto';
  }
};
export default contactButtons;

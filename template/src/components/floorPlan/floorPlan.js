import $ from 'jquery';
const floorPlan = {
  settings: {
    target: '.floorPlan',
    button: '.floorPlan__button',
    image: '.floorPlan__image'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      button: target.find(settings.button),
      image: target.find(settings.image)
    };
  },
  bindEvents() {
    $(this.$target.button).each((index, item) => {
      $(item).on('click', () => {
        $(this.$target.button).removeClass('-active');
        $(this.$target.image).removeClass('-active');
        $(item).addClass('-active');
        $(this.$target.image)
          .eq(index)
          .addClass('-active');
      });
    });
  }
};
export default floorPlan;

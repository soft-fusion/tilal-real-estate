import $ from 'jquery';
import Swiper from 'swiper';

const projectsGallery = {
  settings: {
    target: '.projectsGallery',
    background: '.projectsGallery__bg',
    swiper: '.projectsGallery__swiper',
    slide: '.projectsGallery__slide',
    link: '.projectsGallery__link.-open-modal',
    prevArrow: '.projectsGallery__nav.-prev',
    nextArrow: '.projectsGallery__nav.-next'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
      if (this.$target.slide.length > 1) {
        this.initSwiper();
      }
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      background: target.find(settings.background),
      swiper: target.find(settings.swiper),
      slide: target.find(settings.slide),
      prevArrow: target.find(settings.prevArrow),
      nextArrow: target.find(settings.nextArrow)
    };
  },
  bindEvents() {
    $(this.settings.link).on('click', function(e) {
      e.preventDefault();
      $('.buttonSection__button.-open-modal')[0].click();
    });
  },
  initSwiper() {
    const swiper = new Swiper(this.settings.swiper, {
      autoplay: false,
      loop: false,
      navigation: {
        nextEl: '.projectsGallery__nav.-next',
        prevEl: '.projectsGallery__nav.-prev'
      },
      slidesPerView: 1,
      spaceBetween: 30,
      speed: 300,
      breakpoints: {
        1025: {
          allowTouchMove: false,
          slidesPerView: 1.5,
          spaceBetween: 30
        },
        1281: {
          allowTouchMove: false,
          slidesPerView: 2.5,
          spaceBetween: 60
        }
      }
    });

    swiper.on('slideChangeTransitionStart', swiper => {
      const newSlide = $(this.$target.slide).eq(swiper.activeIndex);
      $(this.$target.background).attr(
        'class',
        `projectsGallery__bg -${newSlide.attr('data-bg')}`
      );
    });

    swiper.on('slideChange', swiper => {
      if (swiper.activeIndex === 0) {
        $(this.$target.prevArrow).addClass('-hidden');
      } else {
        $(this.$target.prevArrow).removeClass('-hidden');
      }
      if (swiper.activeIndex === $(this.$target.slide).length - 1) {
        $(this.$target.nextArrow).addClass('-hidden');
      } else {
        $(this.$target.nextArrow).removeClass('-hidden');
      }
    });

    $(this.$target.prevArrow).on('click', () => {
      swiper.slidePrev();
    });
    $(this.$target.nextArrow).on('click', () => {
      swiper.slideNext();
    });
  }
};

export default projectsGallery;

import $ from 'jquery';
import store from 'store2';
import autocomplete from '../../utils/scripts/autocomplete';

const searchOffCanvas = {
  settings: {
    target: '.searchOffCanvas',
    input: '.searchOffCanvas__input',
    citiesWrapper: '.searchOffCanvas__cities'
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.cities = store.session('cities');
      this.initAutocomplete();
      this.renderCities();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      input: target.find(this.settings.input),
      citiesWrapper: target.find(this.settings.citiesWrapper)
    };
  },
  initAutocomplete() {
    autocomplete($(this.$target.input).eq(0), this.cities, {
      optionAsLink: true
    });
  },
  renderCities() {
    [...this.cities]
      .sort((a, b) => (a.name < b.name ? -1 : a.name > b.name ? 1 : 0))
      .forEach(city => {
        const link = $('<a>', {
          class: 'searchOffCanvas__city',
          href: city.url
        });
        link.html(city.name);
        $(this.$target.citiesWrapper).append(link);
      });
  }
};
export default searchOffCanvas;

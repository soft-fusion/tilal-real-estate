import $ from 'jquery';
import { t } from '../../utils/i18n/i18n';
import { getShortenValue } from '../../utils/scripts/functions';

const getProjectTemplate = (project, index) => {
  const {
    title,
    price,
    types,
    image,
    location,
    bedrooms,
    bathrooms,
    url
  } = project;
  const template = $('.cityResults__item.-template').clone(true, true);
  template.removeClass('-template');
  let table = [];
  types.forEach(project => {
    project.forEach(element => {
      if (!table.includes(element)) {
        table.push(element);
      }
    });
  });

  const typesText = $('body').hasClass('rtl')
    ? `${table.map(type => t(`home-type.${type}`)).join(' ,')}`
    : `${table.map(type => t(`home-type.${type}`)).join(', ')}`;

  const priceText = $('body').hasClass('rtl')
    ? `${getShortenValue(price[1])} - ${getShortenValue(price[0])}`
    : `${getShortenValue(price[0])} - ${getShortenValue(price[1])}`;

  template.attr({
    'data-position': index,
    'data-name': title,
    'data-price': price[1]
  });

  const emptyText = $('body').hasClass('rtl') ? `قريبا` : `Coming Soon`;

  template
    .find('[data-slot="image"]')
    .attr('style', `background-image:url("${image}")`);
  template.find('[data-slot="title"]').html(title);
  template.find('[data-slot="city"]').html(location);

  if (bedrooms[0] === 0 && bathrooms[0] === 0 && price[0] === 0) {
    template.find('[data-slot="home-type"]').html(emptyText);
    template.find('[data-slot="price"]').html(emptyText);
    template.find('[data-slot="bedrooms"]').html(emptyText);
    template.find('[data-slot="bathrooms"]').html(emptyText);
    template.find('[data-slot="url"]').hide();
  } else {
    template.find('[data-slot="home-type"]').html(typesText);
    template
      .find('[data-slot="price"]')
      .html(`${t('currencies.sar', 'SAR')} ${priceText}`);
    template
      .find('[data-slot="bedrooms"]')
      .html(
        `${getShortenValue(bedrooms[0])} - ${getShortenValue(bedrooms[1])}`
      );
    template
      .find('[data-slot="bathrooms"]')
      .html(
        `${getShortenValue(bathrooms[0])} - ${getShortenValue(bathrooms[1])}`
      );
    template.find('[data-slot="url"]').attr('href', url);
  }

  return template;
};

export default getProjectTemplate;

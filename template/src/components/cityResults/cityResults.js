import $ from 'jquery';
import 'select2';
import store from 'store2';
import { t } from '../../utils/i18n/i18n';
import mapsInfoWindow from '../mapsCommon/mapsInfoWindow';
import mapsStyles from '../mapsCommon/mapsStyles';
import getProjectTemplate from './cityResultsTemplate';

const cityResults = {
  projects: [],
  mapMarkers: [],
  settings: {
    target: '.cityResults',
    loader: '.cityResults__loader',
    main: '.cityResults__main',
    selects: '.cityResults__select',
    container: '.cityResults__projects',
    item: '.cityResults__item',
    switch: '.cityResults__switch',
    filters: '.cityFilters',
    locationFilter: 'input[name="location"]',
    homeTypeFilter: 'select[name="home-type"]',
    priceFromFilter: 'input[name="price-from"]',
    priceToFilter: 'input[name="price-to"]',
    bedroomsFilter: 'select[name="bedrooms"]',
    bathroomsFilter: 'select[name="bathrooms"]'
  },
  async init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.initSelect2();
      this.bindEvents();
      this.projects = store.session('projects');
      this.projects = this.projects.filter(
        p =>
          p.location.toLowerCase() ===
          $(this.$target.locationFilter)
            .val()
            .toLowerCase()
      );
      this.renderProjects();
      $(this.$target.loader).addClass('-hidden');
      this.initMap();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      loader: target.find(this.settings.loader),
      main: target.find(this.settings.main),
      selects: target.find(this.settings.selects),
      container: target.find(this.settings.container),
      item: target.find(this.settings.item),
      switch: target.find(this.settings.switch),
      filters: $(this.settings.filters),
      locationFilter: $(this.settings.filters).find(
        this.settings.locationFilter
      )
    };
  },
  bindEvents() {
    $(this.$target.switch).on('click', e => {
      const target = $(e.currentTarget);
      $(this.$target.switch).removeClass('-active');
      $(target).addClass('-active');
      $(
        this.$target.main
      )[0].classList = `cityResults__main ${e.currentTarget.dataset.view}`;
    });
    $(this.$target.selects).on('change', this.sortResults.bind(this));

    $(this.$target.filters)
      .find(this.settings.homeTypeFilter)
      .on('change', this.filterProjects.bind(this));
    $(this.$target.filters)
      .find(this.settings.priceFromFilter)
      .on('blur', this.filterProjects.bind(this));
    $(this.$target.filters)
      .find(this.settings.priceToFilter)
      .on('blur', this.filterProjects.bind(this));
    $(this.$target.filters)
      .find(this.settings.bedroomsFilter)
      .on('change', this.filterProjects.bind(this));
    $(this.$target.filters)
      .find(this.settings.bathroomsFilter)
      .on('change', this.filterProjects.bind(this));
  },
  renderProjects() {
    this.projects.forEach((project, index) => {
      const template = getProjectTemplate(project, index);
      $(this.$target.container).append(template);
    });

    const itemCount = $(this.settings.item).filter(
      ':not(.-template):not(.-hidden)'
    ).length;
    const countNumber = t(`numbers.${itemCount}`, `${itemCount}`);
    $('.cityResults__countNumber').html(countNumber);
  },
  filterProjects() {
    const filters = this.prepareFilters();
    const filtered = this.projects.filter(p => {
      if (
        filters['home-type'] &&
        !p.types.some(t => t.includes(filters['home-type']))
      ) {
        return false;
      }
      if (
        (filters['price-from'] || filters['price-from'] === 0) &&
        filters['price-from'] > p.price[1]
      ) {
        return false;
      }
      if (
        (filters['price-to'] || filters['price-to'] === 0) &&
        filters['price-to'] < p.price[0]
      ) {
        return false;
      }
      if (filters.bedrooms && filters.bedrooms > p.bedrooms[1]) {
        return false;
      }
      if (filters.bathrooms && filters.bathrooms > p.bathrooms[1]) {
        return false;
      }
      return true;
    });

    this.projects.forEach((p, index) => {
      const item = $(this.settings.item).filter(':not(.-template)')[index];
      if (!filtered.includes(p)) {
        p.isVisible = false;
        item.classList.add('-hidden');
      } else {
        p.isVisible = true;
        item.classList.remove('-hidden');
      }
    });

    const itemCount = $(this.settings.item).filter(
      ':not(.-template):not(.-hidden)'
    ).length;
    const countNumber = t(`numbers.${itemCount}`, `${itemCount}`);
    $('.cityResults__countNumber').html(countNumber);
    this.hideMarkers();
  },
  sortResults(e) {
    const value = e.currentTarget.value.split('-');
    const key = value[0];
    const order = value[1];
    $(this.$target.container)
      .find(this.settings.item)
      .sort((a, b) => {
        const aValue = $(a).attr(`data-${key}`);
        const bValue = $(b).attr(`data-${key}`);
        if (order === 'asc') {
          return aValue < bValue ? -1 : aValue > bValue ? 1 : 0;
        } else if (order === 'desc') {
          return aValue > bValue ? -1 : aValue < bValue ? 1 : 0;
        }
      })
      .appendTo(this.$target.container);
  },
  initSelect2() {
    $(this.$target.selects).each((index, item) => {
      $(item).select2({
        minimumResultsForSearch: -1,
        width: 'auto'
      });
    });
  },
  prepareFilters() {
    const filters = {};
    const homeType = $(this.$target.filters)
      .find(this.settings.homeTypeFilter)
      .val();
    const priceFrom = $(this.$target.filters)
      .find(this.settings.priceFromFilter)
      .val();
    const priceTo = $(this.$target.filters)
      .find(this.settings.priceToFilter)
      .val();
    const bedrooms = $(this.$target.filters)
      .find(this.settings.bedroomsFilter)
      .val();
    const bathrooms = $(this.$target.filters)
      .find(this.settings.bathroomsFilter)
      .val();

    filters['home-type'] = homeType;
    filters['price-from'] =
      priceFrom === $('.cityFilters__bar').attr('data-start')
        ? undefined
        : parseInt(priceFrom, 10);
    filters['price-to'] =
      priceTo === $('.cityFilters__bar').attr('data-end')
        ? undefined
        : parseInt(priceTo, 10);
    filters.bedrooms = bedrooms === '' ? '' : parseInt(bedrooms, 10);
    filters.bathrooms = bathrooms === '' ? '' : parseInt(bathrooms, 10);

    Object.keys(filters).forEach(key => {
      if (
        filters[key] === undefined ||
        filters[key] === '' ||
        filters[key].length === 0
      ) {
        delete filters[key];
      }
    });
    return filters;
  },
  hideMarkers() {
    this.mapMarkers.forEach((marker, index) => {
      marker.setVisible(this.projects[index].isVisible);
    });
  },
  initMap() {
    let infoWindow;
    window.googleMapsLoader.load().then(google => {
      $('.mapSection__placeholder').addClass('-hidden');
      const avgLat =
        this.projects.reduce(
          (acc, marker) => parseFloat(acc) + parseFloat(marker.position[0]),
          0
        ) / this.projects.length;
      const avgLng =
        this.projects.reduce(
          (acc, marker) => parseFloat(acc) + parseFloat(marker.position[1]),
          0
        ) / this.projects.length;
      const map = new google.maps.Map(document.getElementById('city-map'), {
        zoom: 10,
        fullscreenControl: false,
        fullscreenControlOptions: false,
        zoomControl: true,
        zoomControlOptions: false,
        clickableIcons: true,
        disableDefaultUI: true,
        disableDoubleClickZoom: true,
        draggable: true,
        draggableCursor: 'default',
        center: new google.maps.LatLng(avgLat, avgLng),
        styles: mapsStyles
      });
      map.addListener('click', () => {
        if (infoWindow) {
          infoWindow.close();
        }
      });
      this.projects.forEach(item => {
        let marker = new google.maps.Marker({
          position: new google.maps.LatLng(...item.position),
          map: map,
          icon: `${location.origin}/wp-content/themes/tilal/assets/images/markerHP-fit.svg`
        });
        this.mapMarkers.push(marker);
        marker.addListener('click', () => {
          if (infoWindow) {
            infoWindow.close();
          }
          infoWindow = new google.maps.InfoWindow({
            content: mapsInfoWindow(item)
          });
          infoWindow.open({
            anchor: marker,
            map,
            shouldFocus: false
          });
        });
      });
    });
  }
};
export default cityResults;

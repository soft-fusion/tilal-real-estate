import $ from 'jquery';

const joinOurVipList = {
  settings: {
    target: '.joinOurVipList',
    formInputs:
      '.joinOurVipList__right form .zcwf_col_fld input:not([type="submit"]):not([type="reset"])',
    returnURL: '.joinOurVipList__right input[name="returnURL"]',
    nameInput: '.joinOurVipList__right input[name="Last Name"]',
    phoneInput: '.joinOurVipList__right input[name="Phone"]'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.pageInit();
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      formInputs: target.find(settings.formInputs),
      returnURL: target.find(settings.returnURL),
      nameInput: target.find(settings.nameInput),
      phoneInput: target.find(settings.phoneInput)
    };
  },
  pageInit() {
    $.each(this.$target.formInputs, (index, item) => {
      const label = $(item)
        .parents('.zcwf_row')
        .find('.zcwf_col_lab');
      if ($(item).val().length > 0) {
        $(label).css('display', 'none');
      } else {
        $(label).css('display', '');
      }
    });

    const returnURL = window.location.href;
    $(this.$target.returnURL).val(returnURL);
  },
  bindEvents() {
    $(this.$target.formInputs).on('input', this.hideLabels.bind(this));
    $(this.$target.nameInput).on('keydown paste', this.allowName.bind(this));
    $(this.$target.phoneInput).on('keydown paste', this.allowPhone.bind(this));
  },
  hideLabels(e) {
    const target = $(e.currentTarget);
    const label = $(target)
      .parents('.zcwf_row')
      .find('.zcwf_col_lab');
    if ($(target).val().length > 0) {
      $(label).css('display', 'none');
    } else {
      $(label).css('display', '');
    }
  },
  allowName(e) {
    const regex = /^[A-Za-z\u0621-\u064A '.-]+$/;
    if (e.type === 'keydown') {
      if (
        !regex.test(e.key) &&
        e.key !== 'Tab' &&
        e.key !== 'Backspace' &&
        e.key !== 'Enter' &&
        e.key !== 'Control'
      ) {
        e.preventDefault();
      }
    } else if (e.type === 'paste') {
      if (
        !regex.test(
          (
            e.clipboardData ||
            e.originalEvent.clipboardData ||
            window.clipboardData
          ).getData('text')
        )
      ) {
        e.preventDefault();
      }
    }
  },
  allowPhone(e) {
    const regex = /^[0-9]+$/;
    if (e.type === 'keydown') {
      if (
        !regex.test(e.key) &&
        e.key !== 'Tab' &&
        e.key !== 'Backspace' &&
        e.key !== 'Enter' &&
        e.key !== 'Control'
      ) {
        e.preventDefault();
      }
    } else if (e.type === 'paste') {
      if (
        !regex.test(
          (
            e.clipboardData ||
            e.originalEvent.clipboardData ||
            window.clipboardData
          ).getData('text')
        )
      ) {
        e.preventDefault();
      }
    }
  }
};
export default joinOurVipList;

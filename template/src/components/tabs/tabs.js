import $ from 'jquery';

const tabs = {
  settings: {
    target: '.tabs',
    item: '.tabs__item',
    trigger: '.tabs__trigger'
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
      this.makeSticky();
      this.changeActiveOnScroll();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      item: target.find(settings.item),
      trigger: target.find(settings.trigger)
    };
  },
  bindEvents() {
    $(this.$target.item).on('click', this.scrollTo.bind(this));
    $(this.$target.trigger).on('click', this.toggleNavigation.bind(this));
    $(window).on('scroll', this.makeSticky.bind(this));
    $(window).on('scroll', this.changeActiveOnScroll.bind(this));
  },
  scrollTo(e) {
    const target = $(e.currentTarget);
    $(this.$target.item).removeClass('-active');
    $(target).addClass('-active');

    const scrollToId = $(target).attr('data-scrollto-id');
    const section = $(`[data-scrolling-id="${scrollToId}"]`)[0];
    if (section) {
      const top = $(section).offset().top;
      window.scrollTo({
        top: top - 50,
        behavior: 'smooth'
      });
    }
  },
  makeSticky() {
    const bar = $(this.$target.root)[0];
    const top = bar.getBoundingClientRect().top;
    if (top <= 0) {
      $(bar).addClass('-sticky');
    } else {
      $(bar).removeClass('-sticky');
    }
  },
  changeActiveOnScroll() {
    const scrollTop = $(window).scrollTop();
    $(this.$target.item).each((index, item) => {
      const scrollToId = $(item).attr('data-scrollto-id');
      const scrollToElement = $(`[data-scrolling-id="${scrollToId}"]`)[0];
      if (
        $(scrollToElement).length &&
        $(scrollToElement).position().top - 51 <= scrollTop
      ) {
        $(this.$target.item).removeClass('-active');
        $(item).addClass('-active');
      }
    });
  },
  toggleNavigation(e) {
    $(e.currentTarget).toggleClass('-open');
    $(e.currentTarget)
      .siblings('.tabs__list')
      .toggleClass('-open');
  }
};
export default tabs;

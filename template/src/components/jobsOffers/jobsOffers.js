import $ from 'jquery';
const jobsOffers = {
  settings: {
    target: '.jobsOffers'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target
    };
  },
  bindEvents() {
    setTimeout(() => {
      $(this.$target.root)
        .find('.rec-job-info')
        .on('click', this.openLink.bind(this));
    }, 1500);
  },
  openLink(event) {
    let item = $(event.currentTarget);
    let link = item.find('a');
    event.preventDefault();
    window.open(link[0].href, '_blank').focus();
  }
};
export default jobsOffers;

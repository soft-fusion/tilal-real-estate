import $ from 'jquery';

const newsletter = {
  settings: {
    target: '.newsletter',
    buttonDesktop: '.newsletter__button.-desktop',
    buttonMobile: '.newsletter__button.-mobile'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.originalData = {
        id: $(this.$target.buttonDesktop).attr('id'),
        name: $(this.$target.buttonDesktop).attr('name')
      };
      if ($(window).width() < 768) {
        this.changeData('mobile');
      } else {
        this.changeData('desktop');
      }
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      buttonDesktop: target.find(settings.buttonDesktop),
      buttonMobile: target.find(settings.buttonMobile)
    };
  },
  bindEvents() {
    $(window).on('resize', () => {
      if ($(window).width() < 768) {
        this.changeData('mobile');
      } else {
        this.changeData('desktop');
      }
    });
  },
  changeData(type) {
    if (type === 'desktop') {
      $(this.$target.buttonMobile).attr('id', `${this.originalData.id}_other`);
      $(this.$target.buttonDesktop).attr('id', this.originalData.id);
      $(this.$target.buttonMobile).attr(
        'name',
        `${this.originalData.name}_other`
      );
      $(this.$target.buttonDesktop).attr('name', this.originalData.name);
    } else if (type === 'mobile') {
      $(this.$target.buttonDesktop).attr('id', `${this.originalData.id}_other`);
      $(this.$target.buttonMobile).attr('id', this.originalData.id);
      $(this.$target.buttonDesktop).attr(
        'name',
        `${this.originalData.name}_other`
      );
      $(this.$target.buttonMobile).attr('name', this.originalData.name);
      setTimeout(() => {
        $(this.$target.buttonMobile).attr(
          'onclick',
          $(this.$target.buttonDesktop).attr('onclick')
        );
      }, 400);
    }
  }
};
export default newsletter;

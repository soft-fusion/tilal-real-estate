import $ from 'jquery';
const banner = {
  settings: {
    target: '.banner',
    scrollButton: '.banner__scrollBox',
    sections: '[data-section]',
    mainVideo: '.banner__mainVideo',
    popup: '.banner__videoPopup',
    close: '.banner__videoClose',
    placeholderVideo: 'video.banner__photoInBg',
    playButton: '.banner__playButton'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
      this.changePlaceholderSrc();
      $(this.$target.playButton).removeClass('-hidden');
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      scrollButton: target.find(settings.scrollButton),
      sections: $(settings.sections),
      mainVideo: target.find(settings.mainVideo),
      popup: target.find(settings.popup),
      close: target.find(settings.close),
      placeholderVideo: target.find(settings.placeholderVideo),
      playButton: target.find(settings.playButton)
    };
  },
  bindEvents() {
    $(this.$target.scrollButton).on('click', this.scrollToNext.bind(this));
    $(this.$target.mainVideo).on('loadeddata', function() {
      $(this)[0].play();
    });
    $(this.$target.playButton).on('click', this.openPopup.bind(this));
    $(this.$target.close).on('click', this.closePopup.bind(this));
    $(window).on('resize', () => {
      let handle;
      setTimeout(() => {
        clearTimeout(handle);
        handle = setTimeout(() => {
          this.changePlaceholderSrc();
        }, 50);
      }, 50);
    });
  },
  scrollToNext(event) {
    let button = event.currentTarget;
    let parent = $(button).parents('[data-section]');
    let indexParent = this.$target.sections.index(parent);
    if (this.$target.sections[indexParent + 1]) {
      let targetOffset = $(this.$target.sections[indexParent + 1]).offset().top;
      $('html,body').animate({ scrollTop: targetOffset + 35 }, 1000);
    }
  },
  openPopup() {
    $(this.$target.popup).addClass('-active');
    $(this.$target.mainVideo).attr(
      'src',
      $(this.$target.mainVideo).attr('data-src')
    );
    $(this.$target.mainVideo)[0].load();
  },
  closePopup() {
    $(this.$target.mainVideo)[0].pause();
    $(this.$target.popup).removeClass('-active');
  },
  changePlaceholderSrc() {
    const placeholder = $(this.$target.placeholderVideo);
    const desktopSrc = placeholder.attr('data-desktop-src');
    const mobileSrc = placeholder.attr('data-mobile-src');
    if ($(window).width() > 768) {
      $(placeholder).attr({
        src: desktopSrc,
        width: 1600
      });
    } else {
      $(placeholder).attr({
        src: mobileSrc,
        width: 1430
      });
    }
  }
};
export default banner;

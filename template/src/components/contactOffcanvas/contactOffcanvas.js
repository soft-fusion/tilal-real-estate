import $ from 'jquery';
const contactOffcanvas = {
  settings: {
    target: '.contactOffcanvas',
    backdrop: '.contactOffcanvas__backdrop',
    close: '.contactOffcanvas__close'
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      backdrop: $(settings.backdrop),
      close: target.find(settings.close)
    };
  },
  bindEvents() {
    $(this.$target.close).on('click', () => {
      $(this.$target.root).removeClass('-open');
      document.body.removeAttribute('style');
    });
    $(this.$target.backdrop).on('click', () => {
      $(this.$target.root).removeClass('-open');
      document.body.removeAttribute('style');
    });
  }
};
export default contactOffcanvas;

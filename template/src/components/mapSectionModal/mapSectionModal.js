import $ from 'jquery';
import noUiSlider from 'nouislider';
import store from 'store2';
import { t } from '../../utils/i18n/i18n';
import autocomplete from '../../utils/scripts/autocomplete';
import { getShortenValue } from '../../utils/scripts/functions';

const mapSectionModal = {
  cities: [],
  settings: {
    target: '.mapSectionModal',
    input: '.mapSectionModal__input',
    bar: '.mapSectionModal__bar',
    clear: '.mapSectionModal__clear',
    numberButton: '.mapSectionModal__numberButton'
  },
  async init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.initSliders();
      this.bindEvents();
      this.cities = store.session('cities');
      this.initAutocomplete();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      input: target.find(this.settings.input),
      bar: target.find(this.settings.bar),
      clear: target.find(this.settings.clear),
      numberButton: target.find(this.settings.numberButton)
    };
  },
  bindEvents() {
    $(this.$target.clear).on('click', this.clearForm.bind(this));
    $(this.$target.numberButton).on('click', e => this.changeNumber(e));
  },
  changeNumber(e) {
    const target = $(e.currentTarget);
    const input = $(target).siblings('.mapSectionModal__numberInput')[0];
    if ($(target).is(':first-child')) {
      this.subtractNumber(input);
    } else if ($(target).is(':last-child')) {
      this.addNumber(input);
    }
  },
  initAutocomplete() {
    autocomplete($(this.$target.input).eq(0), this.cities);
  },
  initSliders() {
    $(this.$target.bar).each((index, item) => {
      const minStart = $(item).attr('data-start') || 0;
      const maxStart = $(item).attr('data-end') || 0;
      const minRange = $(item).attr('data-min') || minStart;
      const maxRange = $(item).attr('data-max') || maxStart;

      const start = [+minStart, +maxStart];
      const range = {
        min: +minRange,
        max: +maxRange
      };

      var pageDirection = 'ltr';

      if ($('body').hasClass('rtl')) {
        pageDirection = 'rtl';
      }

      noUiSlider.create(item, {
        start,
        range,
        connect: true,
        step: 1000,
        direction: pageDirection,
        format: {
          from: function(value) {
            return parseInt(value);
          },
          to: function(value) {
            return parseInt(value);
          }
        }
      });

      const lowerInput = $(item)
        .parents('.mapSectionModal__slider')
        .find('.mapSectionModal__barInput.-lower');
      const upperInput = $(item)
        .parents('.mapSectionModal__slider')
        .find('.mapSectionModal__barInput.-upper');

      let timeout;

      item.noUiSlider.on('update', function() {
        clearTimeout(timeout);
        const bounds = this.get();
        const lower = bounds[0];
        const lowerTip = $(item)
          .parents('.mapSectionModal__slider')
          .find('.mapSectionModal__tip.-lower');
        const upper = bounds[1];
        const upperTip = $(item)
          .parents('.mapSectionModal__slider')
          .find('.mapSectionModal__tip.-upper');

        $(lowerTip).text(`${getShortenValue(lower)}`);
        $(upperTip).text(`${getShortenValue(upper)}`);
        timeout = setTimeout(() => {
          $(lowerInput)
            .val(lower)
            .trigger('blur');
          $(upperInput)
            .val(upper)
            .trigger('blur');
        }, 400);
      });

      $(lowerInput).on('change', function() {
        const value = parseInt($(this).val());
        if (value || value === 0) {
          item.noUiSlider.set([value, null]);
        }
      });
      $(upperInput).on('change', function() {
        const value = parseInt($(this).val());
        if (value || value === 0) {
          item.noUiSlider.set([null, value]);
        }
      });
    });
  },
  addNumber(input) {
    const value = parseInt(input.value);
    if (Number.isNaN(value)) {
      $(input)
        .attr('value', '1')
        .trigger('change');
      $(input)
        .siblings('.mapSectionModal__numberPresentation')
        .html(`${t('numbers.1', '1')}`);
    } else {
      $(input)
        .attr('value', `${value + 1}`)
        .trigger('change');
      $(input)
        .siblings('.mapSectionModal__numberPresentation')
        .html(`${t(`numbers.${value + 1}`, value + 1)}`);
    }
  },
  subtractNumber(input) {
    const value = parseInt(input.value);
    if (value <= 1 || Number.isNaN(value)) {
      $(input)
        .attr('value', 'Any')
        .trigger('change');
      $(input)
        .siblings('.mapSectionModal__numberPresentation')
        .html(`${t('any', 'Any')}`);
    } else {
      $(input)
        .attr('value', value - 1)
        .trigger('change');
      $(input)
        .siblings('.mapSectionModal__numberPresentation')
        .html(`${t(`numbers.${value - 1}`, value - 1)}`);
    }
  },
  clearForm() {
    $('.mapSectionModal__input')
      .val('')
      .trigger('change');
    $('.mapSectionModal__homeTypeInput')
      .prop('checked', false)
      .trigger('change');
    $('.mapSectionModal__barInput.-lower')
      .val('0')
      .trigger('change');
    $('.mapSectionModal__barInput.-upper')
      .val($(this.$target.bar).attr('data-end'))
      .trigger('change');
    $('.mapSectionModal__numberInput').attr('value', `${t('any', 'Any')}`);
  }
};
export default mapSectionModal;

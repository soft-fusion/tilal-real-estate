import $ from 'jquery';
import { Loader } from 'google-maps';

const contact = {
  settings: {
    target: '.contact',
    map: '#map',
    formInputs: '.contact__right form input',
    formTextarea: '.contact__right form textarea',
    nameInput: '.contact__right input[name="SingleLine"]',
    phoneInput: '.contact__right input[name="PhoneNumber_countrycode"]'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
      this.createMap();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      map: target.find(settings.map),
      formInputs: target.find(settings.formInputs),
      formTextarea: target.find(settings.formTextarea),
      nameInput: target.find(settings.nameInput),
      phoneInput: target.find(settings.phoneInput)
    };
  },
  bindEvents() {
    $(this.$target.formInputs).on('input', this.hideLabels.bind(this));
    $(this.$target.formTextarea).on('input', this.hideLabels.bind(this));
    $(this.$target.nameInput).on('keydown paste', this.allowName.bind(this));
    $(this.$target.phoneInput).on('keydown paste', this.allowPhone.bind(this));
  },
  hideLabels(e) {
    const target = $(e.currentTarget);
    const label = $(target)
      .parents('li')
      .find('.zf-labelName');
    if ($(target).val().length > 0) {
      $(label).css('display', 'none');
    } else {
      $(label).css('display', '');
    }
  },
  allowName(e) {
    const regex = /^[A-Za-z\u0621-\u064A '.-]+$/;
    if (e.type === 'keydown') {
      if (
        !regex.test(e.key) &&
        e.key !== 'Tab' &&
        e.key !== 'Backspace' &&
        e.key !== 'Enter' &&
        e.key !== 'Control'
      ) {
        e.preventDefault();
      }
    } else if (e.type === 'paste') {
      if (
        !regex.test(
          (
            e.clipboardData ||
            e.originalEvent.clipboardData ||
            window.clipboardData
          ).getData('text')
        )
      ) {
        e.preventDefault();
      }
    }
  },
  allowPhone(e) {
    const regex = /^[0-9]+$/;
    if (e.type === 'keydown') {
      if (
        !regex.test(e.key) &&
        e.key !== 'Tab' &&
        e.key !== 'Backspace' &&
        e.key !== 'Enter' &&
        e.key !== 'Control'
      ) {
        e.preventDefault();
      }
    } else if (e.type === 'paste') {
      if (
        !regex.test(
          (
            e.clipboardData ||
            e.originalEvent.clipboardData ||
            window.clipboardData
          ).getData('text')
        )
      ) {
        e.preventDefault();
      }
    }
  },
  createMap() {
    const loader = new Loader('AIzaSyBS9VUWp9hy4n9IpioK0WrERw9f6lg-32k');
    loader.load().then(function(google) {
      const map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        fullscreenControl: false,
        fullscreenControlOptions: false,
        zoomControl: false,
        zoomControlOptions: false,
        clickableIcons: false,
        disableDefaultUI: true,
        disableDoubleClickZoom: true,
        draggable: false,
        draggableCursor: 'default',
        center: new google.maps.LatLng(26.67, 48.94),
        styles: [
          {
            featureType: 'landscape',
            elementType: 'all',
            stylers: [
              {
                color: '#f2f2f2'
              }
            ]
          },
          {
            featureType: 'landscape',
            elementType: 'geometry',
            stylers: [
              {
                color: '#f0f1f5'
              }
            ]
          },
          {
            featureType: 'landscape',
            elementType: 'geometry.stroke',
            stylers: [
              {
                visibility: 'on'
              },
              {
                color: '#9b3d3d'
              }
            ]
          },
          {
            featureType: 'landscape',
            elementType: 'labels',
            stylers: [
              {
                visibility: 'on'
              }
            ]
          },
          {
            featureType: 'poi',
            elementType: 'labels',
            stylers: [
              {
                visibility: 'off'
              }
            ]
          },
          {
            featureType: 'road',
            elementType: 'all',
            stylers: [
              {
                saturation: -100
              },
              {
                lightness: 45
              },
              {
                visibility: 'simplified'
              }
            ]
          },
          {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [
              {
                visibility: 'on'
              }
            ]
          },
          {
            featureType: 'road',
            elementType: 'labels',
            stylers: [
              {
                visibility: 'on'
              }
            ]
          },
          {
            featureType: 'road',
            elementType: 'labels.icon',
            stylers: [
              {
                visibility: 'off'
              }
            ]
          },
          {
            featureType: 'transit',
            elementType: 'all',
            stylers: [
              {
                visibility: 'simplified'
              }
            ]
          },
          {
            featureType: 'transit',
            elementType: 'labels.text',
            stylers: [
              {
                visibility: 'off'
              }
            ]
          },
          {
            featureType: 'transit',
            elementType: 'labels.icon',
            stylers: [
              {
                visibility: 'off'
              }
            ]
          },
          {
            featureType: 'water',
            elementType: 'all',
            stylers: [
              {
                color: '#b4d4e1'
              }
            ]
          },
          {
            featureType: 'administrative',
            elementType: 'labels.text',
            stylers: [{ visibility: 'off' }]
          },
          {
            featureType: 'administrative',
            elementType: 'labels',
            stylers: [{ visibility: 'off' }]
          },
          {
            featureType: 'administrative',
            elementType: 'all',
            stylers: [{ visibility: 'off' }]
          }
        ]
      });
      let marker = new google.maps.Marker({
        position: new google.maps.LatLng(26.3121345, 50.2239268),
        map: map,
        icon: '../wp-content/themes/tilal/assets/images/marker.svg'
      });
      marker.addListener('click', () => {
        map.setZoom(17);
        map.setCenter(marker.getPosition());
        $('#map').css('width', '100%');
      });
    });
  }
};
export default contact;

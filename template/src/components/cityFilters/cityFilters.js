import $ from 'jquery';
import noUiSlider from 'nouislider';
import autocomplete from '../../utils/scripts/autocomplete';
import 'select2';
import store from 'store2';
import { getShortenValue } from '../../utils/scripts/functions';

const cityFilters = {
  cities: [],
  settings: {
    target: '.cityFilters',
    mobileButton: '.cityFilters__mobileButton',
    wrapper: '.cityFilters__wrapper',
    dropTrigger: '.cityFilters__trigger',
    drop: '.cityFilters__drop',
    input: '.cityFilters__cityInput',
    bar: '.cityFilters__bar',
    selects: '.cityFilters__select'
  },
  async init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.initSliders();
      this.initSelect2();
      this.bindEvents();
      this.cities = store.session('cities');
      this.initAutocomplete();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      mobileButton: target.find(this.settings.mobileButton),
      wrapper: target.find(this.settings.wrapper),
      dropTrigger: target.find(this.settings.dropTrigger),
      drop: target.find(this.settings.drop),
      input: target.find(this.settings.input),
      bar: target.find(this.settings.bar),
      selects: target.find(this.settings.selects)
    };
  },
  bindEvents() {
    $(this.$target.input).on('autocomplete:change', () => {
      $('.cityResults__loader').removeClass('-hidden');
    });
    $(this.$target.dropTrigger).on('click', this.toggleDrop.bind(this));
    $(this.$target.mobileButton).on(
      'click',
      this.toggleMobileFilters.bind(this)
    );
    $(document).on('click', this.onClickOutsideDrop.bind(this));
  },
  toggleMobileFilters() {
    $(this.$target.wrapper).toggleClass('-open');
  },
  toggleDrop(e) {
    const target = $(e.currentTarget);
    $(target)
      .siblings(this.settings.drop)
      .toggleClass('-open');
  },
  onClickOutsideDrop(e) {
    const target = $(e.target);
    if (
      $(target).closest(this.settings.dropTrigger).length === 0 &&
      $(target).closest(this.settings.drop).length === 0 &&
      !$(target).is(this.settings.dropTrigger)
    ) {
      $(this.settings.drop).removeClass('-open');
    }
  },
  initAutocomplete() {
    autocomplete($(this.$target.input).eq(0), this.cities, {
      optionAsLink: true
    });
  },
  initSliders() {
    const _self = this;
    $(this.$target.bar).each((index, item) => {
      const minStart = $(item).attr('data-start') || 0;
      const maxStart = $(item).attr('data-end') || 0;
      const minRange = $(item).attr('data-min') || minStart;
      const maxRange = $(item).attr('data-max') || maxStart;

      const start = [+minStart, +maxStart];
      const range = {
        min: +minRange,
        max: +maxRange
      };

      noUiSlider.create(item, {
        start,
        range,
        connect: true,
        step: 10000,
        format: {
          from: function(value) {
            return parseInt(value);
          },
          to: function(value) {
            return parseInt(value);
          }
        }
      });

      const lowerInput = $(item)
        .parents('.cityFilters__slider')
        .find('.cityFilters__barInput.-lower');
      const upperInput = $(item)
        .parents('.cityFilters__slider')
        .find('.cityFilters__barInput.-upper');

      let timeout;

      item.noUiSlider.on('update', function() {
        clearTimeout(timeout);
        const bounds = this.get();

        const lower = bounds[0];
        const upper = bounds[1];

        $('.cityFilters__triggerTip.-lower').html(getShortenValue(lower));
        $('.cityFilters__triggerTip.-upper').html(
          `${getShortenValue(upper)}${upper.toString() === maxStart ? '+' : ''}`
        );

        if (lower > start[0] || upper < start[1]) {
          $(_self.$target.dropTrigger).removeClass('-label');
        } else {
          $(_self.$target.dropTrigger).addClass('-label');
        }

        timeout = setTimeout(() => {
          $(lowerInput)
            .val(lower)
            .trigger('blur');
          $(upperInput)
            .val(upper)
            .trigger('blur');
        }, 400);
      });

      $(lowerInput).on('change', function() {
        const value = parseInt($(this).val());
        if (value || value === 0) {
          item.noUiSlider.set([value, null]);
        }
      });
      $(upperInput).on('change', function() {
        const value = parseInt($(this).val());
        if (value || value === 0) {
          item.noUiSlider.set([null, value]);
        }
      });
    });
  },
  initSelect2() {
    $(this.$target.selects).each((index, item) => {
      $(item).select2({
        minimumResultsForSearch: -1,
        width: '100px'
      });
    });
  }
};
export default cityFilters;

import $ from 'jquery';

const callBox = {
  settings: {
    target: '.callBox',
    formInputs:
      '.callBox__formPlace form input:not([type="submit"]):not([type="reset"]), .callBox__formPlace form textarea ',
    returnURL: '.callBox__formPlace input[name="returnURL"]',
    nameInput: '.callBox__formPlace input[name="Last Name"]',
    phoneInput: '.callBox__formPlace input[name="Phone"]'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.pageInit();
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      formInputs: target.find(settings.formInputs),
      returnURL: target.find(settings.returnURL),
      nameInput: target.find(settings.nameInput),
      phoneInput: target.find(settings.phoneInput)
    };
  },
  pageInit() {
    $.each(this.$target.formInputs, (index, item) => {
      const label = $(item)
        .parents('.zcwf_row')
        .find('.zcwf_col_lab');
      if ($(item).val().length > 0) {
        $(label).css('display', 'none');
      } else {
        $(label).css('display', '');
      }
    });

    const returnURL = window.location.href;
    $(this.$target.returnURL).val(returnURL);
  },
  bindEvents() {
    $(this.$target.formInputs).on('input', this.hideLabels.bind(this));
    $(this.$target.nameInput).on('keydown paste', this.allowName.bind(this));
    $(this.$target.phoneInput).on('keydown paste', this.allowPhone.bind(this));
  },
  hideLabels(e) {
    const target = $(e.currentTarget);
    const label = $(target)
      .parents('.zf-tempFrmWrapper')
      .find('.zf-labelName');
    if ($(target).val().length > 0) {
      $(label).css('display', 'none');
    } else {
      $(label).css('display', '');
    }
  },
  allowName(e) {
    const regex = /^[A-Za-z\u0621-\u064A '.-]+$/;
    if (e.type === 'keydown') {
      if (
        !regex.test(e.key) &&
        e.key !== 'Tab' &&
        e.key !== 'Backspace' &&
        e.key !== 'Enter' &&
        e.key !== 'Control'
      ) {
        e.preventDefault();
      }
    } else if (e.type === 'paste') {
      if (
        !regex.test(
          (
            e.clipboardData ||
            e.originalEvent.clipboardData ||
            window.clipboardData
          ).getData('text')
        )
      ) {
        e.preventDefault();
      }
    }
  },
  allowPhone(e) {
    const regex = /^[0-9]+$/;
    if (e.type === 'keydown') {
      if (
        !regex.test(e.key) &&
        e.key !== 'Tab' &&
        e.key !== 'Backspace' &&
        e.key !== 'Enter' &&
        e.key !== 'Control'
      ) {
        e.preventDefault();
      }
    } else if (e.type === 'paste') {
      if (
        !regex.test(
          (
            e.clipboardData ||
            e.originalEvent.clipboardData ||
            window.clipboardData
          ).getData('text')
        )
      ) {
        e.preventDefault();
      }
    }
  }
};
export default callBox;

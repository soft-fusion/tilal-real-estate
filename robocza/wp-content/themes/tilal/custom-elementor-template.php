<?php /* Template Name: Custom-Elementor-Page */ ?>

<?php
get_header('text');

?>
    <div class="elementor__container" data-section="">
        <?php
        the_content();
        ?>
    </div>
<?php
get_footer();
?>



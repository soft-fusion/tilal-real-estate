<div class="buttonSection" data-section style="margin: 0">
    <a href="#popup" class="buttonSection__buttonHide -open-modal" style="display: none">
        Register Your Interest!
    </a>
    <div class="buttonSection__popup -close" style="z-index: 1501;">
        <img class="buttonSection__close" src="<?php echo TEMP_URI; ?>/assets/images/close.svg" alt="">
        <div class="buttonSection__popupBox">
            <div id="zf_div_l2OO1lnSpw-ONeuq3kj90gdwJf54Bm9z_a9GUqSatnc"></div>
            <script type="text/javascript">(function() {
                    try{
                        var f = document.createElement("iframe");

                        if(window.location.href.includes("?lang=en")) { // change to "?lang=en" & swap src if the default language is Arabic
                            f.dataset.src = 'https://forms.zohopublic.com/tilalre/form/Untitled/formperma/l2OO1lnSpw-ONeuq3kj90gdwJf54Bm9z_a9GUqSatnc?zf_lang=en&zf_rszfm=1';

                        }
                        else {
                            f.dataset.src = 'https://forms.zohopublic.com/tilalre/form/Untitled/formperma/l2OO1lnSpw-ONeuq3kj90gdwJf54Bm9z_a9GUqSatnc?zf_rszfm=1';
                        }

                        f.classList.add('-hidden');
                        f.style.border="none";
                        f.style.height="100%";
                        f.style.width="100%";
                        f.style.transition="all 0.5s ease";
                        var d = document.getElementById("zf_div_l2OO1lnSpw-ONeuq3kj90gdwJf54Bm9z_a9GUqSatnc");
                        d.appendChild(f);
                        window.addEventListener('message', function (){
                            var evntData = event.data;
                            if( evntData && evntData.constructor == String ){
                                var zf_ifrm_data = evntData.split("|");
                                if ( zf_ifrm_data.length == 2 ) {
                                    var zf_perma = zf_ifrm_data[0];
                                    var zf_ifrm_ht_nw = ( parseInt(zf_ifrm_data[1], 10) + 15 ) + "px";
                                    var iframe = document.getElementById("zf_div_l2OO1lnSpw-ONeuq3kj90gdwJf54Bm9z_a9GUqSatnc").getElementsByTagName("iframe")[0];
                                    if ( (iframe.src).indexOf('formperma') > 0 && (iframe.src).indexOf(zf_perma) > 0 ) {
                                        var prevIframeHeight = iframe.style.height;
                                        if ( prevIframeHeight != zf_ifrm_ht_nw ) {
                                            iframe.style.height = zf_ifrm_ht_nw;
                                        }
                                    }
                                }
                            }
                        }, false);
                    }catch(e){}
                })();</script>
        </div>
    </div>
</div>
<script>

    jQuery(document).on('click', '.maps__infoWindow.-open-modal', function () {
        console.log(1)
        jQuery('.buttonSection__buttonHide.-open-modal')[0].click();
    });
</script>
<div class="newsletter" data-section>
    <div class="newsletter__container">
        <div class="newsletter__title"><?php echo __("SUBSCRIBE TO OUR <br/>NEWSLETTER TILAL LIFE", "themetextdomain");  ?></div>
        <div class="newsletter__form">
            <!--Zoho Campaigns Web-Optin Form's Header Code Starts Here-->
            <script type="text/javascript" src="https://ckxi.maillist-manage.com/js/optin.min.js" onload="setupSF('sf3zc473d7960435f93c6b35bda831829f6655f00aa5bbf082b31bfd7df794d697d0','ZCFORMVIEW',false,'light',false,'0')"></script>
            <script type="text/javascript">
                function runOnFormSubmit_sf3zc473d7960435f93c6b35bda831829f6655f00aa5bbf082b31bfd7df794d697d0(th){
                    /*Before submit, if you want to trigger your event, "include your code here"*/
                };
            </script>

            <style>
            .quick_form_7_css * {
                -webkit-box-sizing: border-box !important;
                -moz-box-sizing: border-box !important;
                box-sizing: border-box !important;
                overflow-wrap: break-word
            }
            @media only screen and (max-width: 600px) {
                .quick_form_7_css[name="SIGNUP_BODY"] {
                    width: 100% !important; min-width: 100% !important; margin: 0px auto !important; padding: 0px !important
                }
                .SIGNUP_FLD {
                    width: 90% !important; margin: 10px 5% !important; padding: 0px !important
                }
                .SIGNUP_FLD input {
                    margin: 0 !important
                }
            }
            </style>

            <!--Zoho Campaigns Web-Optin Form's Header Code Ends Here--><!--Zoho Campaigns Web-Optin Form Starts Here-->

            <div id="sf3zc473d7960435f93c6b35bda831829f6655f00aa5bbf082b31bfd7df794d697d0" data-type="signupform">
                <div id="customForm">
                    <div class="quick_form_7_css" name="SIGNUP_BODY">
                        <div id="Zc_SignupSuccess" style="display:none;position:absolute;margin-left:4%;width:90%;background-color: white; padding: 3px; border: 3px solid rgb(194, 225, 154);  margin-top: 10px;margin-bottom:10px;word-break:break-all;z-index:100;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td width="10%">
                                            <img loading="lazy" class="successicon" src="https://ckxi.maillist-manage.com/images/challangeiconenable.jpg">
                                        </td>
                                        <td>
                                            <span id="signupSuccessMsg" style="color: rgb(73, 140, 132); font-family: sans-serif; font-size: 14px;word-break:break-word">&nbsp;&nbsp;<?php echo __("Thank you for Signing Up", "themetextdomain");  ?></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <form method="POST" id="zcampaignOptinForm" style="margin: 0px; width: 100%; padding: 0 25px" action="https://maillist-manage.com/weboptin.zc" target="_zcSignup">
                            <div style="background-color: rgb(255, 235, 232); padding: 10px; color: rgb(210, 0, 0); font-size: 11px; margin: 20px 0px 0px; border: 1px solid rgb(255, 217, 211); opacity: 1; display: none" id="errorMsgDiv"><?php echo __("Please correct the marked field(s) below.", "themetextdomain");  ?></div>
                            <div style="position: relative; margin: 10px 0 15px; width: 220px; height: 30px; display: inline-block" class="SIGNUP_FLD">
                                <input type="text" style="font-size: 14px; border: 0; border-radius: 0; width: 100%; height: 100%; z-index: 4; outline: none; padding: 5px 10px; color: rgb(136, 136, 136); text-align: left; font-family: &quot;Arial&quot;; background-color: transparent; border: 0; background-color: rgb(255, 255, 255); box-sizing: border-box" placeholder="<?php echo __("YOUR EMAIL ADDRESS", "themetextdomain");  ?>" changeitem="SIGNUP_FORM_FIELD" name="CONTACT_EMAIL" id="EMBED_FORM_EMAIL_LABEL" spellcheck="false" data-ms-editor="true">
                            </div>
                            <div style="position: relative; width: 100px; height: 30px; text-align: left; display: inline-block" class="SIGNUP_FLD">

                                <input type="button" style="text-align: center; width: 100%; height: 100%; z-index: 5; border: 0; color: rgb(255, 255, 255); cursor: pointer; outline: none; font-size: 14px; background-color: rgb(235, 28, 93); margin: 0; margin-left: -5px" name="SIGNUP_SUBMIT_BUTTON" id="zcWebOptin" class="newsletter__button -desktop" value="<?php echo __("SUBMIT", "themetextdomain");  ?>">
                                <input type="button" style="text-align: center; width: 100%; height: 100%; z-index: 5; border: 0; color: rgb(255, 255, 255); cursor: pointer; outline: none; font-size: 14px; background-color: rgb(235, 28, 93); margin: 0; margin-left: -5px" name="SIGNUP_SUBMIT_BUTTON" id="zcWebOptin" class="newsletter__button -mobile" value=">">
                            </div>
                            <input type="hidden" id="fieldBorder" value="">
                            <input type="hidden" id="submitType" name="submitType" value="optinCustomView">
                            <input type="hidden" id="emailReportId" name="emailReportId" value="">
                            <input type="hidden" id="formType" name="formType" value="QuickForm">
                            <input type="hidden" name="zx" id="cmpZuid" value="12d2ffb2d">
                            <input type="hidden" name="zcvers" value="3.0">
                            <input type="hidden" name="oldListIds" id="allCheckedListIds" value="">
                            <input type="hidden" id="mode" name="mode" value="OptinCreateView">
                            <input type="hidden" id="zcld" name="zcld" value="19e54fee1625d318">
                            <input type="hidden" id="zctd" name="zctd" value="">
                            <input type="hidden" id="document_domain" value="">
                            <input type="hidden" id="zc_Url" value="ckxi.maillist-manage.com">
                            <input type="hidden" id="new_optin_response_in" value="0">
                            <input type="hidden" id="duplicate_optin_response_in" value="0">
                            <input type="hidden" name="zc_trackCode" id="zc_trackCode" value="ZCFORMVIEW">
                            <input type="hidden" id="zc_formIx" name="zc_formIx" value="3zc473d7960435f93c6b35bda831829f6655f00aa5bbf082b31bfd7df794d697d0">
                            <input type="hidden" id="viewFrom" value="URL_ACTION">
                            <span style="display: none" id="dt_CONTACT_EMAIL">1,true,6,Contact Email,2</span>
                        </form>
                    </div>
                </div>
                <img loading="lazy" src="https://ckxi.maillist-manage.com/images/spacer.gif" id="refImage" onload="referenceSetter(this)" style="display:none;">
            </div>
            <input type="hidden" id="signupFormType" value="QuickForm_Horizontal">
            <div id="zcOptinOverLay" oncontextmenu="return false" style="display:none;text-align: center; background-color: rgb(0, 0, 0); opacity: 0.5; z-index: 100; position: fixed; width: 100%; top: 0px; left: 0px; height: 988px;"></div>
            <div id="zcOptinSuccessPopup" style="display:none;z-index: 9999;width: 800px; height: 40%;top: 84px;position: fixed; left: 26%;background-color: #FFFFFF;border-color: #E6E6E6; border-style: solid; border-width: 1px;  box-shadow: 0 1px 10px #424242;padding: 35px;">
                <span style="position: absolute;top: -16px;right:-14px;z-index:99999;cursor: pointer;" id="closeSuccess">
                    <img loading="lazy" src="https://ckxi.maillist-manage.com/images/videoclose.png">
                </span>
                <div id="zcOptinSuccessPanel"></div>
            </div>
            <!--Zoho Campaigns Web-Optin Form Ends Here--> 
        </div>
    </div>
</div>

<?php wp_footer(); ?>
<footer class="footer">
    <div class="footer__container">
        <div class="footer__box">
            <div class="footer__rowTop">
                    <?php
                    $args = array(
                        'theme_location' => 'footer_menu_top',
                        'container' => false,
                        'items_wrap'     =>'<ul class="footer__top">%3$s</ul>',
                        'item_spacing' => true,
                        'walker' => new Footer_Menu_Walker_Top
                    );
                    wp_nav_menu($args);

                    ?>
            </div>
            <?php
            ?>
            <div class="footer__rowBottom">
                <div class="footer__left">
                    <div class="footer__socials">
                        <?php
                        while ( (have_rows( 'add_element_social','option'))): the_row();
                            $title_social_media = get_sub_field('title_social_media');
                            $url_social_media = get_sub_field('url_social_media');
                            $icon_social_media = get_sub_field('icon_social_media');
                            $icon_social_media = wp_get_attachment_image_src($icon_social_media['ID'],'');
                            if(!empty($url_social_media)):
                            ?>
                        <a class="footer__social" href="<?php echo $url_social_media ?>" title="<?= $title_social_media ?>">
                            <img loading="lazy"class="footer__socialIcon" src="<?php echo $icon_social_media[0] ?>"
                                 alt="">
                        </a>
                        <?php endif;
                        endwhile; ?>
                    </div>
                    <ul class="footer__list">
                        <?php
                        $args = array(
                            'theme_location' => 'footer_menu',
                            'container' => false,
                            'item_spacing' => true,
                            'walker' => new Footer_Menu_Walker()
                        );
                        wp_nav_menu($args);

                        ?>
                    </ul>
                </div>
                <div class="footer__right">
                    <div class="footer__text"><?php echo __("COPYRIGHT OF Tilal Real Estate", "themetextdomain");  ?></div><img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/logo-mini.svg">
                </div>
            </div>
        </div>
    </div>
</footer>
<script async src="<?php echo TEMP_URI; ?>/assets/scripts/main.min.js"></script>
<script async src="<?php echo TEMP_URI; ?>/assets/scripts/validation.js"></script>
</body>
</html>



<?php
get_header();

the_post();
global $post;
$city_name = $post->post_title;
?>

<div class="cityFilters">
  <div class="cityFilters__container">
    <div class="cityFilters__mobileButton"><?php echo __("Filters", "themetextdomain"); ?></div>
    <div class="cityFilters__wrapper">
      <div class="cityFilters__filter cityFilters__city">
        <label class="cityFilters__label"><?php echo __("City", "themetextdomain"); ?></label>
        <input class="cityFilters__cityInput" name="location" value="<?php echo $city_name; ?>">
      </div>
      <div class="cityFilters__filter cityFilters__homeType">
        <label class="cityFilters__label"><?php echo __("Home type", "themetextdomain"); ?></label>
        <select class="cityFilters__select" name="home-type">
          <option value=""><?php echo __("Any", "themetextdomain"); ?></option>
          <option value="apartment"><?php echo __("Apartment", "themetextdomain"); ?></option>
          <option value="villa"><?php echo __("Villa", "themetextdomain"); ?></option>
          <option value="duplex"><?php echo __("Duplex", "themetextdomain"); ?></option>
          <option value="townhouse"><?php echo __("Townhouse", "themetextdomain"); ?></option>
          <option value="penthouse"><?php echo __("Penthouse", "themetextdomain"); ?></option>
        </select>
      </div>
      <div class="cityFilters__filter cityFilters__price">
        <label class="cityFilters__label"><?php echo __("Price Range", "themetextdomain"); ?></label>
        <div class="cityFilters__dropdown">
          <div class="cityFilters__trigger -label">
            <span class="cityFilters__triggerLabel"><?php echo __("Price Range", "themetextdomain"); ?></span>
            <span class="cityFilters__triggerTip -lower"></span>
            <span class="cityFilters__triggerTipLine">-</span>
            <span class="cityFilters__triggerTip -upper"></span>
          </div>
          <div class="cityFilters__drop">
            <div class="cityFilters__slider">
              <div class="cityFilters__bar" data-start="0" data-end="3000000"></div>
              <div class="cityFilters__barInputs">
                <input class="cityFilters__barInput -lower" type="hidden" name="price-from">
                <input class="cityFilters__barInput -upper" type="hidden" name="price-to">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="cityFilters__filter cityFilters__bedrooms">
        <label class="cityFilters__label"><?php echo __("Bedrooms", "themetextdomain"); ?></label>
        <select class="cityFilters__select" name="bedrooms">
          <option value=""><?php echo __("Any", "themetextdomain"); ?></option>
          <option value="2"><?php echo __("2+", "themetextdomain"); ?></option>
          <option value="3"><?php echo __("3+", "themetextdomain"); ?></option>
          <option value="4"><?php echo __("4+", "themetextdomain"); ?></option>
        </select>
      </div>
      <div class="cityFilters__filter cityFilters__bathrooms">
        <label class="cityFilters__label"><?php echo __("Bathrooms", "themetextdomain"); ?></label>
        <select class="cityFilters__select" name="bathrooms">
          <option value=""><?php echo __("Any", "themetextdomain"); ?></option>
          <option value="2"><?php echo __("2+", "themetextdomain"); ?></option>
          <option value="3"><?php echo __("3+", "themetextdomain"); ?></option>
          <option value="4"><?php echo __("4+", "themetextdomain"); ?></option>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="cityResults">
  <div class="cityResults__loader"></div>
  <div class="cityResults__title"><?php echo __("Homes in", "themetextdomain"); ?> <?php echo $city_name; ?></div>
  <div class="cityResults__main -list">
    <div class="cityResults__left">
      <div class="cityResults__top">
        <div class="cityResults__countSort">
          <div class="cityResults__count">
            <span><?php echo __("Projects", "themetextdomain"); ?>:</span>
            <span class="cityResults__countNumber"><?php echo __("0", "themetextdomain"); ?></span>
          </div>
          <div class="cityResults__sort">
            <div class="cityResults__label"><?php echo __("Sort by", "themetextdomain"); ?></div>
            <select class="cityResults__select">
              <option value="name-asc"><?php echo __("Name - ascending", "themetextdomain"); ?></option>
              <option value="name-desc"><?php echo __("Name - descending", "themetextdomain"); ?></option>
              <option value="price-asc"><?php echo __("Price - ascending", "themetextdomain"); ?></option>
              <option value="price-desc"><?php echo __("Price - descending", "themetextdomain"); ?></option>
            </select>
          </div>
        </div>
        <div class="cityResults__viewSwitch">
          <div class="cityResults__switch -active" data-view="-list"><?php echo __("List", "themetextdomain"); ?></div>
          <div class="cityResults__switch" data-view="-map"><?php echo __("Map", "themetextdomain"); ?></div>
        </div>
      </div>
      <div class="cityResults__projects">
        <div class="cityResults__item -template">
          <div class="cityResults__itemContainer">
            <div class="cityResults__image" data-slot="image"></div>
            <div class="cityResults__content">
              <div class="cityResults__itemTitle" data-slot="title"></div>
              <div class="cityResults__city" data-slot="city"></div>
              <div class="cityResults__infoWrapper">
                <div class="cityResults__info">
                  <div class="cityResults__infoLabel"><?php echo __("Home type", "themetextdomain"); ?></div>
                  <div class="cityResults__value" data-slot="home-type"></div>
                </div>
                <div class="cityResults__info">
                  <div class="cityResults__infoLabel"><?php echo __("Price", "themetextdomain"); ?></div>
                  <div class="cityResults__value" data-slot="price"></div>
                </div>
                <div class="cityResults__info">
                  <div class="cityResults__infoLabel"><?php echo __("Bedrooms", "themetextdomain"); ?></div>
                  <div class="cityResults__value" data-slot="bedrooms"></div>
                </div>
                <div class="cityResults__info">
                  <div class="cityResults__infoLabel"><?php echo __("Bathrooms", "themetextdomain"); ?></div>
                  <div class="cityResults__value" data-slot="bathrooms"></div>
                </div>
              </div>
              <div class="cityResults__bottom">
                <a class="cityResults__button" href="#" data-slot="url"><?php echo __("View project", "themetextdomain"); ?></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="cityResults__right">
      <div class="cityResults__map maps" id="city-map"></div>
    </div>
  </div>
</div>

<?php

$args = array(
	'post_type' => 'investment',
	'post_status' => 'publish'
);
$query = new WP_Query( $args );
$counter = 0;
$apartmentArray = [];
$jsonArray = [];
$test = $query->posts;
//var_dump(get_field( 'select_apartments', $test[0]->ID ));
foreach ($query->posts as $item) {
	$apartmentCounter = 0;
	$tableApartments  = get_field( 'select_apartments', $item->ID );

}
get_footer();

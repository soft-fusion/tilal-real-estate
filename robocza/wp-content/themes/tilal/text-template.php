<?php /* Template Name: text-template */ ?>

<?php
get_header('text');

?>
    <div class="textSection" data-section="">
        <div class="textSection__container -centerText">
            <div class="textSection__box">
            <div class="textSection__tag"><?php echo get_the_title(); ?></div>
                <div class="textSection__text">
                    <?php
                    the_content();
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
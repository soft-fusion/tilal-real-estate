<?php
$args = array(
  'post_type' => 'investment',
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'orderby' => 'title',
  'order' => 'ASC',
  'meta_key' => 'project_visible_investment',
  'meta_value' => 'true'
);
$projects = new WP_Query($args);
?>
<!--<style>-->
<!--    .projectsGallery__soon{-->
<!--        list-style: none;-->
<!--        opacity: 0;-->
<!--        visibility: hidden;-->
<!--        transition: opacity 600ms, visibility 600ms;-->
<!--        position: absolute;-->
<!--        width: 100%;-->
<!--        height: 100%;-->
<!--        margin-left: auto;-->
<!--        margin-right: auto;-->
<!--        left: 0;-->
<!--        right: 0;-->
<!--        text-align: center;-->
<!--        top: 35%;-->
<!--        z-index: 1;-->
<!--        font-weight: 700;-->
<!--        font-family: "Gotham Bold";-->
<!--        font-size: 70px;-->
<!--        line-height: 1.14;-->
<!--        text-transform: uppercase;-->
<!--        color: #fff;-->
<!--        margin-bottom: 32px;-->
<!--        -webkit-filter: grayscale(0.6);-->
<!--    }-->
<!--    .projectsGallery__linkHover:hover img{-->
<!--        -webkit-filter: grayscale(0.6);-->
<!--        transition: 0.5s;-->
<!--    }-->
<!--    .projectsGallery__linkHover:hover .projectsGallery__soon{-->
<!--        position: absolute;-->
<!--        width: 100%;-->
<!--        height: 100%;-->
<!--        margin-left: auto;-->
<!--        margin-right: auto;-->
<!--        left: 0;-->
<!--        right: 0;-->
<!--        text-align: center;-->
<!--        top: 35%;-->
<!--        z-index: 1;-->
<!--        font-weight: 700;-->
<!--        font-family: "Gotham Bold";-->
<!--        font-size: 70px;-->
<!--        line-height: 1.14;-->
<!--        text-transform: uppercase;-->
<!--        color: #fff;-->
<!--        margin-bottom: 32px;-->
<!--        transition: all 0.2s linear;-->
<!--        -webkit-filter: grayscale(0.6);-->
<!--        visibility: visible;-->
<!--        opacity: 1;-->
<!---->
<!--    }-->
<!--</style>-->
<div class="projectsGallery">
  <div class="projectsGallery__bg -green">
    <div class="projectsGallery__swiper swiper">
      <div class="swiper-wrapper">
        <?php foreach ($projects->posts as $index => $item) :
          $title = $item->post_title;
	        $my_current_lang = apply_filters( 'wpml_current_language', NULL );
	        if($my_current_lang === 'ar') {
		        $soonText = ' قريباً ';
	        }else{
		        $soonText = 'SOON';
	        }
          $url = home_url('/tilal-homes/' . $item->post_name);
          $background_color = 'green';
          if ($index % 3 == 1) {
            $background_color = 'purple';
          } elseif ($index % 3 == 2) {
            $background_color = 'blue';
          }
          $image = get_field('big_image', $item->ID);
          $image = wp_get_attachment_image_url($image["ID"], "Projects_gallery");
          $city = get_field('city_investment', $item->ID);
          $city = $city->post_title;
          $soon = get_field('soon', $item->ID);
        ?>
          <div class="projectsGallery__slide swiper-slide" data-bg="<?php echo $background_color; ?>">
            <a class="projectsGallery__link <?php echo $soon ? 'projectsGallery__linkHover' :  ''; ?> <?php echo $soon ? '-open-modal' : ''; ?>" href="<?php echo $soon ? '#popup' :  $url; ?>" title="<?php echo $title; ?>">
                <div class="projectsGallery__soon"><?= $soonText ?></div>
                <div class="projectsGallery__photo">
                <img src="<?php echo $image; ?>" alt="<?php echo $title; ?>">
              </div>
              <div class="projectsGallery__title">
                <?php echo $title; ?>
              </div>
              <div class="projectsGallery__bottomWrapper">
                <?php
                $priceArray = [];
                $priceArrayCounter = 0;
                $typeArray = [];
                $typeArrayCounter = 0;
                $tableApartments = get_field('select_apartments', $item->ID);
                $tableType = [];
                foreach ($tableApartments as $apartment) {
                  $id = $apartment['add_project']->ID;
                  $priceArray[$priceArrayCounter] = (int)get_field('price_apartment', $id);
                  $priceArrayCounter++;
                  $typeArray = get_field('home_type_apartment', $id, true);
                  foreach ($typeArray as $type){
	                  if (!in_array($type, $tableType))
	                  {
		                  array_push($tableType, $type);
	                  }
                  }
                  $typeArrayCounter++;
                }
                $from = min($priceArray);
                $types = implode('/', array_unique($typeArray));
                ?>

                <?php if ($soon) : ?>
                  <div class="projectsGallery__bottomItem">
                    <?php echo __("Soon", "themetextdomain"); ?>
                  </div>
                <?php elseif (count($priceArray) > 0) : ?>
                  <div class="projectsGallery__bottomItem">
                      <?php if(is_rtl()){
                        echo __("Starting at", "themetextdomain"); echo ' : ';  echo number_format($from, 0, '.', ',').' '; echo __( "SAR", "themetextdomain" ).' ';
                      }else{
	                      echo __("Starting at", "themetextdomain").' '; ?>: <?php echo __("SAR", "themetextdomain").' '; ?> <?php echo number_format($from, 0, '.', ',');
                      }
                      ?>
                  </div>
                <?php endif; ?>

                <?php if (count($tableType) > 0) : ?>
                  <div class="projectsGallery__bottomItem">
                    <?php
                    $counter = 1;
                    foreach ($tableType as $type){

	                    switch ($type) {
		                    case 'apartment':
			                    echo __("Apartment", "themetextdomain");
			                    if(count($tableType) > $counter){
				                    echo ', ';
			                    }
			                    break;
		                    case 'villa':
			                    echo __("Villa", "themetextdomain");
			                    if(count($tableType) > $counter){
				                    echo ', ';
			                    }
			                    break;
		                    case 'duplex':
			                    echo __("Duplex", "themetextdomain");
			                    if(count($tableType) > $counter){
				                    echo ', ';
			                    }
			                    break;
		                    case 'townhouse':
			                    echo __("Townhouse", "themetextdomain");
			                    if(count($tableType) > $counter){
				                    echo ', ';
			                    }
			                    break;
		                    case 'penthouse':
			                    echo __("Penthouse", "themetextdomain");
			                    if(count($tableType) > $counter){
				                    echo ', ';
			                    }
			                    break;
	                    }
	                    $counter++;
                    } ?>
                  </div>
                <?php endif; ?>

                <?php if ($city) : ?>
                  <div class="projectsGallery__bottomItem">
                    <?php echo $city; ?>
                  </div>
                <?php endif; ?>

              </div>
            </a>
          </div>
        <?php endforeach; ?>
      </div>
      <div class="projectsGallery__nav -prev -hidden"><img src="<?php echo TEMP_URI; ?>/assets/images/arrow-right-grey.svg"></div>
      <div class="projectsGallery__nav -next"><img src="<?php echo TEMP_URI; ?>/assets/images/arrow-right-grey.svg"></div>
    </div>
  </div>
</div>
<?php
$title_section = get_sub_field('title_section');
$top_description = get_sub_field('top_description');
?>

<div class="aboutContent" data-section>
    <div class="aboutContent__container -centerText">
        <div class="aboutContent__box">
            <div class="aboutContent__tag"><?php echo $title_section; ?></div>
            <div class="aboutContent__text"><?php echo $top_description; ?></div>
        </div>
    </div>
</div>

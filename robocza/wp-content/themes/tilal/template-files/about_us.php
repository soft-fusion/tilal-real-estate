<?php
$main_title = get_sub_field('main_title');
$show = get_sub_field('show');
$add_image_section = get_sub_field('add_image_section');
$image_first_large = get_sub_field('image_first_large');
$image_first_large = wp_get_attachment_image_url($image_first_large['ID'],'Image_Large1');
$image_first_small = get_sub_field('image_first_small');
$image_first_small = wp_get_attachment_image_url($image_first_small['ID'],'Image_Large1');
$image_second_large = get_sub_field('image_second_large');
$image_second_large = wp_get_attachment_image_url($image_second_large['ID'],'Image_Large2');
$image_second_small = get_sub_field('image_second_small');
$image_second_small = wp_get_attachment_image_url($image_second_small['ID'],'Image_Large2');
global $wp;
$url = home_url($wp->request);
?>
<div class="aboutUs" <?php echo $url === home_url() ? '' : 'style="padding-bottom: 0"' ?> data-section>
    <div class="aboutUs__container">
        <hr class="customHr">
        <div class="aboutUs__box">
            <div class="aboutUs__title"><?php echo $main_title; ?> </div>
            <div class="aboutUs__items">
                <?php
                while ( (have_rows( 'information'))) {
                    the_row();
                    $title_about_us = get_sub_field('titla_about_us');
                    $description_about_us = get_sub_field('description_about_us');
                    $icon_about = get_sub_field('icon_about');
                    $icon_about = wp_get_attachment_image_url($icon_about['ID']);
                    $companies = get_sub_field('companies');
                ?>
                <div class="aboutUsItem">
                    <div class="aboutUsItem__top">
                        <div class="aboutUsItem__name"><?php echo $title_about_us; ?></div>
                        <div class="aboutUsItem__text"><?php echo $description_about_us; ?></div>
                    </div>
                    <?php if((int)$show === 2){ ?>
                    <div class="aboutUsItem__number -hidden-on-mobile"><?php echo $companies;  ?></div><?php } ?><img loading="lazy" class="<?php echo (int)$show === 2 ? '-hidden-on-desktop' : '' ?>" src="<?php echo $icon_about; ?>" alt="">
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php
    if((int)$add_image_section === 1)
    {
?>
    <div class="aboutUs__imageSection">
        <div class="aboutUs__image">
            <picture>
                <source media="(min-width: 376px)" srcset="<?php echo $image_first_large ?>" type="image/png">
                <source srcset="<?php echo $image_first_small ?>" type="image/png"><img loading="lazy" src="<?php echo $image_first_large ?>" alt="">
            </picture>
        </div>
        <div class="aboutUs__image -second">
            <picture>
                <source media="(min-width: 376px)" srcset="<?php echo $image_second_large ?>" type="image/png">
                <source srcset="<?php echo $image_second_small ?>" type="image/png"><img loading="lazy" src="<?php echo $image_second_large ?>" alt="">
            </picture>
        </div>
    </div>
    <?php
    }
    ?>
</div>

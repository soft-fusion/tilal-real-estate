<?php
$button_title = get_sub_field('button_title');
$button_link = get_sub_field('button_link');
?>
<div class="buttonSection" data-section>
    <hr class="customHr">
    <a href="#popup" class="buttonSection__button -open-modal">
        <?php echo $button_title ?>
    </a>

</div>
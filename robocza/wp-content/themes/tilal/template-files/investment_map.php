<?php
$scrolling_id = get_sub_field('scrolling_id');
$post = get_post();
$title = $post->post_title;
?>

<div
  class="investmentMap"
  data-section data-investment-name="<?php echo $title; ?>" 
  <?php if($scrolling_id != '') { echo 'data-scrolling-id="'.$scrolling_id.'"'; } ?>
>
  <div class="investmentMap__map">
    <div class="investmentMap__mapPlace" id="investment-map">
      <div class="investmentMap__placeholder"></div>
    </div>
  </div>
</div>
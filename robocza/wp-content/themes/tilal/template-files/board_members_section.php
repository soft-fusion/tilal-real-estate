
<?php if(have_rows( 'add_new')) {?>
<div class="board" data-section>
    <div class="board__container">
        <hr class="customHr">
        <div class="board__box">
            <div class="board__title"><?php echo __("board<br>members", "themetextdomain");  ?></div>
            <div class="board__grid">
                <?php
                while ( (have_rows( 'add_new'))) {
                    the_row();
                    $image_board_member = get_sub_field('image_board_member');
                    $image_board_member = wp_get_attachment_image_url($image_board_member,'Boardx2');
                    $name_board_member = get_sub_field('name_board_member');
                    $position_board_member = get_sub_field('position_board_member');
                ?>
                <div class="board__person"> <img loading="lazy" src="<?php echo $image_board_member ?>" alt="">
                    <div class="board__name"><?php echo $name_board_member ?></div>
                    <div class="board__info"><?php echo $position_board_member ?></div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php if(have_rows( 'add_new_ceo')) {?>
<div class="board">
    <div class="board__container">
        <hr class="customHr">
        <div class="board__box">
            <div class="board__title"><?php echo __("CEO", "themetextdomain");  ?></div>
            <div class="board__grid">
                <?php
                while ( (have_rows( 'add_new_ceo'))) {
                    the_row();
                    $image_board_ceo = get_sub_field('image_board_ceo');
                    $image_board_ceo = wp_get_attachment_image_url($image_board_ceo['ID'],'Boardx2');
                    $name_board_ceo = get_sub_field('name_board_ceo');
                    $position_board_ceo = get_sub_field('position_board_ceo');


                ?>
                <div class="board__person"> <img loading="lazy" src="<?php echo $image_board_ceo ?>" alt="">
                    <div class="board__name"><?php echo $name_board_ceo ?></div>
                    <div class="board__info"><?php echo $position_board_ceo ?></div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php
$title_section_virtual = get_sub_field('title_section_virtual');
$button_title_virtual = get_sub_field('button_title_virtual');
$button_link_virtual = get_sub_field('button_link_virtual');
$open_new_card_virtual = get_sub_field('open_new_card_virtual');
?>
<div class="investmentGallery__container">
    <hr class="customHr">
    <div class="investmentGallery__title"><?php echo $title_section_virtual; ?></div>
</div>
<div class="buttonSection" data-section>
	<a href="<?php echo $button_link_virtual; ?>" class="buttonSection__button" <?php echo $open_new_card_virtual[0] === '1' ? 'target="_blank"' : '' ?>>
		<?php echo $button_title_virtual ?>
	</a>
</div>

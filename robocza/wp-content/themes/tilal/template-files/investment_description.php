<?php
$tag = get_sub_field('tag');
$title = get_sub_field('title');
$text = get_sub_field('text');
$list_title = get_sub_field('list_title');
$list = get_sub_field('list');
$add_button = get_sub_field('add_button');
?>

<div class="textList" data-section>
  <div class="textList__container">
    <div class="textList__box">
      <div class="textList__tag"><?php echo $tag; ?></div>
      <div class="textList__title"><?php echo $title; ?></div>
      <div class="textList__text"><?php echo $text; ?></div>
      <div class="textList__tag2"><?php echo $list_title; ?></div>
      <ul class="textList__list">
        <?php foreach($list as $item): ?>
          <li><?php echo $item['text']; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php if ($add_button == true):
        $button_text = get_sub_field('button_text');
        $button_link = get_sub_field('button_link');
        $button_link_type = get_sub_field('button_link_type');
      ?>
        <div class="textList__buttonWrapper">
          <a href="<?php echo $button_link; ?>" class="textList__button" <?php if ($button_link_type == 'download') { echo 'download'; } ?>>
            <?php echo $button_text; ?>
          </a>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>
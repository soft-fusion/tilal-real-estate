<?php
$image = get_sub_field('banner_image');
$image = wp_get_attachment_image_url($image['ID'], 'Banner');
$logo = get_sub_field('logo');
$logo = wp_get_attachment_image_url($logo['ID'], 'Banner_title_image');
$text_or_image = get_sub_field('text_or_image');
$title_text = get_sub_field('banner_text');
if ($text_or_image == 'image') {
	$title_image = get_sub_field('banner_title_image');
	$title_image = wp_get_attachment_image_url($title_image['ID'], 'Banner_title_image');
}
$show_image_or_video = get_sub_field('show_image_or_video');
$scroll_text = get_sub_field('scroll_text');
$banner_mobile = get_sub_field('banner_mobile');
$language = $_GET['lang'] != '' ? '?lang=' . $_GET['lang'] : '';
$contact_circle_or_buttons = get_sub_field('contact_circle_or_buttons');
$add_class_mobile = 0;
if ((int)$banner_mobile === 1) {
	$add_class_mobile = '-leftOnMobile';
} elseif ((int)$banner_mobile === 2) {
	$add_class_mobile = '-rightOnMobile';
} elseif ((int)$banner_mobile === 3) {
	$add_class_mobile = '-centerRightOnMobile';
} elseif ((int)$banner_mobile === 4) {
	$add_class_mobile = '-bottomRightOnMobile';
} else {
	$add_class_mobile = '';
}

$phone_number = get_field('phone_number', 'option');
$whatsapp_number = get_field('whatsapp_number', 'option');
$whatsapp_description = get_field('whatsapp_description', 'option');
$map_iframe= '';

if ($_GET['lang'] === 'ar' || $_GET['lang'] === null) :
    $map_iframe = get_field('ar_map_link_option', 'option');
	?>
<?php
elseif ($_GET['lang'] === 'en') :
	$map_iframe = get_field('en_map_link_option', 'option');
	?>
<?php
endif;

?>

<div class="banner <?php echo $show_image_or_video === "1" ? '-withVideo' : '' ?>">
	<?php if ((int)$show_image_or_video === 2) : ?>
		<img loading="lazy" class="banner__photoInBg <?php echo $add_class_mobile; ?>" src="<?php echo $image  ?>">
	<?php elseif ((int)$show_image_or_video === 1) : ?>
		<?php
		$placeholder_video = get_sub_field('placeholder_video');
		$placeholder_video_src = wp_get_attachment_url($placeholder_video['ID']);
		$placeholder_video_poster = $placeholder_video['sizes']['2048x2048'];
		$placeholder_video_mobile = get_sub_field('placeholder_video_mobile');
		$placeholder_video_mobile = wp_get_attachment_url($placeholder_video_mobile['ID']);
		$main_video = get_sub_field('banner_video');
		$main_video = wp_get_attachment_url($main_video['ID']);
		?>
		<div class="banner__playButton -hidden">
			<img class="banner__playIcon" src="<?php echo TEMP_URI; ?>/assets/images/play.svg" alt="">
		</div>
		<video width="1600" height="auto" class="banner__photoInBg" playsinline autoplay muted loop poster="<?= $placeholder_video_poster; ?>" data-desktop-src="<?= $placeholder_video_src; ?>" data-mobile-src="<?= $placeholder_video_mobile; ?>">
			Your browser does not support the video tag.
		</video>
		<div class="banner__videoPopup">
			<div class="banner__videoBackdrop"></div>
			<div class="banner__videoWrapper">
				<img class="banner__videoClose" src="<?php echo TEMP_URI; ?>/assets/images/close.svg" alt="">
				<video data-src="<?= $main_video; ?>" class="banner__mainVideo" playsinline controls>
					Your browser does not support the video tag.
				</video>
			</div>
		</div>
	<?php endif; ?>
	<div class="banner__container"><img loading="lazy" class="banner__shape" src="<?php echo TEMP_URI; ?>/assets/images/banner-green-shape.svg">
		<?php if ($logo) : ?>
		<div class="banner__logo">
			<img loading="lazy" src="<?php echo $logo; ?>" alt="">
		</div>
		<?php endif; ?>
		<div class="banner__title">
			<?php if ($text_or_image == 'text') {
				echo $title_text;
			} else if ($text_or_image == 'image') { ?>
				<img src="<?php echo $title_image; ?>" alt="">
			<?php } ?>
		</div>
		<div class="banner__scrollBox">
			<div class="banner__circle"> <img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/arrow-down.svg"></div>
			<div class="banner__text"><?php echo $scroll_text; ?></div>
		</div>
		<?php if ($contact_circle_or_buttons == 'circle') : ?>
		<div class="contactCircles">
			<div class="contactCircles__info"><?php echo __("Need help?", "themetextdomain");  ?> </div>
			<div class="contactCircles__circleBox">
				<div class="contactCircles__circle -openForm"> <img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/phone.svg"></div>
				<div class="callBox">
					<div class="callBox__box">
						<div class="callBox__title"><?php echo __("How can we help?", "themetextdomain");  ?></div>
						<!-- <div class="callBox__alternative"> <?php echo __("or chat with us on", "themetextdomain");  ?><a class="callBox__link -whatsApp" href="https://wa.me/<?php echo $whatsapp_number ?>/?text=<?php echo $whatsapp_description ?>" title="chat us on WhatsApp"><?php echo __("WhatsApp", "themetextdomain");  ?></a><img loading="lazy" class="callBox__whatsApp" src="<?php echo TEMP_URI; ?>/assets/images/whatsApp.svg" alt=""></div> -->
						<div class="callBox__formPlace">
							<div class="zf-templateWidth">
								<form action='https://forms.zohopublic.com/tilalre/form/Signup/formperma/YPfRrYfd18OjXnThgY7KEoIYKhtA5uECaORlw2gGyb8/htmlRecords/submit' name='form' method='POST' onSubmit='javascript:document.charset="UTF-8"; return zf_ValidateAndSubmit();' accept-charset='UTF-8' enctype='multipart/form-data' id='form'><input type="hidden" name="zf_referrer_name" value=""><!-- To Track referrals , place the referrer name within the " " in the above hidden input field -->
									<input type="hidden" name="zf_redirect_url" value=""><!-- To redirect to a specific page after record submission , place the respective url within the " " in the above hidden input field -->
									<input type="hidden" name="zc_gad" value=""><!-- If GCLID is enabled in Zoho CRM Integration, click details of AdWords Ads will be pushed to Zoho CRM -->
									<div class="zf-templateWrapper">
										<!---------template Header Starts Here---------->
										<ul class="zf-tempHeadBdr">
											<li class="zf-tempHeadContBdr">
												<h2 class="zf-frmTitle"><em>Signup</em></h2>
												<p class="zf-frmDesc"></p>
												<div class="zf-clearBoth"></div>
											</li>
										</ul>
										<!---------template Header Ends Here---------->
										<!---------template Container Starts Here---------->
										<div class="zf-subContWrap zf-topAlign">
											<ul>
												<!---------Single Line Starts Here---------->
												<li class="zf-tempFrmWrapper zf-small"><label class="zf-labelName"><?php echo __("Your name", "themetextdomain");  ?>
														<em class="zf-important">*</em>
													</label>
													<div class="zf-tempContDiv"><span> <input type="text" name="SingleLine" checktype="c1" value="" maxlength="255" fieldType=1 /></span>
														<p id="SingleLine_error" class="zf-errorMessage" style="display:none;">Invalid value</p>
													</div>
													<div class="zf-clearBoth"></div>
												</li>
												<!---------Single Line Ends Here---------->
												<!---------Email Starts Here---------->
												<li class="zf-tempFrmWrapper zf-large"><label class="zf-labelName"><?php echo __("Your email", "themetextdomain");  ?>
														<em class="zf-important">*</em>
													</label>
													<div class="zf-tempContDiv"><span> <input fieldType=9 type="text" maxlength="255" name="Email" checktype="c5" value="" /></span>
														<p id="Email_error" class="zf-errorMessage" style="display:none;">Invalid value</p>
													</div>
													<div class="zf-clearBoth"></div>
												</li>
												<!---------Email Ends Here---------->
												<!---------Phone Starts Here---------->
												<li class="zf-tempFrmWrapper zf-small"><label class="zf-labelName"><?php echo __("Your phone", "themetextdomain");  ?>
														<em class="zf-important">*</em>
													</label>
													<div class="zf-tempContDiv zf-phonefld">
														<div class="zf-phwrapper zf-phNumber">
															<span> <input type="text" compname="PhoneNumber" name="PhoneNumber_countrycode" maxlength="20" checktype="c7" value="" phoneFormat="1" isCountryCodeEnabled=false fieldType="11" id="international_PhoneNumber_countrycode" valType="number" phoneFormatType="1" />
																<label>Number</label> </span>
															<div class="zf-clearBoth"></div>
														</div>
														<p id="PhoneNumber_error" class="zf-errorMessage" style="display:none;">Invalid value</p>
													</div>
													<div class="zf-clearBoth"></div>
												</li>
												<!---------Phone Ends Here---------->
												<!---------Multiple Line Starts Here---------->
												<li class="zf-tempFrmWrapper zf-small"><label class="zf-labelName"><?php echo __("Enter message", "themetextdomain");  ?>
														<em class="zf-important">*</em>
													</label>
													<div class="zf-tempContDiv"><span> <textarea name="MultiLine" checktype="c1" maxlength="65535"></textarea> </span>
														<p id="MultiLine_error" class="zf-errorMessage" style="display:none;">Invalid value</p>
													</div>
													<div class="zf-clearBoth"></div>
												</li>
												<!---------Multiple Line Ends Here---------->
												<!--Terms and conditions-->
												<li class="zf-tempFrmWrapper">
													<div class="zf-tempContDiv">
														<div class="zf-termsContainer">
															<div class="zf-termsAccept"><input class="zf-checkBoxType zf-flLeft" name="TermsConditions" type="checkbox"><label class="zf-descFld"><?php echo __("I accept the", "themetextdomain");  ?> <a href='/privacy-policy/<?= $language ?>' title='privacy-policy' target='_blank'><?php echo __("terms & conditions", "themetextdomain");  ?></a>.</label></div>
														</div>
													</div>
													<p class="zf-errorMessage" elname="error" id="TermsConditions_error" style="display:none;">Invalid value</p>
													<div class="clearBoth"></div>
												</li>
											</ul>
										</div>
										<!---------template Container Starts Here---------->
										<ul>
											<li class="zf-fmFooter"><button class="zf-submitColor"><?php echo __("SEND REQUEST", "themetextdomain");  ?></button></li>
										</ul>
									</div><!-- 'zf-templateWrapper' ends -->
								</form>
							</div><!-- 'zf-templateWidth' ends -->
							<script type="text/javascript">
								var zf_DateRegex = new RegExp("^(([0][1-9])|([1-2][0-9])|([3][0-1]))[-](Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)[-](?:(?:19|20)[0-9]{2})$");
								var zf_MandArray = ["SingleLine", "Email", "PhoneNumber_countrycode", "MultiLine", "TermsConditions"];
								var zf_FieldArray = ["SingleLine", "Email", "PhoneNumber_countrycode", "MultiLine", "TermsConditions"];
								var isSalesIQIntegrationEnabled = false;
								var salesIQFieldsArray = [];
							</script>
							<script>
								function zf_ValidateAndSubmit() {
									if (zf_CheckMandatory()) {
										if (zf_ValidCheck()) {
											if (isSalesIQIntegrationEnabled) {
												zf_addDataToSalesIQ();
											}
											return true;
										} else {
											return false;
										}
									} else {
										return false;
									}
								}

								function zf_CheckMandatory() {
									for (i = 0; i < zf_MandArray.length; i++) {
										var fieldObj = document.forms.form[zf_MandArray[i]];
										if (fieldObj) {
											if (fieldObj.nodeName != null) {
												if (fieldObj.nodeName == 'OBJECT') {
													if (!zf_MandatoryCheckSignature(fieldObj)) {
														zf_ShowErrorMsg(zf_MandArray[i]);
														return false;
													}
												} else if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length == 0) {
													if (fieldObj.type == 'file') {
														fieldObj.focus();
														zf_ShowErrorMsg(zf_MandArray[i]);
														return false;
													}
													fieldObj.focus();
													zf_ShowErrorMsg(zf_MandArray[i]);
													return false;
												} else if (fieldObj.nodeName == 'SELECT') { // No I18N
													if (fieldObj.options[fieldObj.selectedIndex].value == '-Select-') {
														fieldObj.focus();
														zf_ShowErrorMsg(zf_MandArray[i]);
														return false;
													}
												} else if (fieldObj.type == 'checkbox' || fieldObj.type == 'radio') {
													if (fieldObj.checked == false) {
														fieldObj.focus();
														zf_ShowErrorMsg(zf_MandArray[i]);
														return false;
													}
												}
											} else {
												var checkedValsCount = 0;
												var inpChoiceElems = fieldObj;
												for (var ii = 0; ii < inpChoiceElems.length; ii++) {
													if (inpChoiceElems[ii].checked === true) {
														checkedValsCount++;
													}
												}
												if (checkedValsCount == 0) {
													inpChoiceElems[0].focus();
													zf_ShowErrorMsg(zf_MandArray[i]);
													return false;
												}
											}
										}
									}
									return true;
								}

								function zf_ValidCheck() {
									var isValid = true;
									for (ind = 0; ind < zf_FieldArray.length; ind++) {
										var fieldObj = document.forms.form[zf_FieldArray[ind]];
										if (fieldObj) {
											if (fieldObj.nodeName != null) {
												var checkType = fieldObj.getAttribute("checktype");
												if (checkType == "c2") { // No I18N
													if (!zf_ValidateNumber(fieldObj)) {
														isValid = false;
														fieldObj.focus();
														zf_ShowErrorMsg(zf_FieldArray[ind]);
														return false;
													}
												} else if (checkType == "c3") { // No I18N
													if (!zf_ValidateCurrency(fieldObj) || !zf_ValidateDecimalLength(fieldObj, 10)) {
														isValid = false;
														fieldObj.focus();
														zf_ShowErrorMsg(zf_FieldArray[ind]);
														return false;
													}
												} else if (checkType == "c4") { // No I18N
													if (!zf_ValidateDateFormat(fieldObj)) {
														isValid = false;
														fieldObj.focus();
														zf_ShowErrorMsg(zf_FieldArray[ind]);
														return false;
													}
												} else if (checkType == "c5") { // No I18N
													if (!zf_ValidateEmailID(fieldObj)) {
														isValid = false;
														fieldObj.focus();
														zf_ShowErrorMsg(zf_FieldArray[ind]);
														return false;
													}
												} else if (checkType == "c6") { // No I18N
													if (!zf_ValidateLiveUrl(fieldObj)) {
														isValid = false;
														fieldObj.focus();
														zf_ShowErrorMsg(zf_FieldArray[ind]);
														return false;
													}
												} else if (checkType == "c7") { // No I18N
													if (!zf_ValidatePhone(fieldObj)) {
														isValid = false;
														fieldObj.focus();
														zf_ShowErrorMsg(zf_FieldArray[ind]);
														return false;
													}
												} else if (checkType == "c8") { // No I18N
													zf_ValidateSignature(fieldObj);
												}
											}
										}
									}
									return isValid;
								}

								function zf_ShowErrorMsg(uniqName) {
									var fldLinkName;
									for (errInd = 0; errInd < zf_FieldArray.length; errInd++) {
										fldLinkName = zf_FieldArray[errInd].split('_')[0];
										document.getElementById(fldLinkName + "_error").style.display = 'none';
									}
									var linkName = uniqName.split('_')[0];
									document.getElementById(linkName + "_error").style.display = 'block';
								}

								function zf_ValidateNumber(elem) {
									var validChars = "-0123456789";
									var numValue = elem.value.replace(/^\s+|\s+$/g, '');
									if (numValue != null && !numValue == "") {
										var strChar;
										var result = true;
										if (numValue.charAt(0) == "-" && numValue.length == 1) {
											return false;
										}
										for (i = 0; i < numValue.length && result == true; i++) {
											strChar = numValue.charAt(i);
											if ((strChar == "-") && (i != 0)) {
												return false;
											}
											if (validChars.indexOf(strChar) == -1) {
												result = false;
											}
										}
										return result;
									} else {
										return true;
									}
								}

								function zf_ValidateDateFormat(inpElem) {
									var dateValue = inpElem.value.replace(/^\s+|\s+$/g, '');
									if (dateValue == "") {
										return true;
									} else {
										return (zf_DateRegex.test(dateValue));
									}
								}

								function zf_ValidateCurrency(elem) {
									var validChars = "0123456789.";
									var numValue = elem.value.replace(/^\s+|\s+$/g, '');
									if (numValue.charAt(0) == '-') {
										numValue = numValue.substring(1, numValue.length);
									}
									if (numValue != null && !numValue == "") {
										var strChar;
										var result = true;
										for (i = 0; i < numValue.length && result == true; i++) {
											strChar = numValue.charAt(i);
											if (validChars.indexOf(strChar) == -1) {
												result = false;
											}
										}
										return result;
									} else {
										return true;
									}
								}

								function zf_ValidateDecimalLength(elem, decimalLen) {
									var numValue = elem.value;
									if (numValue.indexOf('.') >= 0) {
										var decimalLength = numValue.substring(numValue.indexOf('.') + 1).length;
										if (decimalLength > decimalLen) {
											return false;
										} else {
											return true;
										}
									}
									return true;
								}

								function zf_ValidateEmailID(elem) {
									var check = 0;
									var emailValue = elem.value;
									if (emailValue != null && !emailValue == "") {
										var emailArray = emailValue.split(",");
										for (i = 0; i < emailArray.length; i++) {
											var emailExp = /^[\w]([\w\-.+'/]*)@([a-zA-Z0-9]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,22}$/;
											if (!emailExp.test(emailArray[i].replace(/^\s+|\s+$/g, ''))) {
												check = 1;
											}
										}
										if (check == 0) {
											return true;
										} else {
											return false;
										}
									} else {
										return true;
									}
								}

								function zf_ValidateLiveUrl(elem) {
									var urlValue = elem.value;
									if (urlValue !== null && typeof(urlValue) !== "undefined") {
										urlValue = urlValue.replace(/^\s+|\s+$/g, '');
										if (urlValue !== "") {
											var urlregex = new RegExp("^((((h|H)(t|T)|(f|F))(t|T)(p|P)((s|S)?)://[-.\\w]*)|(((w|W){3}\\.)[-.\\w]+))(/?)([-\\w.?,:'/\\\\+=&;%$#@()!~]*)?$"); // Same regex as website_url in security-regex.xml. But single backslash is replaced with two backslashes.
											return (urlregex.test(urlValue));
										}
									}
									return true;
								}

								function zf_ValidatePhone(inpElem) {
									var phoneFormat = parseInt(inpElem.getAttribute("phoneFormat"));
									var fieldInpVal = inpElem.value.replace(/^\s+|\s+$/g, '');
									var toReturn = true;
									if (phoneFormat === 1) {
										if (inpElem.getAttribute("valType") == 'code') {
											var codeRexp = /^[+][0-9]{1,4}$/;
											if (fieldInpVal != "" && !codeRexp.test(fieldInpVal)) {
												return false;
											}
										} else {
											var IRexp = /^[+]{0,1}[()0-9- ]+$/;
											if (inpElem.getAttribute("phoneFormatType") == '2') {
												IRexp = /^[0-9]+$/;
											}
											if (fieldInpVal != "" && !IRexp.test(fieldInpVal)) {
												toReturn = false;
												return toReturn;
											}
										}
										return toReturn;
									} else if (phoneFormat === 2) {
										var InpMaxlength = inpElem.getAttribute("maxlength");
										var USARexp = /^[0-9]+$/;
										if (fieldInpVal != "" && USARexp.test(fieldInpVal) && fieldInpVal.length == InpMaxlength) {
											toReturn = true;
										} else if (fieldInpVal == "") {
											toReturn = true;
										} else {
											toReturn = false;
										}
										return toReturn;
									}
								}

								function zf_ValidateSignature(objElem) {
									var linkName = objElem.getAttribute("compname");
									var canvasElem = document.getElementById("drawingCanvas-" + linkName);
									var isValidSign = zf_IsSignaturePresent(objElem, linkName, canvasElem);
									var hiddenSignInputElem = document.getElementById("hiddenSignInput-" + linkName);
									if (isValidSign) {
										hiddenSignInputElem.value = canvasElem.toDataURL();
									} else {
										hiddenSignInputElem.value = ""; // No I18N
									}
									return isValidSign;
								}

								function zf_MandatoryCheckSignature(objElem) {
									var linkName = objElem.getAttribute("compname");
									var canvasElem = document.getElementById("drawingCanvas-" + linkName);
									var isValid = zf_IsSignaturePresent(objElem, linkName, canvasElem);
									return isValid;
								}

								function zf_IsSignaturePresent(objElem, linkName, canvasElem) {
									var context = canvasElem.getContext('2d'); // No I18N
									var canvasWidth = canvasElem.width;
									var canvasHeight = canvasElem.height;
									var canvasData = context.getImageData(0, 0, canvasWidth, canvasHeight);
									var signLen = canvasData.data.length;
									var flag = false;
									for (var index = 0; index < signLen; index++) {
										if (!canvasData.data[index]) {
											flag = false;
										} else if (canvasData.data[index]) {
											flag = true;
											break;
										}
									}
									return flag;
								}

								function zf_FocusNext(elem, event) {
									if (event.keyCode == 9 || event.keyCode == 16) {
										return;
									}
									if (event.keyCode >= 37 && event.keyCode <= 40) {
										return;
									}
									var compname = elem.getAttribute("compname");
									var inpElemName = elem.getAttribute("name");
									if (inpElemName == compname + "_countrycode") {
										if (elem.value.length == 3) {
											document.getElementsByName(compname + "_first")[0].focus();
										}
									} else if (inpElemName == compname + "_first") {
										if (elem.value.length == 3) {
											document.getElementsByName(compname + "_second")[0].focus();
										}
									}
								}
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div style="display:none" class="contactCircles__circle -grey"> <img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/messages.png"></div>
		<?php elseif ($contact_circle_or_buttons == 'buttons') : ?>
			<div class="contactButtons">
				<div class="contactButtons__button -sales"><?php echo __("Contact Sales", "themetextdomain");  ?></div>
				<div class="contactButtons__button -vip"><?php echo __("Register interest", "themetextdomain");  ?></div>
			</div>
			<div class="contactOffcanvas -sales">
				<?php
					$contact_sales_tag = get_sub_field('contact_sales_tag');
					$contact_sales_title = get_sub_field('contact_sales_title');
					$contact_sales_text = get_sub_field('contact_sales_text');
					$contact_sales_list = get_sub_field('contact_sales_list');
					$contact_sales_map = get_sub_field('contact_sales_map');
					$contact_sales_map = wp_get_attachment_image_url($contact_sales_map['ID'], 'Contact_map');
				 ?>
				<div class="contactOffcanvas__close"><img src="<?php echo TEMP_URI; ?>/assets/images/close-grey.svg" alt=""></div>
				<div class="contactOffcanvas__tag"><?php echo $contact_sales_tag; ?></div>
				<div class="contactOffcanvas__title"><?php echo $contact_sales_title; ?></div>
				<div class="contactOffcanvas__text"><?php echo $contact_sales_text; ?></div>
				<div class="contactOffcanvas__contactList">
					<?php foreach ($contact_sales_list as $item): ?>
                    <?php if($item['option'] === '1') {
							?>
                            <div class="contactOffcanvas__contactItem">
                                <div class="contactOffcanvas__name"><?php echo $item['title'] ?></div>
                                <div class="contactOffcanvas__details"><a href="tel:<?= $item['contact'] ?>"><?php echo $item['contact'] ?></a></div>
                            </div>
							<?php
						}
						elseif ($item['option'] === '2'){
							?>
                            <div class="contactOffcanvas__contactItem">
                                <div class="contactOffcanvas__name"><?php echo $item['title'] ?></div>
                                <div class="contactOffcanvas__details"><?php echo $item['contact'] ?></div>
                                <div class="contactOffcanvas__map"><?php echo $item['office_map'] ?></div>
                            </div>
							<?php
						}
						else{ ?>
                            <div class="contactOffcanvas__contactItem">
                                <div class="contactOffcanvas__name"><?php echo $item['title'] ?></div>
                                <div class="contactOffcanvas__details"><?php echo $item['contact'] ?></div>
                            </div>
					<?php }
					endforeach ?>
				</div>
				<div class="contactOffcanvas__map">
					<img src="<?php echo $contact_sales_map; ?>" alt="">
				</div>
			</div>
			<div class="buttonSection__popup -close">
					<img class="buttonSection__close" src="<?php echo TEMP_URI; ?>/assets/images/close.svg" alt="">
					<div class="buttonSection__popupBox">
					<div id="zf_div_l2OO1lnSpw-ONeuq3kj90gdwJf54Bm9z_a9GUqSatnc"></div>
					<script type="text/javascript">(function() {
				try{
					var f = document.createElement("iframe");
					
					if(window.location.href.includes("?lang=en")) { // change to "?lang=en" & swap src if the default language is Arabic
						f.dataset.src = 'https://forms.zohopublic.com/tilalre/form/Untitled/formperma/l2OO1lnSpw-ONeuq3kj90gdwJf54Bm9z_a9GUqSatnc?zf_lang=en&zf_rszfm=1';

					}
					else {
						f.dataset.src = 'https://forms.zohopublic.com/tilalre/form/Untitled/formperma/l2OO1lnSpw-ONeuq3kj90gdwJf54Bm9z_a9GUqSatnc?zf_rszfm=1';
					}
					
					f.classList.add('-hidden');
					f.style.border="none";
					f.style.height="100%";
					f.style.width="100%";
					f.style.transition="all 0.5s ease";
					var d = document.getElementById("zf_div_l2OO1lnSpw-ONeuq3kj90gdwJf54Bm9z_a9GUqSatnc");
					d.appendChild(f);
					window.addEventListener('message', function (){
					var evntData = event.data;
					if( evntData && evntData.constructor == String ){
					var zf_ifrm_data = evntData.split("|");
					if ( zf_ifrm_data.length == 2 ) {
					var zf_perma = zf_ifrm_data[0];
					var zf_ifrm_ht_nw = ( parseInt(zf_ifrm_data[1], 10) + 15 ) + "px";
					var iframe = document.getElementById("zf_div_l2OO1lnSpw-ONeuq3kj90gdwJf54Bm9z_a9GUqSatnc").getElementsByTagName("iframe")[0];
					if ( (iframe.src).indexOf('formperma') > 0 && (iframe.src).indexOf(zf_perma) > 0 ) {
					var prevIframeHeight = iframe.style.height;
					if ( prevIframeHeight != zf_ifrm_ht_nw ) {
					iframe.style.height = zf_ifrm_ht_nw;
					}
					}
					}
					}
					}, false);
					}catch(e){}
				})();</script>
				</div>
			</div>
			<div class="contactOffcanvas__backdrop"></div>
		<?php endif; ?>
	</div>
	<div class="customModal"> </div>
</div>
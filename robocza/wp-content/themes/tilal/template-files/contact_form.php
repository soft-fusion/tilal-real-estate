<?php
$title_contact = get_sub_field('title_contact');
$phone_contact = get_sub_field('phone_contact');
$email_contact = get_sub_field('email_contact');
$address_contact = get_sub_field('address_contact');
$language = $_GET['lang'] != '' ? '?lang='.$_GET['lang'] : '';
$my_current_lang = apply_filters( 'wpml_current_language', NULL );
if($my_current_lang === 'ar'){
	$url = '/سياسة-الخصوصية/';
}
else{
	$url = '/privacy-policy/?lang=en';
}

?>
<div class="contact" data-section>
    <div class="contact__topImg">
        <div class="contact__map" id="map"></div>
    </div>
    <div class="contact__container">
        <div class="contact__left">
            <div class="contact__title"><?php echo $title_contact; ?></div>
            <div>
                <div class="contact__text">
                    <?php echo __("Phone", "themetextdomain");  ?> <?php echo $phone_contact; ?> <br><?php echo __("E-mail:", "themetextdomain");  ?> <?php echo $email_contact; ?> </div>
                <div class="contact__text">
                    <?php echo $address_contact; ?></div>
                <ul class="contact__links">
                    <?php
                    while ( (have_rows( 'social_media'))) {
                        the_row();
                        $name_social_media = get_sub_field('name_social_media');
                        $url_social_media = get_sub_field('url_social_media');
                        ?>
                        <li class="contact__link"><a href="<?php echo $url_social_media; ?>" title="visit our profile on <?php echo $name_social_media; ?>"><?php echo $name_social_media; ?></a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
            <div class="contact__right">

                <div class="zf-templateWidth"><form action='https://forms.zohopublic.com/tilalre/form/Signup/formperma/YPfRrYfd18OjXnThgY7KEoIYKhtA5uECaORlw2gGyb8/htmlRecords/submit' name='form' method='POST' onSubmit='javascript:document.charset="UTF-8"; return zf_ValidateAndSubmit();' accept-charset='UTF-8' enctype='multipart/form-data' id='form'><input type="hidden" name="zf_referrer_name" value=""><!-- To Track referrals , place the referrer name within the " " in the above hidden input field -->
                        <input type="hidden" name="zf_redirect_url" value=""><!-- To redirect to a specific page after record submission , place the respective url within the " " in the above hidden input field -->
                        <input type="hidden" name="zc_gad" value=""><!-- If GCLID is enabled in Zoho CRM Integration, click details of AdWords Ads will be pushed to Zoho CRM -->
                        <div class="zf-templateWrapper"><!---------template Header Starts Here---------->
                            <ul class="zf-tempHeadBdr"><li class="zf-tempHeadContBdr"><h2 class="zf-frmTitle"><em>Signup</em></h2>
                                    <p class="zf-frmDesc"></p>
                                    <div class="zf-clearBoth"></div></li></ul><!---------template Header Ends Here---------->
                            <!---------template Container Starts Here---------->
                            <div class="zf-subContWrap zf-topAlign"><ul>
                                    <!---------Single Line Starts Here---------->
                                    <li class="zf-tempFrmWrapper zf-small"><label class="zf-labelName"><?php echo __("Your name", "themetextdomain");  ?>
                                            <em class="zf-important">*</em>
                                        </label>
                                        <div class="zf-tempContDiv"><span> <input type="text" name="SingleLine" checktype="c1" value="" maxlength="255" fieldType=1 /></span> <p id="SingleLine_error" class="zf-errorMessage" style="display:none;"><?php echo __("Invalid value", "themetextdomain");  ?></p>
                                        </div><div class="zf-clearBoth"></div></li><!---------Single Line Ends Here---------->
                                    <!---------Email Starts Here---------->
                                    <li class="zf-tempFrmWrapper zf-large"><label class="zf-labelName"><?php echo __("Your email", "themetextdomain");  ?>
                                            <em class="zf-important">*</em>
                                        </label>
                                        <div class="zf-tempContDiv"><span> <input fieldType=9  type="text" maxlength="255" name="Email" checktype="c5" value=""/></span> <p id="Email_error" class="zf-errorMessage" style="display:none;"><?php echo __("Invalid value", "themetextdomain");  ?> </p>
                                        </div><div class="zf-clearBoth"></div></li><!---------Email Ends Here---------->
                                    <!---------Phone Starts Here---------->
                                    <li  class="zf-tempFrmWrapper zf-small"><label class="zf-labelName"><?php echo __("Your phone", "themetextdomain");  ?>
                                        </label>
                                        <div class="zf-tempContDiv zf-phonefld"><div
                                                    class="zf-phwrapper zf-phNumber"
                                            >
<span> <input type="text" compname="PhoneNumber" name="PhoneNumber_countrycode" maxlength="20" checktype="c7" value="" phoneFormat="1" isCountryCodeEnabled=false fieldType="11" id="international_PhoneNumber_countrycode" valType="number" phoneFormatType="1"/>
<label>Number</label> </span>
                                                <div class="zf-clearBoth"></div></div><p id="PhoneNumber_error" class="zf-errorMessage" style="display:none;"><?php echo __("Invalid value", "themetextdomain");  ?></p>
                                        </div><div class="zf-clearBoth"></div></li><!---------Phone Ends Here---------->
                                    <!---------Multiple Line Starts Here---------->
                                    <li class="zf-tempFrmWrapper zf-small"><label class="zf-labelName"><?php echo __("Enter message", "themetextdomain");  ?>
                                            <em class="zf-important">*</em>
                                        </label>
                                        <div class="zf-tempContDiv"><span> <textarea name="MultiLine" checktype="c1" maxlength="65535"></textarea> </span><p id="MultiLine_error" class="zf-errorMessage" style="display:none;"><?php echo __("Invalid value", "themetextdomain");  ?></p>
                                        </div><div class="zf-clearBoth"></div></li><!---------Multiple Line Ends Here---------->
                                    <!--Terms and conditions-->
                                    <li class="zf-tempFrmWrapper">
                                        <div class="zf-tempContDiv" ><div class="zf-termsContainer">
                                                <div class="zf-termsAccept"><input class="zf-checkBoxType zf-flLeft" name="TermsConditions" type="checkbox"><label class="zf-descFld"><?php echo __("I accept the", "themetextdomain");  ?> <a href='<?= $url ?>' title='privacy-policy' target='_blank'><?php echo __("terms & conditions", "themetextdomain");  ?></a>.</label></div></div></div><p class="zf-errorMessage" elname="error" id="TermsConditions_error" style="display:none;"><?php echo __("Invalid value", "themetextdomain");  ?></p>
                                        <div class="clearBoth"></div></li>
                                </ul></div><!---------template Container Starts Here---------->
                            <ul><li class="zf-fmFooter"><button class="zf-submitColor" ><?php echo __("SEND", "themetextdomain");  ?></button></li></ul></div><!-- 'zf-templateWrapper' ends --></form></div><!-- 'zf-templateWidth' ends -->
                <script type="text/javascript">var zf_DateRegex = new RegExp("^(([0][1-9])|([1-2][0-9])|([3][0-1]))[-](Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)[-](?:(?:19|20)[0-9]{2})$");
                    var zf_MandArray = [ "SingleLine", "Email", "MultiLine", "TermsConditions"];
                    var zf_FieldArray = [ "SingleLine", "Email", "PhoneNumber_countrycode", "MultiLine", "TermsConditions"];
                    var isSalesIQIntegrationEnabled = false;
                    var salesIQFieldsArray = [];</script>
            </div>
        </div>
    </div>

<?php
$scrolling_id = get_sub_field('scrolling_id');
$homes_title = get_sub_field('title');
$homes_array = get_field('select_apartments');
global $post;
$post_slug = $post->post_name;
?>
<div class="homesGrid" data-section <?php if ($scrolling_id != '') {
                                      echo 'data-scrolling-id="' . $scrolling_id . '"';
                                    } ?>>
  <hr class="customHr">
  <div class="homesGrid__title"><?php echo $homes_title; ?></div>
  <div class="homesGrid__grid">
    <?php foreach ($homes_array as $home) :
      $item = $home['add_project'];
      $title = $item->post_title;
      $price = get_field('price_apartment', $item->ID);
      $image = get_field('grid_image', $item->ID);
      $image = wp_get_attachment_image_url($image['ID'], 'Home_grid');
      $label = get_field('label', $item->ID);
      $details = get_field('details', $item->ID);
	    $my_current_lang = apply_filters( 'wpml_current_language', NULL );
	    if($my_current_lang === 'ar') {
		    $rewrite = 'بيوت-تلال';
	    }else{
		    $rewrite = 'tilal-homes';
	    }
      $link = home_url('tilal-homes'. $post_slug.'/'.$item->post_name);

    ?>
      <a href="<?php echo $link; ?>" class="homesGrid__item">
        <div class="homesGrid__imageBox" style="background-image:url(<?php echo $image; ?>)">
          <?php if ($label != '') : ?>
            <div class="homesGrid__label"><?php echo $label; ?></div>
          <?php endif; ?>
        </div>
        <div class="homesGrid__main">
          <div class="homesGrid__name">
            <?php echo $title; ?>
          </div>
          <div class="homesGrid__price">
	          <?php if(is_rtl()){
                  echo __("Starting at", "themetextdomain").' '; ?> <span dir="ltr"> <?php echo number_format($price, 0, '.', ' ').' '; ?></span><span><?php echo __("SAR", "themetextdomain"); ?></span><?php
	          }else{
		          echo __("Starting at", "themetextdomain").' '; ?> <span> <?php echo number_format($price, 0, '.', ' '); echo  ' '.__( "SAR", "themetextdomain" ) ; ?></span><?php
	          }
	          ?>
          </div>
          <div class="homesGrid__details">
            <?php foreach ($details as $detail) : ?>
              <div class="homesGrid__info">
                <div class="homesGrid__value"><?php echo $detail['title']; ?></div>
                <div class="homesGrid__text"><?php echo $detail['text']; ?></div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
        <div class="homesGrid__bottom">
          <div class="homesGrid__view"><?php echo __("View home design", "themetextdomain");  ?></div>
        </div>
      </a>
    <?php endforeach; ?>
  </div>
</div>
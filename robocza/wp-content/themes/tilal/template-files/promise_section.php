<?php
$main_title = get_sub_field('main_title');
$show = get_sub_field('show');
$add_image_section = get_sub_field('add_image_section');
$slogan = get_sub_field('slogan');
$image_first_large = get_sub_field('image_first_large');
$image_first_large = wp_get_attachment_image_url($image_first_large['ID'],'Image_Large1');
$image_first_small = get_sub_field('image_first_small');
$image_first_small = wp_get_attachment_image_url($image_first_small['ID'],'Image_Large1');
$image_second_large = get_sub_field('image_second_large');
$image_second_large = wp_get_attachment_image_url($image_second_large['ID'],'Image_Large2');
$image_second_small = get_sub_field('image_second_small');
$image_second_small = wp_get_attachment_image_url($image_second_small['ID'],'Image_Large2');
global $wp;
$url = home_url($wp->request);
?>
<div class="promiseSection" <?php echo $url === home_url() ? '' : 'style="padding-bottom: 0"' ?> data-section>
    <div class="promiseSection__container">
        <hr class="customHr">
        <div class="promiseSection__box">
            <div class="promiseSection__title"><?php echo $main_title; ?> </div>
            <div class="promiseSection__items">
                <?php
                while ( (have_rows( 'information'))) {
                    the_row();
                    $title_promise_section = get_sub_field('title_promise_section');
                    $description_promise_section = get_sub_field('description_promise_section');
                    $icon_promise = get_sub_field('icon_promise');
                    $icon_promise = wp_get_attachment_image_url($icon_promise['ID']);
                    $companies = get_sub_field('companies');
                ?>
                <div class="promiseSectionItem <?php echo $description_promise_section == '' ? '-noText' : '' ?>">
                    <div class="promiseSectionItem__top">
                        <div class="promiseSectionItem__name"><?php echo $title_promise_section; ?></div>
                        <?php if((int)$show === 2){ ?>
                        <div class="promiseSectionItem__number -hidden-on-mobile">
                            <?php echo $companies;  ?>
                        </div>
                        <?php } ?>
                        <img loading="lazy" class="<?php echo (int)$show === 2 ? '-hidden-on-desktop' : '' ?>" src="<?php echo $icon_promise; ?>" alt="">
                    </div>
                    <div class="promiseSectionItem__text"><?php echo $description_promise_section; ?></div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php
    if((int)$add_image_section === 1)
    {
?>
    <div class="promiseSection__imageSection">
        <div class="promiseSection__image">
            <picture>
                <source media="(min-width: 376px)" srcset="<?php echo $image_first_large ?>" type="image/png">
                <source srcset="<?php echo $image_first_small ?>" type="image/png"><img loading="lazy" src="<?php echo $image_first_large ?>" alt="">
            </picture>
        </div>
        <div class="promiseSection__image -second">
            <picture>
                <source media="(min-width: 376px)" srcset="<?php echo $image_second_large ?>" type="image/png">
                <source srcset="<?php echo $image_second_small ?>" type="image/png"><img loading="lazy" src="<?php echo $image_second_large ?>" alt="">
            </picture>
        </div>
    </div>
    <?php
    }
    ?>

    <?php if($slogan) { ?>
    <div class="promiseSection__slogan">
        <?php echo $slogan ?>
    </div>
    <?php
    }
    ?>
</div>

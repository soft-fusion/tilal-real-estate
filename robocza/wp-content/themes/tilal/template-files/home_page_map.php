<?php
$map_title = get_sub_field('map_title');
$map_image = get_sub_field('map_image');
$map_image = wp_get_attachment_image_url($map_image['ID'], '');
$my_current_lang = apply_filters( 'wpml_current_language', NULL );
if($my_current_lang === 'ar'){
	$tilal_home = 'بيوت-تلال';
}
else{
	$tilal_home = 'tilal-homes';
}
$projects_url = home_url($tilal_home);
?>
<div class="mapSection" data-section>
    <hr class="customHr">
    <div class="mapSection__title">
        <?php echo $map_title; ?>
    </div>
    <div class="mapSection__map maps">
        <div class="mapSection__box -open">
            <button class="mapSection__backButton"><img src="<?php echo TEMP_URI; ?>/assets/images/chevron-right.svg" alt="arrow"></button>
            <div class="mapSection__textBox">
                <div class="mapSection__boxTag"><?php echo __("Projects", "themetextdomain"); ?></div>
                <div class="mapSection__boxTitle"><?php echo __("Tilal builds inspiring communities", "themetextdomain"); ?></div>
                <div class="mapSection__boxText">
                    <?php echo __("Find your new home or browse all projects", "themetextdomain"); ?>
                    <a href="<?php echo $projects_url; ?>"><?php echo __("here", "themetextdomain"); ?></a>
                </div>
            </div>
            <button class="mapSection__filtersButton">
                <span class="mapSection__filtersText"><?php echo __("Filters", "themetextdomain"); ?></span>
                <span class="mapSection__filtersCount"><?php echo __("0", "themetextdomain"); ?></span>
            </button>
        </div>
        <div class="mapSection__mapPlace" id="hp-map">
            <div class="mapSection__placeholder"></div>
        </div>
    </div>
    <div class="mapSectionModal">
        <div class="mapSectionModal__backdrop"></div>
        <div class="mapSectionModal__bg">
            <div class="mapSectionModal__close"><img src="<?php echo TEMP_URI; ?>/assets/images/close-black.svg" alt=""></div>
            <div class="mapSectionModal__main">
                <div class="mapSectionModal__title"><?php echo __("Filter projects", "themetextdomain"); ?></div>
                <form class="mapSectionModal__form" id="search-form">
                    <div class="mapSectionModal__control">
                        <label class="mapSectionModal__label"><?php echo __("Location", "themetextdomain"); ?></label>
                        <input class="mapSectionModal__input" type="text" name="location" placeholder="<?php echo __("Enter city", "themetextdomain"); ?>">
                    </div>
                    <div class="mapSectionModal__control">
                        <label class="mapSectionModal__label"><?php echo __("Home type", "themetextdomain"); ?></label>
                        <div class="mapSectionModal__homeTypes">
                            <div class="mapSectionModal__homeType">
                                <input class="mapSectionModal__homeTypeInput" id="mapsection-home-type-apartment" type="checkbox" name="home-type" value="apartment">
                                <label class="mapSectionModal__homeTypeLabel" for="mapsection-home-type-apartment"><?php echo __("Apartment", "themetextdomain"); ?></label>
                            </div>
                            <div class="mapSectionModal__homeType">
                                <input class="mapSectionModal__homeTypeInput" id="mapsection-home-type-villa" type="checkbox" name="home-type" value="villa">
                                <label class="mapSectionModal__homeTypeLabel" for="mapsection-home-type-villa"><?php echo __("Villa", "themetextdomain"); ?></label>
                            </div>
                            <div class="mapSectionModal__homeType">
                                <input class="mapSectionModal__homeTypeInput" id="mapsection-home-type-duplex" type="checkbox" name="home-type" value="duplex">
                                <label class="mapSectionModal__homeTypeLabel" for="mapsection-home-type-duplex"><?php echo __("Duplex", "themetextdomain"); ?></label>
                            </div>
                            <div class="mapSectionModal__homeType">
                                <input class="mapSectionModal__homeTypeInput" id="mapsection-home-type-townhouse" type="checkbox" name="home-type" value="townhouse">
                                <label class="mapSectionModal__homeTypeLabel" for="mapsection-home-type-townhouse"><?php echo __("Townhouse", "themetextdomain"); ?></label>
                            </div>
                            <div class="mapSectionModal__homeType">
                                <input class="mapSectionModal__homeTypeInput" id="mapsection-home-type-penthouse" type="checkbox" name="home-type" value="penthouse">
                                <label class="mapSectionModal__homeTypeLabel" for="mapsection-home-type-penthouse"><?php echo __("Penthouse", "themetextdomain"); ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="mapSectionModal__control">
                        <label class="mapSectionModal__label"><?php echo __("Price range", "themetextdomain"); ?></label>
                        <div class="mapSectionModal__slider">
                            <div class="mapSectionModal__tips">
                                <div class="mapSectionModal__tip -lower"></div>
                                <div class="mapSectionModal__tipLine">-</div>
                                <div class="mapSectionModal__tip -upper"></div>
                            </div>
                            <div class="mapSectionModal__bar" data-start="0" data-end="3000000"></div>
                            <div class="mapSectionModal__barInputs">
                                <input class="mapSectionModal__barInput -lower" type="hidden" name="price-from">
                                <input class="mapSectionModal__barInput -upper" type="hidden" name="price-to">
                            </div>
                        </div>
                    </div>
                    <div class="mapSectionModal__control">
                        <label class="mapSectionModal__label"><?php echo __("Bedrooms", "themetextdomain"); ?></label>
                        <div class="mapSectionModal__number">
                            <div class="mapSectionModal__numberButton"><img src="<?php echo TEMP_URI; ?>/assets/images/minus.svg" alt=""></div>
                            <div class="mapSectionModal__numberPresentation"><?php echo __("Any", "themetextdomain"); ?></div>
                            <input class="mapSectionModal__numberInput" type="hidden" name="bedrooms" value="<?php echo __("Any", "themetextdomain"); ?>" readonly>
                            <div class="mapSectionModal__numberButton"><img src="<?php echo TEMP_URI; ?>/assets/images/plus.svg" alt=""></div>
                        </div>
                    </div>
                    <div class="mapSectionModal__control">
                        <label class="mapSectionModal__label"><?php echo __("Bathrooms", "themetextdomain"); ?></label>
                        <div class="mapSectionModal__number">
                            <div class="mapSectionModal__numberButton"><img src="<?php echo TEMP_URI; ?>/assets/images/minus.svg" alt=""></div>
                            <div class="mapSectionModal__numberPresentation"><?php echo __("Any", "themetextdomain"); ?></div>
                            <input class="mapSectionModal__numberInput" type="hidden" name="bathrooms" value="<?php echo __("Any", "themetextdomain"); ?>" readonly>
                            <div class="mapSectionModal__numberButton"><img src="<?php echo TEMP_URI; ?>/assets/images/plus.svg" alt=""></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="mapSectionModal__bottom">
                <div class="mapSectionModal__clear"><?php echo __("Clear", "themetextdomain"); ?></div>
                <div class="mapSectionModal__resultWrapper">
                    <div class="mapSectionModal__result">
                        <span><?php echo __("Found", "themetextdomain"); ?>:</span>
                        <span class="mapSectionModal__resultCount"><?php echo __("0", "themetextdomain"); ?></span>
                    </div>
                    <div class="mapSectionModal__showButton"><?php echo __("Show", "themetextdomain"); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
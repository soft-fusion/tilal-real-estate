<?php
$scrolling_id = get_sub_field('scrolling_id');
$details = get_sub_field('custom_details');
?>

<div
  class="detailsGrid"
  data-section
  <?php if($scrolling_id != '') { echo 'data-scrolling-id="'.$scrolling_id.'"'; } ?>
>
  <hr class="customHr">
  <div class="detailsGrid__grid">
    <?php foreach($details as $item):
      $icon = $item['icon'];
      $icon = wp_get_attachment_image_url($icon['ID']);
    ?>
    <div class="detailsGrid__item">
      <div class="detailsGrid__textBox">
        <div class="detailsGrid__title"><?php echo $item['title']; ?></div>
        <div class="detailsGrid__text"><?php echo $item['text']; ?></div>
      </div>
      <div class="detailsGrid__icon">
        <img src="<?php echo $icon; ?>" alt="">
      </div>
    </div>
    <?php endforeach; ?>
  </div>
</div>
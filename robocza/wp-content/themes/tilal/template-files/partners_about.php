
<div class="box" data-section>
    <?php
    while ( (have_rows( 'select_partners'))) {
        the_row();
        $add_partner = get_sub_field('add_partner');
        $partner_id = $add_partner->ID;
        $add_link = get_field("add_single_page_link",$partner_id)

    ?>
    <div class="imageText">
        <div class="imageText__container">
            <hr class="customHr">
            <div class="imageText__box">
                <div class="imageText__left">
                    <div class="imageText__title"><?php echo get_the_title($partner_id) ?></div>
                    <div class="imageText__imageBox"><img loading="lazy" class="imageText__image" src="<?php echo get_the_post_thumbnail_url($partner_id) ?>" alt=""><img loading="lazy" class="imageText__image -mobile" src="<?php echo get_the_post_thumbnail_url($partner_id) ?>" alt=""></div>
                </div>
                <div class="imageText__right">
                    <div>
                        <?php
                        echo $add_partner->post_content;
                        ?>
                    </div>
                    <?php if((int)$add_link[0] === 1){ ?>
                    <a class="imageText__readMore" href="<?php echo get_the_permalink($partner_id) ?>" title="read more"><?php echo __("read more", "themetextdomain");  ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

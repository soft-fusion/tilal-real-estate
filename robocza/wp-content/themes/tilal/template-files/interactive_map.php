<?php
$scrolling_id = get_sub_field('scrolling_id');
$title = get_sub_field('title');
$map = get_sub_field('map');
$image = get_sub_field('image');
$image = wp_get_attachment_image_url($image['ID'], 'Banner_Small');
?>

<div
  class="interactiveMap"
  data-section
  <?php if($scrolling_id != '') { echo 'data-scrolling-id="'.$scrolling_id.'"'; } ?>
>
  <hr class="customHr">
  <div class="interactiveMap__container">
    <div class="interactiveMap__title"><?php echo $title; ?></div>
  </div>
  <div class="interactiveMap__mapWrapper">
    <?php if ($map != null): ?>
    <div class="interactiveMap__map"></div>
    <?php elseif($image != null): ?>
    <div class="interactiveMap__placeholder">
      <img src="<?php echo $image; ?>" alt="">
    </div>
    <?php else: ?>
    <div class="interactiveMap__placeholder">
      <img src="<?php echo TEMP_URI; ?>/assets/images/interactive-map-placeholder.png" alt="">
    </div>
    <?php endif; ?>
  </div>
</div>
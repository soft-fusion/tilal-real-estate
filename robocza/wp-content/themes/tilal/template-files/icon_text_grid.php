<?php
$title = get_sub_field('title');
?>

<div class="iconTextGrid" data-section>
    <hr class="customHr">
    <div class="iconTextGrid__grid">
        <?php
        while ( (have_rows('add_icon_text_item'))) {
            the_row();
            $title_icon_text = get_sub_field('title_icon_text');
            $text_icon_text = get_sub_field('text_icon_text');
            $icon_icon_text = get_sub_field('icon_icon_text');
            $icon_icon_text = wp_get_attachment_image_url($icon_icon_text['ID']);
        ?>
        <div class="iconTextGrid__item">
        <img loading="lazy" class="iconTextGrid__icon" src="<?php echo $icon_icon_text; ?>" alt="">
        <div class="iconTextGrid__title"><?php echo $title_icon_text; ?></div>
        <div class="iconTextGrid__text"><?php echo $text_icon_text; ?></div>
        </div>
        <?php } ?>
    </div>
</div>
<?php
$top_left = get_sub_field('top_left');
$top_right = get_sub_field('top_right');
$bottom_left = get_sub_field('bottom_left');
$bottom_right = get_sub_field('bottom_right');
?>

<div class="scatterGallery" data-section>
  <div class="scatterGallery__image -topLeft">
    <?php
      $top_left_image = $top_left['image'];
      $top_left_image = wp_get_attachment_image_url($top_left_image['ID'], 'Scatter_top_left');
      $top_left_caption = $top_left['caption'];
    ?>
    <img src="<?php echo $top_left_image; ?>" alt="">
    <div class="scatterGallery__caption"><?php echo $top_left_caption; ?></div>
  </div>
  <div class="scatterGallery__image -topRight">
    <?php
      $top_right_image = $top_right['image'];
      $top_right_image = wp_get_attachment_image_url($top_right_image['ID'], 'Scatter_top_right');
      $top_right_caption = $top_right['caption'];
    ?>
    <img src="<?php echo $top_right_image; ?>" alt="">
    <div class="scatterGallery__caption"><?php echo $top_right_caption; ?></div>
  </div>
  <div class="scatterGallery__image -bottomLeft">
    <?php
      $bottom_left_image = $bottom_left['image'];
      $bottom_left_image = wp_get_attachment_image_url($bottom_left_image['ID'], 'Scatter_bottom_left');
      $bottom_left_caption = $bottom_left['caption'];
    ?>
    <img src="<?php echo $bottom_left_image; ?>" alt="">
    <div class="scatterGallery__caption"><?php echo $bottom_left_caption; ?></div>
  </div>
  <div class="scatterGallery__image -bottomRight">
    <?php
      $bottom_right_image = $bottom_right['image'];
      $bottom_right_image = wp_get_attachment_image_url($bottom_right_image['ID'], 'Scatter_bottom_right');
      $bottom_right_caption = $bottom_right['caption'];
    ?>
    <img src="<?php echo $bottom_right_image; ?>" alt="">
    <div class="scatterGallery__caption"><?php echo $bottom_right_caption; ?></div>
  </div>
</div>
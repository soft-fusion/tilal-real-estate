<?php
$image_main_description = get_sub_field('image_main_description');
$first_image_description = get_sub_field('first_image_description');
$second_image_description = get_sub_field('second_image_description');
$first_image_big = get_sub_field('first_image_big');
$first_image_big = wp_get_attachment_image_url($first_image_big['ID'], 'Image_about1');
$first_image_small = get_sub_field('first_image_small');
$first_image_small = wp_get_attachment_image_url($first_image_small['ID'], 'Image_about1');
$second_image_big = get_sub_field('second_image_big');
$second_image_big = wp_get_attachment_image_url($second_image_big['ID'], 'Image_about2');
$second_image_small = get_sub_field('second_image_small');
$second_image_small = wp_get_attachment_image_url($second_image_small['ID'], 'Image_about2');

?>

<div class="imagesSection" data-section>
    <div class="imagesSection__container">
        <p class="imagesSection__text"><?php echo $image_main_description; ?></p>
        <div class="imagesSection__item -image-to-left-on-mobile">
            <picture>
                <source media="(min-width: 611px)" srcset="<?php echo $first_image_big; ?>" type="image/png">
                <source srcset="<?php echo $first_image_small; ?>" type="image/png"><img loading="lazy" class="imagesSection__img" src="<?php echo $first_image_big; ?>" alt="">
            </picture><img loading="lazy" class="imagesSection__img-mobile" src="<?php echo $first_image_small; ?>" alt="">
            <div class="imagesSection__caption"><?php echo $first_image_description; ?></div>
        </div>
        <div class="imagesSection__item -image-to-right-on-mobile">
            <picture>
                <source media="(min-width: 611px)" srcset="<?php echo $second_image_big; ?>" type="image/png">
                <source srcset="<?php echo $second_image_small; ?>" type="image/png"><img loading="lazy" class="imagesSection__img" src="<?php echo $second_image_big; ?>" alt="">
            </picture><img loading="lazy" class="imagesSection__img-mobile" src="<?php echo $second_image_small; ?>" alt="">
            <div class="imagesSection__caption"><?php echo $second_image_description; ?></div>
        </div>
    </div>
</div>

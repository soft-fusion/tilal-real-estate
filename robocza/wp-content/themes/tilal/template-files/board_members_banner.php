<?php
$banner_image_board = get_sub_field('banner_image_board');
$banner_image_board = wp_get_attachment_image_url($banner_image_board['ID'], '');
$banner_mobile = get_sub_field('banner_mobile');
$add_class_mobile = '';
$add_class_mobiles = '';
if((int)$banner_mobile === 1)
{
    $add_class_mobile = '-leftOnMobile';
}
elseif ((int)$banner_mobile === 2)
{
    $add_class_mobile = '-rightOnMobile';
}
elseif ((int)$banner_mobile === 3)
{
    $add_class_mobile = '-centerRightOnMobile';
}
elseif ((int)$banner_mobile === 4)
{
    $add_class_mobiles = '-withoutScaling ';
}
else
{
    $add_class_mobile = '';
}
?>
<div class="banner -onlyImg <?php echo $add_class_mobiles; ?>"><img loading="lazy" class="banner__photoInBg <?php echo $add_class_mobile; ?>" src="<?php echo $banner_image_board ?>"></div>
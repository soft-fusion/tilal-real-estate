
<div class="logos" data-section>
    <div class="logos__container">
        <hr class="customHr">
        <div class="logos__title"><?= get_sub_field('title_logo_section'); ?></div>
        <div class="logos__box">
            <?php
            while ( (have_rows( 'add_logo'))): the_row();
                $image_secton_logo = get_sub_field('image_secton_logo');
                $url_logo_section = get_sub_field('url_logo_section');
            ?>
                <a class="logos__item" href="<?= $url_logo_section; ?>">
                    <img loading="lazy" class="logos__image" src="<?= $image_secton_logo; ?>" alt="">
                </a>
            <?php endwhile; ?>
        </div>
    </div>
</div>

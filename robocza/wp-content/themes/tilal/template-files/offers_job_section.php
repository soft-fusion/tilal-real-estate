<?php
    $offer_job= get_sub_field('offers_job');
    ?>

<div class="jobsOffers" data-section>
    <div class="jobsOffers__container">
        <hr class="customHr">
        <div class="jobsOffers__box">
            <div class="jobsOffers__title"><?php echo __("job openings", "themetextdomain");  ?></div>
            <div class="jobsOffers__offers">
                <?php
                    echo $offer_job;
                ?>
            </div>
        </div>
    </div>
</div>
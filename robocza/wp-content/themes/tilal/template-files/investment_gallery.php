<?php
$scrolling_id = get_sub_field('scrolling_id');
$title = get_sub_field('title');
$images_array = get_sub_field('images');
?>

<div
  class="investmentGallery"
  data-section
  <?php if($scrolling_id != '') { echo 'data-scrolling-id="'.$scrolling_id.'"'; } ?>
>
  <div class="investmentGallery__container">
    <hr class="customHr">
    <div class="investmentGallery__title"><?php echo $title; ?></div>
    <div class="investmentGallery__grid">
      <?php foreach($images_array as $item):
        $image = $item['image'];
        $image = wp_get_attachment_image_url($image['ID'], 'Investment_gallery');
        $caption = $item['caption'];
      ?>
      <div class="investmentGallery__box"><img class="investmentGallery__image" src="<?php echo $image; ?>">
        <div class="investmentGallery__text"><?php echo $caption; ?></div>
      </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>
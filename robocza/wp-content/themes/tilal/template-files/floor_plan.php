<?php
$scrolling_id = get_sub_field('scrolling_id');
$title = get_sub_field('title');
$floors_array = get_sub_field('floors');
$description = get_sub_field('description');
$list_title = get_sub_field('list_title');
$list = get_sub_field('list');
?>

<div
  class="floorPlan"
  data-section
  <?php if($scrolling_id != '') { echo 'data-scrolling-id="'.$scrolling_id.'"'; } ?>
>
  <hr class="customHr">
  <div class="floorPlan__title"><?php echo $title; ?></div>
  <div class="floorPlan__buttonHolder">
    <?php foreach($floors_array as $index=>$floor): ?>
    <div class="floorPlan__button <?php if($index == 0){ echo '-active'; } ?>"><?php echo $floor['floor_name']; ?></div>
    <?php endforeach; ?>
  </div>
  <div class="floorPlan__imageHolder">
    <?php foreach($floors_array as $index=>$floor):
      $image = $floor['image'];
      $image = wp_get_attachment_image_url($image['ID'], 'Floor_plan');
    ?>
    <img class="floorPlan__image <?php if($index == 0){ echo '-active'; } ?>" src="<?php echo $image; ?>">
    <?php endforeach; ?>
  </div>
  <div class="floorPlan__bottomBox">
    <div class="floorPlan__text">
      <?php echo $description; ?>
    </div>
    <div class="floorPlan__box">
      <div class="floorPlan__tag"><?php echo $list_title; ?></div>
      <ul class="floorPlan__list">
        <?php foreach($list as $item): ?>
        <li><?php echo $item['text']; ?></li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>
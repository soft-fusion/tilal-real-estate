<?php
$tabs_array = get_sub_field('tabs');
$post = get_post();
$title = $post->post_title;
?>

<div class="tabs">
  <div class="tabs__scrollWatcher">
    <div class="tabs__container">
      <div class="tabs__trigger">
        <span><?php echo $title; ?> <?php echo __("Navigation", "themetextdomain"); ?></span>
        <img src="<?php echo TEMP_URI; ?>/assets/images/arrow-down-black.svg" alt="">
      </div>
      <div class="tabs__list">
        <?php foreach($tabs_array as $index=>$tab):
          $text = $tab['text'];
          $scrollto_id = $tab['scrolling_id'];
        ?>
        <div class="tabs__item <?php if($index == 0) { echo '-active'; } ?>" data-scrollto-id="<?php echo $scrollto_id; ?>"><span><?php echo $text; ?></span></div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>
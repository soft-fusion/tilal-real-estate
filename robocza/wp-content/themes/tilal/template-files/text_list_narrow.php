<?php
$title = get_sub_field('title');
$description = get_sub_field('description');
$list_title = get_sub_field('list_title');
$list = get_sub_field('list');
?>

<div class="textListNarrow" data-section>
  <div class="textListNarrow__container">
    <hr class="customHr">
    <div class="textListNarrow__title"><?php echo $title; ?></div>
    <div class="textListNarrow__box">
      <div class="textListNarrow__left">
        <div class="textListNarrow__text"><?php echo $description; ?></div>
        <div class="textListNarrow__tag"><?php echo $list_title; ?></div>
        <ul class="textListNarrow__list">
          <?php foreach ($list as $item) : ?>
            <li><?php echo $item['text']; ?></li>
          <?php endforeach; ?>
        </ul>
      </div>
      <div class="textListNarrow__right">
      </div>
    </div>
  </div>
</div>
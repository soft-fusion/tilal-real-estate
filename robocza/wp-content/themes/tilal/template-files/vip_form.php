<?php
$title_vip = get_sub_field('title_vip');
$description_vip = get_sub_field('description_vip');
$list_title_vip = get_sub_field('list_title_vip');
$list_vip = get_sub_field('list_vip');
$language = $_GET['lang'] != '' ? '?lang='.$_GET['lang'] : '';
?>

<div class="joinOurVipList" data-section>
    <div class="joinOurVipList__container">
        <hr class="customHr">
        <div class="joinOurVipList__title"><?php echo $title_vip; ?></div>
        <div class="joinOurVipList__box">
            <div class="joinOurVipList__left">
							<div class="joinOurVipList__text"><?php echo $description_vip; ?></div>
              <div class="joinOurVipList__tag"><?php echo $list_title_vip; ?></div>
							<ul class="joinOurVipList__list">
								<?php foreach($list_vip as $item): ?>
									<li><?php echo $item['text']; ?></li>
								<?php endforeach; ?>
							</ul>
            </div>
            <div class="joinOurVipList__right">
                <!-- Note :
   - You can modify the font style and form style to suit your website. 
   - Code lines with comments Do not remove this code are required for the form to work properly, make sure that you do not remove these lines of code. 
   - The Mandatory check script can modified as to suit your business needs. 
   - It is important that you test the modified form before going live.-->
<div id='crmWebToEntityForm' class='zcwf_lblLeft crmWebToEntityForm' style='background-color: white;color: black;max-width: 600px;'>
  <meta name='viewport' content='width=device-width, initial-scale=1.0'>
   <META HTTP-EQUIV ='content-type' CONTENT='text/html;charset=UTF-8'>
   <form action='https://crm.zoho.com/crm/WebToContactForm' name=WebToContacts4549824000001335009 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory4549824000001335009()' accept-charset='UTF-8'>
 <input type='text' style='display:none;' name='xnQsjsdp' value='defead8c71a8df133075880868173b8b3a2308d74cb1dc3878d3b68e23b2ab93'></input> 
 <input type='hidden' name='zc_gad' id='zc_gad' value=''></input> 
 <input type='text' style='display:none;' name='xmIwtLD' value='d1dd95473602a9cd4f0d74f77d38b97a043d3191cc2d358e1a60033384befb16'></input> 
 <input type='text'  style='display:none;' name='actionType' value='Q29udGFjdHM='></input>
 <input type='text' style='display:none;' name='returnURL' value='https&#x3a;&#x2f;&#x2f;tilalre.com&#x2f;' > </input>
	 <!-- Do not remove this code. -->
<style>
html,body{
	margin: 0px;
}
#crmWebToEntityForm.zcwf_lblLeft {
	width:100%;
	padding: 25px;
	margin: 0 auto;
	box-sizing: border-box;
}
#crmWebToEntityForm.zcwf_lblLeft * {
	box-sizing: border-box;
}
#crmWebToEntityForm{text-align: left;}
#crmWebToEntityForm * {
	direction: ltr;
}
.zcwf_lblLeft .zcwf_title {
	word-wrap: break-word;
	padding: 0px 6px 10px;
	font-weight: bold;
}
.zcwf_lblLeft .zcwf_col_fld input[type=text], .zcwf_lblLeft .zcwf_col_fld textarea {
	width: 60%;
	border: 1px solid #ccc !important;
	resize: vertical;
	border-radius: 2px;
	float: left;
}
.zcwf_lblLeft .zcwf_col_lab {
	width: 30%;
	word-break: break-word;
	padding: 0px 6px 0px;
	margin-right: 10px;
	margin-top: 5px;
	float: left;
	min-height: 1px;
}
.zcwf_lblLeft .zcwf_col_fld {
	float: left;
	width: 68%;
	padding: 0px 6px 0px;
	position: relative;
	margin-top: 5px;
}
.zcwf_lblLeft .zcwf_privacy{padding: 6px;}
.zcwf_lblLeft .wfrm_fld_dpNn{display: none;}
.dIB{display: inline-block;}
.zcwf_lblLeft .zcwf_col_fld_slt {
	width: 60%;
	border: 1px solid #ccc;
	background: #fff;
	border-radius: 4px;
	font-size: 12px;
	float: left;
	resize: vertical;
}
.zcwf_lblLeft .zcwf_row:after, .zcwf_lblLeft .zcwf_col_fld:after {
	content: '';
	display: table;
	clear: both;
}
.zcwf_lblLeft .zcwf_col_help {
	float: left;
	margin-left: 7px;
	font-size: 12px;
	max-width: 35%;
	word-break: break-word;
}
.zcwf_lblLeft .zcwf_help_icon {
	cursor: pointer;
	width: 16px;
	height: 16px;
	display: inline-block;
	background: #fff;
	border: 1px solid #ccc;
	color: #ccc;
	text-align: center;
	font-size: 11px;
	line-height: 16px;
	font-weight: bold;
	border-radius: 50%;
}
.zcwf_lblLeft .zcwf_row {margin: 15px 0px;}
.zcwf_lblLeft .formsubmit {
	margin-right: 5px;
	cursor: pointer;
	color: #333;
	font-size: 12px;
}
.zcwf_lblLeft .zcwf_privacy_txt {
	color: rgb(0, 0, 0);
	font-size: 12px;
	font-family: Arial;
	display: inline-block;
	vertical-align: top;
	color: #333;
	padding-top: 2px;
	margin-left: 6px;
}
.zcwf_lblLeft .zcwf_button {
	font-size: 12px;
	color: #333;
	border: 1px solid #ccc;
	padding: 3px 9px;
	border-radius: 4px;
	cursor: pointer;
	max-width: 120px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
.zcwf_lblLeft .zcwf_tooltip_over{
	position: relative;
}
.zcwf_lblLeft .zcwf_tooltip_ctn{
	position: absolute;
	background: #dedede;
	padding: 3px 6px;
	top: 3px;
	border-radius: 4px;word-break: break-all;
	min-width: 50px;
	max-width: 150px;
	color: #333;
}
.zcwf_lblLeft .zcwf_ckbox{
	float: left;
}
.zcwf_lblLeft .zcwf_file{
	width: 55%;
	box-sizing: border-box;
	float: left;
}
.clearB:after{
	content:'';
	display: block;
	clear: both;
}
@media all and (max-width: 600px) {
	.zcwf_lblLeft .zcwf_col_lab, .zcwf_lblLeft .zcwf_col_fld {
		width: auto;
		float: none !important;
	}
	.zcwf_lblLeft .zcwf_col_help {width: 40%;}
}
</style>
<div class='zcwf_title' style='max-width: 600px;color: black;'>Contact Requests</div>
<div class='zcwf_row'><div class='zcwf_col_lab' style='font-size:12px; font-family: Arial;'><label for='Last_Name'><?php echo __("Your name", "themetextdomain");  ?><span style='color:red;'>*</span></label></div><div class='zcwf_col_fld'><input type='text' id='Last_Name' name='Last Name' maxlength='80'></input><div class='zcwf_col_help'></div></div></div>
<div class='zcwf_row'><div class='zcwf_col_lab' style='font-size:12px; font-family: Arial;'><label for='Email'><?php echo __("Your email", "themetextdomain");  ?><span style='color:red;'>*</span></label></div><div class='zcwf_col_fld'><input type='text' ftype='email' id='Email' name='Email' maxlength='100'></input><div class='zcwf_col_help'></div></div></div>
<div class='zcwf_row'><div class='zcwf_col_lab' style='font-size:12px; font-family: Arial;'><label for='Phone'><?php echo __("Your phone", "themetextdomain");  ?></label></div><div class='zcwf_col_fld'><input type='text' id='Phone' name='Phone' maxlength='50'></input><div class='zcwf_col_help'></div></div></div>
<div class='zcwf_row'><div class='zcwf_privacy'><div class='dIB vat' align='left'><div class='displayPurpose  f13'><label class='newCustomchkbox-md dIB w100per'><input autocomplete='off' id='privacyTool4549824000001335009' type='checkbox' name='privacyTool' onclick='disableErr4549824000001335009()'></label></div></div><div class='dIB zcwf_privacy_txt' style='font-size: 12px;font-family:Arial;color: black;'><?php echo __("I accept the", "themetextdomain");  ?> <a href='/privacy-policy/<?=$language?>' title='privacy-policy' target='_blank'><?php echo __("terms & conditions", "themetextdomain");  ?></a></div> <div  id='privacyErr4549824000001335009' style='font-size:12px;color:red;padding-left: 5px;visibility:hidden;'><?php echo __("Please accept this", "themetextdomain");  ?></div></div></div><div class='zcwf_row'><div class='zcwf_col_lab'></div><div class='zcwf_col_fld'><input type='submit' id='formsubmit' class='formsubmit zcwf_button' value='<?php echo __("JOIN OUR VIP LIST", "themetextdomain");  ?>' title='Submit'><input type='reset' class='zcwf_button' name='reset' value='Reset' title='Reset'></div></div>

	<script>
		function privacyAlert4549824000001335009()
		{
			var privacyTool = document.getElementById('privacyTool4549824000001335009');
			var privacyErr = document.getElementById('privacyErr4549824000001335009');
			if(privacyTool !=undefined && !privacyTool.checked )
			{
				privacyErr.style.visibility='visible';
				privacyTool.focus();
				return false;
			}
			return true;
		}
		function disableErr4549824000001335009()
		{
			var privacyTool = document.getElementById('privacyTool4549824000001335009');
			var privacyErr = document.getElementById('privacyErr4549824000001335009');
			if(privacyTool !=undefined && privacyTool.checked && privacyErr !=undefined )
			{
				privacyErr.style.visibility='hidden';
			}
		}
	function validateEmail4549824000001335009()
	{
		var form = document.forms['WebToContacts4549824000001335009'];
		var emailFld = form.querySelectorAll('[ftype=email]');
		var i;
		for (i = 0; i < emailFld.length; i++)
		{
			var emailVal = emailFld[i].value;
			if((emailVal.replace(/^\s+|\s+$/g, '')).length!=0 )
			{
				var atpos=emailVal.indexOf('@');
				var dotpos=emailVal.lastIndexOf('.');
				if (atpos<1 || dotpos<atpos+2 || dotpos+2>=emailVal.length)
				{
					alert('Please enter a valid email address. ');
					emailFld[i].focus();
					return false;
				}
			}
		}
		return true;
	}

 	  function checkMandatory4549824000001335009() {
		var mndFileds = new Array('Last Name','Email');
		var fldLangVal = new Array('Your\x20name','Your\x20email');
		for(i=0;i<mndFileds.length;i++) {
		  var fieldObj=document.forms['WebToContacts4549824000001335009'][mndFileds[i]];
		  if(fieldObj) {
			if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
			 if(fieldObj.type =='file')
				{ 
				 alert('Please select a file to upload.'); 
				 fieldObj.focus(); 
				 return false;
				} 
			alert(fldLangVal[i] +' cannot be empty.'); 
   	   	  	  fieldObj.focus();
   	   	  	  return false;
			}  else if(fieldObj.nodeName=='SELECT') {
  	   	   	 if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
				alert(fldLangVal[i] +' cannot be none.'); 
				fieldObj.focus();
				return false;
			   }
			} else if(fieldObj.type =='checkbox'){
 	 	 	 if(fieldObj.checked == false){
				alert('Please accept  '+fldLangVal[i]);
				fieldObj.focus();
				return false;
			   } 
			 } 
			 try {
			     if(fieldObj.name == 'Last Name') {
				name = fieldObj.value;
 	 	 	    }
			} catch (e) {}
		    }
		}
		if(!validateEmail4549824000001335009()){return false;}
		
		if(!privacyAlert4549824000001335009()){return false;}
		document.querySelector('.crmWebToEntityForm .formsubmit').setAttribute('disabled', true);
	}

function tooltipShow4549824000001335009(el){
	var tooltip = el.nextElementSibling;
	var tooltipDisplay = tooltip.style.display;
	if(tooltipDisplay == 'none'){
		var allTooltip = document.getElementsByClassName('zcwf_tooltip_over');
		for(i=0; i<allTooltip.length; i++){
			allTooltip[i].style.display='none';
		}
		tooltip.style.display = 'block';
	}else{
		tooltip.style.display='none';
	}
}
</script>
	<!-- Do not remove this --- Analytics Tracking code starts --><script id='wf_anal' src='https://crm.zohopublic.com/crm/WebFormAnalyticsServeServlet?rid=d1dd95473602a9cd4f0d74f77d38b97a043d3191cc2d358e1a60033384befb16giddefead8c71a8df133075880868173b8b3a2308d74cb1dc3878d3b68e23b2ab93gid5cf99212d7958dbcd176c761c6ce0a4d7e43e277bdc5d8b9c12010015418942cgid14f4ec16431e0686150daa43f3210513'></script><!-- Do not remove this --- Analytics Tracking code ends. --></form>
</div>
            </div>
        </div>
    </div>
</div>

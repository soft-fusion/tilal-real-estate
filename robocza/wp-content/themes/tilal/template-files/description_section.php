<?php
$description = get_sub_field('description');
$add_image = get_sub_field('add_image');
$add_text_image = get_sub_field('add_text_image');
$image_section = get_sub_field('image_section');
$image_section = wp_get_attachment_image_url($image_section['ID'], 'Banner_small');
?>

<div class="careerInfo" data-section>
    <div class="careerInfo__container">
        <div class="careerInfo__scroll"> <img loading="lazy" src="<?php echo TEMP_URI; ?>/assets/images/arrow-down-black.svg"></div>
        <div class="careerInfo__box">
            <div class="careerInfo__text">
                <?php echo $description ?>
            </div>
            <?php
            if((int)$add_image === 1)
            {
            ?>
                    <div>
                        <img loading="lazy" class="careerInfo__img" src="<?php  echo $image_section; ?>" alt="">
                        <img loading="lazy" class="careerInfo__img-mobile" src="<?php  echo $image_section; ?>" alt="">
                    </div>
                <div class="careerInfo__smallText"><?php echo $add_text_image; ?></div>
            <?php } ?>
        </div>
    </div>
</div>

<?php
$link_or_media = get_sub_field('link_or_media');
$video = get_sub_field('video');
$video = wp_get_attachment_url($video['ID']);
$video_link = get_sub_field('video_link');
$video_caption = get_sub_field('caption');
?>

<div class="videoTour">
  <div class="videoTour__container">
    <div class="videoTour__video">
      <video class="videoTour__player" width="1400" id="video" height="auto" playsinline loop
        src="<?php echo $link_or_media == 'link' ? $video_link : $video ; ?>"
      >
        Your browser does not support the video tag.
      </video>
      <div class="videoTour__button" type="button" id="play_button"></div>
    </div>
    <div class="videoTour__text"><?php echo $video_caption; ?></div>
  </div>
</div>
<?php
$four_column = get_sub_field('four_column');
?>
<div class="tlr" data-section>
    <hr class="customHr">
    <div class="tlr__container <?php echo $four_column === true ? '-fourColumn' : '' ?>">
        <?php
        while ( (have_rows('add_items'))) {
            the_row();
            $item_type = get_sub_field('item_type');
            $link_text = get_sub_field('link_text');
            $link_url = get_sub_field('link_url');
            $link_color = get_sub_field('link_color');
            $item_text = get_sub_field('item_text');
        ?>
        <div class="tlr__item">
            <?php if ($item_type == 'link') {?>
                <a href="<?php echo $link_url; ?>" class="tlr__link -<?php echo $link_color; ?>"><?php echo $link_text; ?></a>
            <?php } else {?>
                <div class="tlr__box">
                    <?php echo $item_text; ?>
                </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
</div>
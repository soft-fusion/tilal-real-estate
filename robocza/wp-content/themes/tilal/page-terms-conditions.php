<?php /* Template Name: Terms & Conditions */ ?>

<?php
get_header('text');

?>
    <div class="textSection" data-section="">
            <?php
            $args = get_field('add_element');
            foreach ($args as $counter => $arg):
            ?>

            <div class="textSection__container -centerText">
            <div class="textSection__box" style="<?= $counter === 0 ? '' : 'margin-top: 40px' ?>">
                <div class="textSection__tag" id="<?= $arg['id_element'] ?>" name="<?= $arg['id_element'] ?>"><?php echo $arg['title_terms'] ?></div>
                <div class="textSection__text">
                    <?php
                    echo $arg['description_terms'];
                    ?>
                </div>
            </div>
            </div>

            <?php endforeach; ?>
    </div>
<?php
get_footer();


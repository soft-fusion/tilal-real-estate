<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <title>TILAL</title>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="<?php bloginfo('description'); ?>" />
    <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>

    <meta charset="utf-8">
    <meta name="title" content="Tilal">
    <meta name="description" content="Tilal">
    <meta name="keywords" content="Szablon keywords">
    <meta name="facebook-domain-verification" content="bstyfgq5mhacidc5ypnj0wi8b3q3s7" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="<?php echo TEMP_URI; ?>/assets/styles/main83.min.css" type="text/css">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo TEMP_URI; ?>/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo TEMP_URI; ?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo TEMP_URI; ?>/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo TEMP_URI; ?>/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo TEMP_URI; ?>/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <img width="9999" height="9999" style="pointer-events: none; position: absolute; top: 0; left: 0; z-index: -50; width: 99vw; height: 99vh; max-width: 99vw; max-height: 99vh;" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA5OTk5IDk5OTkiLz4=">
    <header class="header -alwaysWhite">
        <div class="header__container"> <a class="header__logo" href="<?php echo home_url(); ?>" title="go to main page"><img class="-first" src="<?php echo TEMP_URI; ?>/assets/images/logo.svg" alt=""><img class="-second" src="<?php echo TEMP_URI; ?>/assets/images/logo-color.svg" alt=""></a>
            <div class="header__mobile">
                <div class="search">
                    <button class="search__button -mobile"></button>
                </div>
                <div class="header__iconMobile -menuOpen"></div>
            </div>
            <div class="header__right">
                <ul class="menu -desktop">
                    <?php
                    $args = array(
                        'theme_location' => 'top_menu',
                        'container' => false,
                        'item_spacing' => true,
                        'walker' => new Top_Menu_Walker(),
                        'menu_class' => 'menu__inner'
                    );
                    wp_nav_menu($args);

                    ?>
                    <li class="menu__item -mobile">
                        <div class="menu__link">language </div>
                        <ul class="menu__language">
                            <li class="menu__subItem" title="English"><img src="assets/images/english.png">English</li>
                            <li class="menu__subItem" title="Arabic"><a href="https://newtilalre.widelab.co/about/?lang=ar"><img src="assets/images/secondLanguage.png">Arabic</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="menu -mobile">
                    <?php
                    $args = array(
                        'theme_location' => 'top_menu_mobile',
                        'container' => false,
                        'item_spacing' => true,
                        'walker' => new Top_Menu_Mobile_Walker(),
                        'menu_class' => 'menu__inner'
                    );
                    wp_nav_menu($args);

                    ?>
                </ul>
                <div class="header__buttons">
                    <div class="search">
                        <button class="search__button -desktop"><?php echo __("Search for home", "themetextdomain");  ?></button>
                    </div>
                    <div class="header__toggleL">
                        <?php echo do_shortcode("[wpml_language_selector_widget]"); ?>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="searchOffCanvas">
        <div class="searchOffCanvas__backdrop"></div>
        <div class="searchOffCanvas__bg">
            <div class="searchOffCanvas__close"><img src="<?php echo TEMP_URI; ?>/assets/images/close-black.svg" alt=""></div>
            <div class="searchOffCanvas__top">
                <div class="searchOffCanvas__title"><?php echo __("Search for home", "themetextdomain"); ?></div>
                <div class="searchOffCanvas__row">
                    <div class="searchOffCanvas__mapButton">
                        <button class="searchOffCanvas__button"><?php echo __("Search by map", "themetextdomain"); ?></button>
                    </div>
                    <div class="searchOffCanvas__smallText"><?php echo __("OR", "themetextdomain"); ?></div>
                    <div class="searchOffCanvas__inputPlace">
                        <input class="searchOffCanvas__input" type="text" placeholder="<?php echo __("Enter city", "themetextdomain"); ?>">
                        <button class="searchOffCanvas__searchButton"><img src="<?php echo TEMP_URI; ?>/assets/images/search-green.svg" alt=""></button>
                    </div>
                </div>
            </div>
            <div class="searchOffCanvas__bottom">
                <div class="searchOffCanvas__text"><?php echo __("Search by city", "themetextdomain"); ?></div>
                <div class="searchOffCanvas__cities"></div>
            </div>
        </div>
    </div>
    <div class="searchMap">
        <div class="searchMap__map maps" id="search-map"></div>
        <div class="searchMap__buttons">
            <button class="searchMap__backButton"><img src="<?php echo TEMP_URI; ?>/assets/images/chevron-right.svg" alt=""></button>
            <button class="searchMap__filtersButton">
                <span class="searchMap__filtersText"><?php echo __("Filters", "themetextdomain"); ?></span>
                <span class="searchMap__filtersCount"><?php echo __("0", "themetextdomain"); ?></span>
            </button>
        </div>
    </div>
    <div class="searchFiltersModal">
        <div class="searchFiltersModal__backdrop"></div>
        <div class="searchFiltersModal__bg">
            <div class="searchFiltersModal__close"><img src="<?php echo TEMP_URI; ?>/assets/images/close-black.svg" alt=""></div>
            <div class="searchFiltersModal__main">
                <div class="searchFiltersModal__title"><?php echo __("Filter projects", "themetextdomain"); ?></div>
                <form class="searchFiltersModal__form" id="search-form">
                    <div class="searchFiltersModal__control">
                        <label class="searchFiltersModal__label"><?php echo __("Location", "themetextdomain"); ?></label>
                        <input class="searchFiltersModal__input" type="text" name="location" placeholder="<?php echo __("Enter city", "themetextdomain"); ?>">
                    </div>
                    <div class="searchFiltersModal__control">
                        <label class="searchFiltersModal__label"><?php echo __("Home type", "themetextdomain"); ?></label>
                        <div class="searchFiltersModal__homeTypes">
                            <div class="searchFiltersModal__homeType">
                                <input class="searchFiltersModal__homeTypeInput" id="searchfilters-home-type-apartment" type="checkbox" name="home-type" value="apartment">
                                <label class="searchFiltersModal__homeTypeLabel" for="searchfilters-home-type-apartment"><?php echo __("Apartment", "themetextdomain"); ?></label>
                            </div>
                            <div class="searchFiltersModal__homeType">
                                <input class="searchFiltersModal__homeTypeInput" id="searchfilters-home-type-villa" type="checkbox" name="home-type" value="villa">
                                <label class="searchFiltersModal__homeTypeLabel" for="searchfilters-home-type-villa"><?php echo __("Villa", "themetextdomain"); ?></label>
                            </div>
                            <div class="searchFiltersModal__homeType">
                                <input class="searchFiltersModal__homeTypeInput" id="searchfilters-home-type-duplex" type="checkbox" name="home-type" value="duplex">
                                <label class="searchFiltersModal__homeTypeLabel" for="searchfilters-home-type-duplex"><?php echo __("Duplex", "themetextdomain"); ?></label>
                            </div>
                            <div class="searchFiltersModal__homeType">
                                <input class="searchFiltersModal__homeTypeInput" id="searchfilters-home-type-townhouse" type="checkbox" name="home-type" value="townhouse">
                                <label class="searchFiltersModal__homeTypeLabel" for="searchfilters-home-type-townhouse"><?php echo __("Townhouse", "themetextdomain"); ?></label>
                            </div>
                            <div class="searchFiltersModal__homeType">
                                <input class="searchFiltersModal__homeTypeInput" id="searchfilters-home-type-penthouse" type="checkbox" name="home-type" value="penthouse">
                                <label class="searchFiltersModal__homeTypeLabel" for="searchfilters-home-type-penthouse"><?php echo __("Penthouse", "themetextdomain"); ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="searchFiltersModal__control">
                        <label class="searchFiltersModal__label"><?php echo __("Price range", "themetextdomain"); ?></label>
                        <div class="searchFiltersModal__slider">
                            <div class="searchFiltersModal__tips">
                                <div class="searchFiltersModal__tip -lower <?php echo !is_rtl() ? '-smfBf' : '' ?>"></div>
                                <div class="searchFiltersModal__tipLine">-</div>
                                <div class="searchFiltersModal__tip -upper <?php echo !is_rtl() ? '-smfBf' : '' ?>"></div>
                            </div>
                            <div class="searchFiltersModal__bar" data-start="0" data-end="3000000"></div>
                            <div class="searchFiltersModal__barInputs">
                                <input class="searchFiltersModal__barInput -lower" type="hidden" name="price-from">
                                <input class="searchFiltersModal__barInput -upper" type="hidden" name="price-to">
                            </div>
                        </div>
                    </div>
                    <div class="searchFiltersModal__control">
                        <label class="searchFiltersModal__label"><?php echo __("Bedrooms", "themetextdomain"); ?></label>
                        <div class="searchFiltersModal__number">
                            <div class="searchFiltersModal__numberButton"><img src="<?php echo TEMP_URI; ?>/assets/images/minus.svg" alt=""></div>
                            <div class="searchFiltersModal__numberPresentation"><?php echo __("Any", "themetextdomain"); ?></div>
                            <input class="searchFiltersModal__numberInput" type="hidden" name="bedrooms" value="<?php echo __("Any", "themetextdomain"); ?>" readonly>
                            <div class="searchFiltersModal__numberButton"><img src="<?php echo TEMP_URI; ?>/assets/images/plus.svg" alt=""></div>
                        </div>
                    </div>
                    <div class="searchFiltersModal__control">
                        <label class="searchFiltersModal__label"><?php echo __("Bathrooms", "themetextdomain"); ?></label>
                        <div class="searchFiltersModal__number">
                            <div class="searchFiltersModal__numberButton"><img src="<?php echo TEMP_URI; ?>/assets/images/minus.svg" alt=""></div>
                            <div class="searchFiltersModal__numberPresentation"><?php echo __("Any", "themetextdomain"); ?></div>
                            <input class="searchFiltersModal__numberInput" type="hidden" name="bathrooms" value="<?php echo __("Any", "themetextdomain"); ?>" readonly>
                            <div class="searchFiltersModal__numberButton"><img src="<?php echo TEMP_URI; ?>/assets/images/plus.svg" alt=""></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="searchFiltersModal__bottom">
                <div class="searchFiltersModal__clear"><?php echo __("Clear", "themetextdomain"); ?></div>
                <div class="searchFiltersModal__resultWrapper">
                    <div class="searchFiltersModal__result">
                        <span><?php echo __("Found", "themetextdomain"); ?>:</span>
                        <span class="searchFiltersModal__resultCount"><?php echo __("0", "themetextdomain"); ?></span>
                    </div>
                    <div class="searchFiltersModal__showButton"><?php echo __("Show", "themetextdomain"); ?></div>
                </div>
            </div>
        </div>
    </div>
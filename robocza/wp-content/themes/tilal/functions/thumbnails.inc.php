<?php

add_image_size( 'Banner', 1600 , 800, true );
add_image_size( 'Banner_small', 1600 , 600, true );
add_image_size( 'Banner_title_image', 300, 0, true);
add_image_size( 'Board', 280 , 415, true );
add_image_size( 'Image_Large1', 889 , 549, true );
add_image_size( 'Image_Small1', 889 , 547, true );
add_image_size( 'Image_Large2', 710 , 550, true );


add_image_size( 'Image_about1', 600 , 410, true );
add_image_size( 'Image_about2', 700 , 580, true );


add_image_size( 'Home_grid', 440, 283, true);

add_image_size( 'Contact_map', 380, 200, true);

add_image_size( 'Scatter_top_left', 450, 450, true);
add_image_size( 'Scatter_top_right', 700, 580, true);
add_image_size( 'Scatter_bottom_left', 600, 370, true);
add_image_size( 'Scatter_bottom_right', 450, 450, true);

add_image_size( 'Investment_gallery', 680, 480, true);

add_image_size( 'Floor_plan', 1000, 0, true);

add_image_size( 'Projects_gallery', 1028, 638, true);

add_image_size( 'Boardx2', 560 , 830, true );
add_image_size( 'Miniature', 400 , 300, true );

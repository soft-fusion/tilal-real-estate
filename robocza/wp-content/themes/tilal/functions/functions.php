<?php
if(! defined( 'ABSPATH' )) return;
add_theme_support( 'post-thumbnails' );
function generateRandomString($length = 20)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGIJKLMNOPRSTUWYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }

    return $randomString;
}
function set_html_content_type()
{
    return 'text/html';
}

function get_id_by_template_filename($filename)
{
	global $wpdb;
	return $wpdb->get_var("SELECT wp_pm.post_id FROM {$wpdb->posts} wp_p JOIN {$wpdb->postmeta} wp_pm ON wp_p.ID=wp_pm.post_id WHERE wp_p.post_type='page' AND wp_p.post_status='publish' AND meta_key='_wp_page_template' AND meta_value='$filename' ");
}

function print_r_e($var)
{
    echo "<pre>";
	echo htmlspecialchars(print_r($var,true));
    echo "</pre>";
}

acf_add_options_page(array(
    'page_title' 	=> 'Contact',
    'menu_title'	=> 'Contact Settings',
    'menu_slug' 	=> 'contact_settings',
    'capability'	=> 'edit_posts',
));

acf_add_options_page(array(
    'page_title' 	=> 'Social Media',
    'menu_title'	=> 'Social Media',
    'menu_slug' 	=> 'social_media',
    'icon_url'      => 'dashicons-share',
    'capability'	=> 'edit_posts',
));



add_action( 'admin_menu', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}

function permalinksApartments() {
	global $wp,$wp_rewrite;
	$wp->add_query_var('apartment');
	$wp->add_query_var('investment');
	add_rewrite_rule('apartment/([a-z\-]+)\/(.+)\/?','index.php?pagename=tilal-homes&investment=$matches[1]&apartment=$matches[2]', 'top');
//	$my_current_lang = apply_filters( 'wpml_current_language', NULL );
//	if($my_current_lang === 'ar'){
//		add_rewrite_rule('apartment/([a-z\-]+)\/(.+)\/?','index.php?pagename=بيوت-تلال&investment=$matches[1]&apartment=$matches[2]', 'top');
//
//	}
//	else{
//		add_rewrite_rule('apartment/([a-z\-]+)\/(.+)\/?','index.php?pagename=tilal-homes&investment=$matches[1]&apartment=$matches[2]', 'top');
//
//	}
	$wp_rewrite->flush_rules(false);
}
add_action("init", "permalinksApartments");
add_filter( 'do_redirect_guess_404_permalink', '__return_false' );



function createSingleUrl($actualId){
	global $sitepress;
	if(!isset($_GET['lang']))
	{
		$sitepress->switch_lang('en');
		$langUrl = '/?lang=en';
		$lang = 'en';
	}
	else{
		$sitepress->switch_lang('ar');
		$langUrl = '';
		$lang = 'ar';
	}
	$args = array(
		'post_type' => 'investment',
		'post_status' => 'publish',
		'numberposts' => -1,
		'lang' => 'ar'
	);
	$wpml_home = apply_filters( 'wpml_permalink', home_url(), 'ar' );
	$url = $wpml_home;
	$id_en = apply_filters( 'wpml_object_id', $actualId, 'apartment', false, $lang );
	$query = new WP_Query( $args );
	$end = false;
	foreach ($query->posts as $item) {
		$tableApartments  = get_field( 'select_apartments', $item->ID );
		foreach ( $tableApartments as $apartment ) {
			$id = $apartment['add_project']->ID;
			if($id === $id_en){
				$my_current_lang = apply_filters( 'wpml_current_language', NULL );
				if($my_current_lang === 'ar'){
					$tilal_home = 'بيوت-تلال';
				}
				else{
					$tilal_home = 'tilal-homes';
				}
				$url  =  $wpml_home.'/tilal-homes/' . $item->post_name . '/' . $apartment['add_project']->post_name.$langUrl ;
				$end = true;
				break;
			}
		}
		if($end){
			break;
		}
	}
	if($lang === 'en')
	{
		$sitepress->switch_lang('ar');
	}
	else{
		$sitepress->switch_lang('en');
	}
	return $url;

}


add_filter('query_vars', 'query_vars');
function query_vars($public_query_vars)
{
	$public_query_vars[] = 'id';
	return $public_query_vars;
}
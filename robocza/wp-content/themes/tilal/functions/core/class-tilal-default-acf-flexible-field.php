<?php
new TilalDefaultField();
class TilalDefaultField {

	private const NAME_DEFAULT_ACF_FIELD = 'Default ACF';
	private const SLUG_DEFAULT_ACF_FIELD = 'default-fields_acf';

	public function __construct() {
		$this->declare_options_acf_field();
		$this->create_extra_acf_field();
		$this->generate_default_acf_field();

	}

	private function generate_default_acf_field(){

		add_filter('acf/load_value/name=fields', function ($value, $post_id, $field){
			if ($value !== NULL) {
				// $value will only be NULL on a new post
				return $value;
			}
			$defaults = get_field('typepost_default_acf_value', 'option');
			if(is_array($defaults) && !empty($defaults) && count($defaults) > 0){
				foreach ($defaults as $default){
					if ( $default['post_type_value'] == get_post_type()){
						foreach ($default['declare_default_field'] as $item){
							$value[] = array('acf_fc_layout' => $item['default_field_slug_text']);
						}
					}
				}
			}

			return $value;
		}, 10, 3);
	}

	private function create_extra_acf_field(){
		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_63ca5ea147f9d',
				'title' => 'Default fields',
				'fields' => array(
					array(
						'key' => 'field_63ca5ec88cf46',
						'label' => 'Default fields ACF',
						'name' => 'typepost_default_acf_value',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => '',
						'wpml_cf_preferences' => 0,
						'sub_fields' => array(
							array(
								'key' => 'field_63ca5ef08cf47',
								'label' => 'Post Types',
								'name' => 'post_type_value',
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array(
									'apartment' => 'Apartments',
									'investment' => 'Investment',
									'city' => 'City',
								),
								'default_value' => false,
								'allow_null' => 0,
								'multiple' => 0,
								'ui' => 0,
								'return_format' => 'value',
								'wpml_cf_preferences' => 0,
								'ajax' => 0,
								'placeholder' => '',
							),
							array(
								'key' => 'field_63ca6086b64c5',
								'label' => 'Declare default field',
								'name' => 'declare_default_field',
								'type' => 'repeater',
								'instructions' => 'Enter the slug fields you want to set as default. Example: main_banner or scrolling_tabs',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'collapsed' => '',
								'min' => 0,
								'max' => 0,
								'layout' => 'table',
								'button_label' => '',
								'wpml_cf_preferences' => 0,
								'sub_fields' => array(
									array(
										'key' => 'field_63ca6134b64c6',
										'label' => 'Slug text',
										'name' => 'default_field_slug_text',
										'type' => 'text',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array(
											'width' => '',
											'class' => '',
											'id' => '',
										),
										'wpml_cf_preferences' => 0,
										'default_value' => '',
										'placeholder' => '',
										'prepend' => '',
										'append' => '',
										'maxlength' => '',
									),
								),
							),
						),
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'default-fields_acf',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

		endif;
	}

	private function declare_options_acf_field(){
		acf_add_options_page(array(
			'page_title' 	=> self::NAME_DEFAULT_ACF_FIELD,
			'menu_title'	=> self::NAME_DEFAULT_ACF_FIELD,
			'menu_slug' 	=> self::SLUG_DEFAULT_ACF_FIELD,
			'icon_url'      => 'dashicons-media-default',
			'capability'	=> 'edit_posts',
		));
	}
}
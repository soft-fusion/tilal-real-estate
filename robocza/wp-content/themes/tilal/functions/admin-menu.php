<?php
if(! defined( 'ABSPATH' )) return;

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

register_nav_menus(array(
    'top_menu' => __("Top Menu"),
    'top_menu_mobile' => __("Top Menu Mobile"),
    'footer_menu' => __("Footer Menu"),
    'footer_menu_top' => __("Footer Menu Top"),
));
function remove_wp_version_rss() { return ''; }
add_filter('the_generator', 'remove_wp_version_rss');
function vc_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 99999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 99999 );

function custom_menu_page_removing() {
}
add_action('admin_menu', 'custom_menu_page_removing');

function filter_url_img($image)
{
    $image[0] = utf8_uri_encode($image[0]);
    return $image;
}

add_filter('wp_get_attachment_image_src', 'filter_url_img' );


add_action('wp_head','add_ajax_url');
function add_ajax_url()
{
    $admin_url = admin_url('admin-ajax.php');
    echo "<script>var ajaxurl = \"{$admin_url}\"; </script>";
}

function my_htaccess_contents( $rules )
{
	return $rules . "\n" .
	       "<IfModule mod_headers.c>
 <FilesMatch \"\.(jpg|jpeg|png|gif|swf|JPG)$\">
 Header set Cache-Control \"max-age=4838400, public\"
 </FilesMatch>\n
 <FilesMatch \"\.(css|js)$\">
 Header set Cache-Control \"max-age=4838400, private\"
 </FilesMatch>\n
 </IfModule>";
}
add_filter('mod_rewrite_rules', 'my_htaccess_contents');

class Top_Menu_Walker extends Walker_Nav_Menu {

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {

        if (0 == $depth) {
            $item_html = apply_filters('the_title', $item->title, $item->ID);
            $output .= '<li class="menu__item"><a href ="' . $item->url . '" class="menu__link">' . $item_html . '</a>';
            return;
        }
    }
}
class Top_Menu_Mobile_Walker extends Walker_Nav_Menu {

	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
	{

		if (0 == $depth) {
			if ($item->url === 'http://translate')
			{
				$item_html = apply_filters('the_title', $item->title, $item->ID);
				$id_apartment = get_the_ID();
				if($id_apartment === false){
					global $wp_query;
					$posts = get_posts( array(
						'name' => $wp_query->query_vars['name'],
						'post_type' => 'apartment',
					));
					$id_apartment = $posts[0]->ID;
				}
				if(get_post_type($id_apartment) === 'apartment' ){
					if ($_GET['lang'] === 'ar' || $_GET['lang'] === null) :
						$wpml_permalink_switch = createSingleUrl($id_apartment);
						$flagFirst = 'ar.png';
						$flagSecond = 'en.png';
						$altFirst = 'العربية';
						$altSecond = 'الإنجليزية';
					elseif ($_GET['lang'] === 'en') :
						$wpml_permalink_switch = createSingleUrl($id_apartment);
						$flagFirst = 'en.png';
						$flagSecond = 'ar.png';
						$altFirst = 'English';
						$altSecond = 'Arabic';
					endif;
					$switcher = '<div class="wpml-ls-statics-shortcode_actions wpml-ls wpml-ls-rtl wpml-ls-legacy-dropdown js-wpml-ls-legacy-dropdown">
                                <ul>
                                    <li tabindex="0" class="wpml-ls-slot-shortcode_actions wpml-ls-item wpml-ls-item-ar wpml-ls-current-language wpml-ls-last-item wpml-ls-item-legacy-dropdown">
                                        <a href="#" class="js-wpml-ls-item-toggle wpml-ls-item-toggle">
                                            <img class="wpml-ls-flag" src="https://tilalre.com/wp-content/plugins/sitepress-multilingual-cms/res/flags/'.$flagFirst.'" alt="'. $altFirst .'" width="18" height="12"></a>
                                        <ul class="wpml-ls-sub-menu">
                                            <li class="wpml-ls-slot-shortcode_actions wpml-ls-item wpml-ls-item-en wpml-ls-first-item">
                                                <a href="'. $wpml_permalink_switch .'" class="wpml-ls-link">
                                                    <img class="wpml-ls-flag" src="https://tilalre.com/wp-content/plugins/sitepress-multilingual-cms/res/flags/'. $flagSecond .'" alt="'. $altSecond .'" width="18" height="12">
                                                </a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </div>'
					?>

					<?php
				}
				else{
					$switcher = do_shortcode("[wpml_language_selector_widget]");
				}
				$output .= '<li class="menu__item">
                    <div class="menu__link">'. $item_html.'</div>
                    <ul class="menu__language">'. $switcher .'
                    </ul>
                </li>';
			}
			else
			{
				$item_html = apply_filters('the_title', $item->title, $item->ID);
				$output .= '<li class="menu__item"><a href ="' . $item->url . '" class="menu__link">' . $item_html . '</a>';
			}

			return;
		}
	}
}

class Footer_Menu_Walker extends Walker_Nav_Menu {

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {

        if (0 == $depth) {
            $item_html = apply_filters('the_title', $item->title, $item->ID);
            $output .= '<li class="footer__listItem"><a href ="' . $item->url . '" class="footer__link">' . $item_html . '</a>';
            return;
        }
    }
}


class Footer_Menu_Walker_Top extends Walker_Nav_Menu {

    public function start_el( &$output, $item, $depth = 0, $args = null, $id = 0 ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

        $classes   = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;
        $classes[] = ($depth == 0) ? 'footer__item' : 'footer__second_link';

        /**
         * Filters the arguments for a single nav menu item.
         *
         * @since 4.4.0
         *
         * @param stdClass $args  An object of wp_nav_menu() arguments.
         * @param WP_Post  $item  Menu item data object.
         * @param int      $depth Depth of menu item. Used for padding.
         */
        $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

        /**
         * Filters the CSS classes applied to a menu item's list item element.
         *
         * @since 3.0.0
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param string[] $classes Array of the CSS classes that are applied to the menu item's `<li>` element.
         * @param WP_Post  $item    The current menu item.
         * @param stdClass $args    An object of wp_nav_menu() arguments.
         * @param int      $depth   Depth of menu item. Used for padding.
         */
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        /**
         * Filters the ID applied to a menu item's list item element.
         *
         * @since 3.0.1
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param string   $menu_id The ID that is applied to the menu item's `<li>` element.
         * @param WP_Post  $item    The current menu item.
         * @param stdClass $args    An object of wp_nav_menu() arguments.
         * @param int      $depth   Depth of menu item. Used for padding.
         */
        $id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $class_names . '>';

        $atts           = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target ) ? $item->target : '';
        if ( '_blank' === $item->target && empty( $item->xfn ) ) {
            $atts['rel'] = 'noopener noreferrer';
        } else {
            $atts['rel'] = $item->xfn;
        }
        $atts['href']         = ! empty( $item->url ) ? $item->url : '';
        $atts['aria-current'] = $item->current ? 'page' : '';

        /**
         * Filters the HTML attributes applied to a menu item's anchor element.
         *
         * @since 3.6.0
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param array $atts {
         *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
         *
         *     @type string $title        Title attribute.
         *     @type string $target       Target attribute.
         *     @type string $rel          The rel attribute.
         *     @type string $href         The href attribute.
         *     @type string $aria_current The aria-current attribute.
         * }
         * @param WP_Post  $item  The current menu item.
         * @param stdClass $args  An object of wp_nav_menu() arguments.
         * @param int      $depth Depth of menu item. Used for padding.
         */
        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( is_scalar( $value ) && '' !== $value && false !== $value ) {
                $value       = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        /** This filter is documented in wp-includes/post-template.php */
        $title = apply_filters( 'the_title', $item->title, $item->ID );

        /**
         * Filters a menu item's title.
         *
         * @since 4.4.0
         *
         * @param string   $title The menu item's title.
         * @param WP_Post  $item  The current menu item.
         * @param stdClass $args  An object of wp_nav_menu() arguments.
         * @param int      $depth Depth of menu item. Used for padding.
         */
        $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );


        if($depth == 0 )
        {
            $item_output = $args->before;
            $item_output .= '<h3 class="footer__title">';
            $item_output .= $args->link_before . $title . $args->link_after;
            $item_output .= '</h3>';
            $item_output .= $args->after;
        }
        else
        {
            $item_output = $args->before;
            $item_output .= '<a class="footer__link_url"' . $attributes . '>';
            $item_output .= $args->link_before . $title . $args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;
        }

        /**
         * Filters a menu item's starting output.
         *
         * The menu item's starting output only includes `$args->before`, the opening `<a>`,
         * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
         * no filter for modifying the opening and closing `<li>` for a menu item.
         *
         * @since 3.0.0
         *
         * @param string   $item_output The menu item's starting HTML output.
         * @param WP_Post  $item        Menu item data object.
         * @param int      $depth       Depth of menu item. Used for padding.
         * @param stdClass $args        An object of wp_nav_menu() arguments.
         */
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    public function start_lvl( &$output, $depth = 0, $args = null ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat( $t, $depth );

        // Default class.
        $classes = array( 'sub-menu' );


        if(in_array('sub-menu', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth )))
            $classes[] = 'footer__column';


        /**
         * Filters the CSS class(es) applied to a menu list element.
         *
         * @since 4.8.0
         *
         * @param string[] $classes Array of the CSS classes that are applied to the menu `<ul>` element.
         * @param stdClass $args    An object of `wp_nav_menu()` arguments.
         * @param int      $depth   Depth of menu item. Used for padding.
         */
        $class_names = join( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );

        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $output .= "{$n}{$indent}<ul$class_names>{$n}";
    }
}
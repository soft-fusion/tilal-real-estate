<?php

require_once("thumbnails.inc.php");
require_once("admin-menu.php");
require_once("functions.php");

require_once("post-types/SF_post.php");
require_once("post-types/SF_partners.php");
require_once("post-types/SF_investment.php");
require_once("post-types/SF_apartment.php");
require_once("post-types/SF_city.php");
require_once("post-types/SF_newsletter.php");

require_once("endpoints/api-all-investment.php");
require_once("endpoints/api-city.php");
require_once("endpoints/api_name_investment.php");


require_once("core/class-tilal-default-acf-flexible-field.php");

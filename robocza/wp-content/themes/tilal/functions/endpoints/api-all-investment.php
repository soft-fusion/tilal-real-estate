<?php
add_action( 'rest_api_init', function() {
	register_rest_route( 'allInvestment/v1', '/response', [
		'methods' => 'GET',
		'callback' => 'get_projects',
		'permission_callback' => '__return_true',
	] );
} );
function get_projects() {
	$coming = 'cooming-soon';
	if(!isset($_GET['lang']))
	{
		global $sitepress;
		$sitepress->switch_lang('ar');
	}
	$args = array(
		'post_type' => 'investment',
		'post_status' => 'publish'
	);
	$query = new WP_Query( $args );
	$counter = 0;
	$apartmentArray = [];
	$jsonArray = [];
	foreach ($query->posts as $item) {
		$apartmentCounter = 0;
		$tableApartments  = get_field( 'select_apartments', $item->ID );
		
		foreach ( $tableApartments as $apartment ) {
			$id                                  = $apartment['add_project']->ID;
			$my_current_lang = apply_filters( 'wpml_current_language', NULL );
			if($my_current_lang === 'ar'){
				$tilal_home = 'بيوت-تلال';
			}
			else{
				$tilal_home = 'tilal-homes';
			}
			$url                                 = home_url( '/tilal-homes/' . $item->post_name . '/' . $apartment['add_project']->post_name );
			$apartmentArray[ $apartmentCounter ] = array(
				'name'      => $apartment['add_project']->post_title,
				'type'      => get_field( 'home_type_apartment', $id ),
				'price'     => get_field( 'price_apartment', $id ),
				'bedrooms'  => get_field( 'bedrooms_apartments', $id ),
				'bathrooms' => get_field( 'bathrooms_apartment', $id ),
				'link'      => $url,
			);
			$apartmentCounter ++;
		}
		$image         = get_field( 'miniature_apartments', $item->ID );
		$image         = wp_get_attachment_image_url( $image['ID'], 'Miniature' );
		$latitude      = get_field( 'latitude_apartments', $item->ID );
		$longitude     = get_field( 'longitude_apartments', $item->ID );
		$positionArray = array( 'latitude' => $latitude, 'longitude' => $longitude );

		$visible  = get_field( 'project_visible_investment', $item->ID, true );
		if(empty($apartmentArray) && $visible === 'true'){
			$apartmentArray[] = array(
				'name'      => $coming,
				'type'      => array(0=>'0'),
				'price'     => 0,
				'bedrooms'  => 0,
				'bathrooms' => 0,
				'link'      => $coming,
			);
		}

		if ( !empty($apartmentArray) && $visible === 'true') {
			$jsonArray[ $counter ] = array(
				'name_investment' => $item->post_title,
				'city'            => get_field( 'city_investment', $item->ID )->post_title,
				'miniature'       => $image,
				'link_investment' => get_the_permalink( $item->ID ),
				'position'        => $positionArray,
				'apartment'       => $apartmentArray,

			);
			$counter++;
		}

	}
	return $jsonArray;
}
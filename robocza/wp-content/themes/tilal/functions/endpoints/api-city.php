<?php
add_action( 'rest_api_init', function() {
	register_rest_route( 'allCity/v1', '/response', [
		'methods' => 'GET',
		'callback' => 'get_city',
		'permission_callback' => '__return_true',
	] );
} );
function get_city() {
	if(!isset($_GET['lang']))
	{
		global $sitepress;
		$sitepress->switch_lang('ar');
	}
	$args = array(
		'post_type' => 'city',
	);
	$query = new WP_Query( $args );
	$counter = 0;
	$jsonArray = [];
	foreach ($query->posts as $item)
	{
		$jsonArray[$counter] = array(
			'name_city' => $item->post_title,
			'link_city' => get_the_permalink($item->ID),
		);
		$counter++;
	}
	return $jsonArray;
}
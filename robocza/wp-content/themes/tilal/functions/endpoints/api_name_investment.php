<?php

add_action( 'rest_api_init', function () {
	register_rest_route( 'searchPosition/v1', '/response', [
		'methods'             => 'GET',
		'callback'            => 'get_name_investment',
		'permission_callback' => '__return_true',
	] );
} );
function get_name_investment() {
	if(!isset($_GET['lang']))
	{
		global $sitepress;
		$sitepress->switch_lang('ar');
	}
	$args = array(
		'post_type' => 'investment',
		'post_status' => array('publish','private'),
		's' => $_GET['investment']
	);
	$query = new WP_Query( $args );
	$jsonArray = [];
	$counter = 0;
	foreach ($query->posts as $item)
	{
		$latitude = get_field('latitude_apartments', $item->ID);
		$longitude = get_field('longitude_apartments', $item->ID);
		$googleMapsLink = get_field('google_maps_link', $item->ID);
		$positionArray = array( 'latitude' => $latitude, 'longitude' => $longitude);
		$jsonArray[$counter] = array(
			'position' => $positionArray,
			'google_maps_link' => $googleMapsLink
		);
		$counter++;
	}
	return $jsonArray;
}
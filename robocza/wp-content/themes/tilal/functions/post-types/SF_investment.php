<?php

class SF_investment extends SF_abstract_post {

	public static $name = 'Investment';
	public static $slug = 'investment';

	public function createPost()
	{
		$labels = array(
			'name' => _x(self::$name, 'tag'),
			'singular_name' => _x(self::$name, 'tag'),
			'add_new' => _x('Add investment', 'tag'),
			'add_new_item' => _x('Add investment', 'tag'),
			'edit_item' => _x('Edit investment', 'tag'),
			'new_item' => _x('New investment', 'tag'),
			'view_item' => _x('View investment', 'tag'),
			'search_items' => _x('Search investment', 'tag'),
			'not_found' => _x('Not Found', 'tag'),
			'not_found_in_trash' => _x('Not found in trash', 'tag'),
			'parent_item_colon' => _x('Parent:', 'tag'),
			'menu_name' => _x(self::$name, 'tag'),
		);

		$my_current_lang = apply_filters( 'wpml_current_language', NULL );
		if($my_current_lang === 'ar') {
			$rewrite = 'بيوت-تلال';
		}else{
			$rewrite = 'tilal-homes';
		}

		$args = array(
			'labels' => $labels,
			'hierarchical' => false,
			'supports' => array('title'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 53,
			'menu_icon' => 'dashicons-book',
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'has_archive' => false,
			'query_var' => false,
			'can_export' => true,
			'capability_type' => 'post',
			'rewrite' => array('slug' => 'tilal-homes')
		);

		register_post_type(self::$slug, $args);
	}
}
new SF_investment();
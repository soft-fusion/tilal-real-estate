<?php


class SF_partners extends SF_abstract_post
{
    public static $name = 'Partners';
    public static $slug = 'partners';

    public function createPost()
    {
        $labels = array(
            'name' => _x(self::$name, 'tag'),
            'singular_name' => _x(self::$name, 'tag'),
            'add_new' => _x('Add partner', 'tag'),
            'add_new_item' => _x('Add partner', 'tag'),
            'edit_item' => _x('Edit partner', 'tag'),
            'new_item' => _x('New partner', 'tag'),
            'view_item' => _x('View partner', 'tag'),
            'search_items' => _x('Search partner', 'tag'),
            'not_found' => _x('Not Found', 'tag'),
            'not_found_in_trash' => _x('Not found in trash', 'tag'),
            'parent_item_colon' => _x('Rodzic:', 'tag'),
            'menu_name' => _x(self::$name, 'tag'),
        );

        $args = array(
            'labels' => $labels,
            'hierarchical' => false,
            'supports' => array('title', 'thumbnail' , 'editor'),
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 51,
            'menu_icon' => 'dashicons-buddicons-buddypress-logo',
            'show_in_nav_menus' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => true,
            'has_archive' => false,
            'query_var' => false,
            'can_export' => true,
            'capability_type' => 'post',
            'rewrite' => array('slug' => 'partner')
        );

        register_post_type(self::$slug, $args);
    }
}
new SF_partners();
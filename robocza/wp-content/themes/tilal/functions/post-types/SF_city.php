<?php


class SF_city extends SF_abstract_post {

	public static $name = 'City';
	public static $slug = 'city';

	public function createPost()
	{
		$labels = array(
			'name' => _x(self::$name, 'tag'),
			'singular_name' => _x(self::$name, 'tag'),
			'add_new' => _x('Add city', 'tag'),
			'add_new_item' => _x('Add city', 'tag'),
			'edit_item' => _x('Edit city', 'tag'),
			'new_item' => _x('New city', 'tag'),
			'view_item' => _x('View cityt', 'tag'),
			'search_items' => _x('Search city', 'tag'),
			'not_found' => _x('Not Found', 'tag'),
			'not_found_in_trash' => _x('Not found in trash', 'tag'),
			'parent_item_colon' => _x('Parent:', 'tag'),
			'menu_name' => _x(self::$name, 'tag'),
		);

		$args = array(
			'labels' => $labels,
			'hierarchical' => false,
			'supports' => array('title'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 54,
			'menu_icon' => 'dashicons-admin-site',
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'has_archive' => false,
			'query_var' => false,
			'can_export' => true,
			'capability_type' => 'post',
		);

		register_post_type(self::$slug, $args);
	}
}
new SF_city();
<?php


class SF_newsletter extends SF_abstract_post {

	public static $name = 'Newsletter Post';
	public static $slug = 'newsletter_post';

	public function createPost()
	{
		$labels = array(
			'name' => _x(self::$name, 'tag'),
			'singular_name' => _x(self::$name, 'tag'),
			'add_new' => _x('Add post', 'tag'),
			'add_new_item' => _x('Add post', 'tag'),
			'edit_item' => _x('Edit post', 'tag'),
			'new_item' => _x('New post', 'tag'),
			'view_item' => _x('View post', 'tag'),
			'search_items' => _x('Search post', 'tag'),
			'not_found' => _x('Not Found', 'tag'),
			'not_found_in_trash' => _x('Not found in trash', 'tag'),
			'parent_item_colon' => _x('Parent:', 'tag'),
			'menu_name' => _x(self::$name, 'tag'),
		);

		$args = array(
			'labels' => $labels,
			'hierarchical' => false,
			'supports' => array('title'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-admin-post',
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'has_archive' => false,
			'query_var' => false,
			'can_export' => true,
			'capability_type' => 'post',
		);

		register_post_type(self::$slug, $args);
	}
}
new SF_newsletter();

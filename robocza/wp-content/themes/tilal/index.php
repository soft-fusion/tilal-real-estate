<?php
get_header();
//$languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );
//$actualLang = apply_filters( 'wpml_current_language', null );

//var_dump($languages);
//var_dump($actualLang);
//var_dump($wpml_permalink);

the_post();
global $wp;
$postValue = $wp->query_vars['attachment'];
//$url = get_the_permalink();
//$wpml_permalink = apply_filters( 'wpml_permalink', $url , 'en' );
//$args = new WP_Query( array(
//	'post_type'         => 'apartment',
//	'post_status'       => 'publish',
//	'posts_per_page'    => -1,
//	'suppress_filters'  => false,
//	's' => $postValue,
//) );
//var_dump($args);

$query = new WP_Query( array(
	'post_type' => 'apartment',
	'name' => $postValue ) );
$id = $query->posts[0]->ID;

if(have_rows('fields', $id)) {

	$close_div = 0;
	while (have_rows('fields', $id)) {
		the_row();
		$block = get_row_layout();
		include TEMP_VAR . '/template-files/' . $block . '.php';
	}
}
?>
<?php
get_footer();

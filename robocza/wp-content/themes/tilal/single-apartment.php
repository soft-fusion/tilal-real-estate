<?php
global $wp;
$postValue = $wp->query_vars['name'];
$args = array(
	'post_type' => 'investment',
);
$query = new WP_Query( $args );
$counter = 0;
$apartmentArray = [];
$jsonArray = [];
$url = home_url();
$id = get_the_ID();

foreach ($query->posts as $item) {
	$tableApartments  = get_field( 'select_apartments', $item->ID );
	foreach ( $tableApartments as $apartment ) {
		if($apartment['add_project']->post_name === $postValue) {
			$my_current_lang = apply_filters( 'wpml_current_language', NULL );
			if($my_current_lang === 'ar'){
				$tilal_home = 'بيوت-تلال';
			}
			else{
				$tilal_home = 'tilal-homes';
			}
			$url = home_url( '/tilal-homes/' . $item->post_name . '/' . $apartment['add_project']->post_name );
			$_POST['single_id'] = $id;
			break;
		}
	}
}

header('Location: '. $url);